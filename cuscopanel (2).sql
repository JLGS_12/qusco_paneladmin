-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 10-06-2019 a las 06:52:27
-- Versión del servidor: 10.1.38-MariaDB
-- Versión de PHP: 7.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `cuscopanel`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `activity_logs`
--

CREATE TABLE `activity_logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `model_id` int(11) DEFAULT NULL,
  `model_class` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `data` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `activity_logs`
--

INSERT INTO `activity_logs` (`id`, `user_id`, `model_id`, `model_class`, `message`, `data`, `created_at`) VALUES
(1, 1, NULL, NULL, 'Logged In', NULL, '2019-04-12 11:46:40'),
(2, 1, 2, 'Kjjdion\\LaravelAdminPanel\\Models\\Role', 'Created Role: administrador', '{\"name\":\"administrador\",\"permissions\":[\"1\",\"12\"]}', '2019-04-12 11:47:17'),
(3, 1, 2, 'App\\User', 'Created User: jose', '{\"name\":\"jose\",\"email\":\"jose@jose.com\",\"roles\":[\"2\"]}', '2019-04-12 11:47:38'),
(4, 2, NULL, NULL, 'Logged In', NULL, '2019-04-12 11:47:53'),
(5, 2, NULL, NULL, 'Logged In', NULL, '2019-04-13 20:08:41'),
(6, 2, NULL, NULL, 'Logged In', NULL, '2019-04-14 00:20:15'),
(7, 2, NULL, NULL, 'Logged In', NULL, '2019-04-14 03:17:21'),
(8, 2, NULL, NULL, 'Logged In', NULL, '2019-04-14 05:27:54'),
(9, 2, NULL, NULL, 'Logged In', NULL, '2019-04-14 18:19:34'),
(10, 2, NULL, NULL, 'Logged In', NULL, '2019-04-15 06:19:45'),
(11, 2, NULL, NULL, 'Logged In', NULL, '2019-05-17 06:38:51'),
(12, 1, NULL, NULL, 'Logged In', NULL, '2019-05-17 06:44:00'),
(13, 2, NULL, NULL, 'Logged In', NULL, '2019-05-18 20:58:19'),
(14, 1, NULL, NULL, 'Logged In', NULL, '2019-05-18 21:00:18'),
(15, 1, 3, 'Kjjdion\\LaravelAdminPanel\\Models\\Role', 'Created Role: adm_distritos', '{\"name\":\"adm_distritos\",\"permissions\":[\"1\",\"12\"]}', '2019-05-18 21:01:16'),
(16, 1, 3, 'App\\User', 'Created User: manhatan', '{\"name\":\"manhatan\",\"email\":\"manhathan@manhatha.com\",\"roles\":[\"3\"]}', '2019-05-18 21:02:37'),
(17, 1, 3, 'App\\User', 'Updated User: manhatan', '{\"name\":\"manhatan\",\"email\":\"manhattan@manhattan.com\",\"roles\":[\"3\"]}', '2019-05-18 21:04:07'),
(18, 1, 3, 'App\\User', 'Updated User: manhatan', '{\"name\":\"manhatan\",\"email\":\"manhattan@manhattan.com\",\"roles\":[\"3\"]}', '2019-05-18 21:04:12'),
(19, 1, 3, 'App\\User', 'Changed User Password: manhatan', NULL, '2019-05-18 21:04:29'),
(20, 3, NULL, NULL, 'Logged In', NULL, '2019-05-18 21:04:53'),
(21, 2, NULL, NULL, 'Logged In', NULL, '2019-05-18 22:46:48'),
(22, 3, NULL, NULL, 'Logged In', NULL, '2019-05-18 22:53:12'),
(23, 3, NULL, NULL, 'Logged In', NULL, '2019-05-29 08:44:53'),
(24, 2, NULL, NULL, 'Logged In', NULL, '2019-06-05 09:31:45'),
(25, 3, NULL, NULL, 'Logged In', NULL, '2019-06-09 05:59:25'),
(26, 3, NULL, NULL, 'Logged In', NULL, '2019-06-09 21:54:45');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `disamd_capas`
--

CREATE TABLE `disamd_capas` (
  `id` int(11) NOT NULL,
  `nom_capa` text NOT NULL,
  `idsector` int(11) DEFAULT NULL,
  `maestro` int(11) DEFAULT NULL,
  `esclavo` int(11) DEFAULT NULL,
  `descripcion` text,
  `estado` int(11) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `disamd_capas`
--

INSERT INTO `disamd_capas` (`id`, `nom_capa`, `idsector`, `maestro`, `esclavo`, `descripcion`, `estado`, `created_at`, `updated_at`) VALUES
(1, 'josejose', 1, NULL, NULL, 'estamos muy muy alto', 1, '2019-06-09 07:26:19', '2019-06-09 07:26:19'),
(2, 'jojojo', 1, NULL, NULL, 'asdasdasdasd', 1, '2019-06-09 07:26:47', '2019-06-09 07:26:47');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `disamd_mapa`
--

CREATE TABLE `disamd_mapa` (
  `id` int(11) NOT NULL,
  `idrut` int(11) NOT NULL,
  `lat` double NOT NULL,
  `lon` double NOT NULL,
  `idcapas` int(11) NOT NULL DEFAULT '0',
  `area` double NOT NULL DEFAULT '0',
  `estado` int(11) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `disamd_mapa`
--

INSERT INTO `disamd_mapa` (`id`, `idrut`, `lat`, `lon`, `idcapas`, `area`, `estado`, `created_at`, `updated_at`) VALUES
(1, 1, -11.901499807167562, -77.03771352460632, 0, 0, NULL, NULL, NULL),
(2, 1, -11.904187334251795, -77.04067468335876, 0, 0, NULL, NULL, NULL),
(3, 1, -11.905426107319792, -77.03447341611633, 0, 0, NULL, NULL, NULL),
(10, 3, -11.900680945977943, -77.03852891614684, 0, 0, NULL, NULL, NULL),
(11, 3, -11.903998368372191, -77.04155444791564, 0, 0, NULL, NULL, NULL),
(12, 3, -11.904985188739408, -77.0353317230011, 0, 0, NULL, NULL, NULL),
(13, 3, -11.901615287393302, -77.03547119786987, 0, 0, NULL, NULL, NULL),
(14, 4, -11.902129698711956, -77.03610419919738, 0, 0, NULL, NULL, NULL),
(15, 4, -11.90477522725923, -77.0376384227539, 0, 0, NULL, NULL, NULL),
(16, 4, -11.905405111213128, -77.04269170453796, 0, 0, NULL, NULL, NULL),
(17, 4, -11.9053211267703, -77.03434467008361, 0, 0, NULL, NULL, NULL),
(18, 4, -11.904103349432633, -77.03576087644348, 0, 0, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `disamd_proyectos`
--

CREATE TABLE `disamd_proyectos` (
  `id` int(11) NOT NULL,
  `nom_proyecto` text NOT NULL,
  `pro_presupuesto` double DEFAULT NULL,
  `id_sector` int(11) DEFAULT NULL,
  `inicio` text,
  `fin` text,
  `descripcion` text,
  `estado` int(11) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `disamd_proyectos`
--

INSERT INTO `disamd_proyectos` (`id`, `nom_proyecto`, `pro_presupuesto`, `id_sector`, `inicio`, `fin`, `descripcion`, `estado`, `created_at`, `updated_at`) VALUES
(1, 'jos', 12.21, 4, '2019-10-12', '2019-12-12', '12sdddad', 1, '2019-06-09 22:37:37', '2019-06-09 22:37:37');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `disamd_rutas`
--

CREATE TABLE `disamd_rutas` (
  `id` int(11) NOT NULL,
  `nombreruta` varchar(91) NOT NULL,
  `estado` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `disamd_rutas`
--

INSERT INTO `disamd_rutas` (`id`, `nombreruta`, `estado`, `created_at`, `updated_at`) VALUES
(1, 'asdasdsa', 0, NULL, NULL),
(3, 'trenes', 0, NULL, NULL),
(4, 'jose', 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `disamd_sectors`
--

CREATE TABLE `disamd_sectors` (
  `id` int(11) NOT NULL,
  `nom_sector` text NOT NULL,
  `presupuesto` double DEFAULT NULL,
  `descripcion` text,
  `estado` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `disamd_sectors`
--

INSERT INTO `disamd_sectors` (`id`, `nom_sector`, `presupuesto`, `descripcion`, `estado`, `created_at`, `updated_at`) VALUES
(1, 'Jose', 15.69, 'una oveja', 1, '2019-06-09 06:05:12', '2019-06-09 06:05:12'),
(2, 'Proyecto de Salida', 12.12, 'Esta es la prueba', 1, '2019-06-09 22:17:47', '2019-06-09 22:17:47'),
(3, 'tmr este es', 12.21, 'esta es la salida', 1, '2019-06-09 22:22:20', '2019-06-09 22:22:20'),
(4, 'ESTA SI ES PORLPTM', 12.21, 'esta es', 1, '2019-06-09 22:25:36', '2019-06-09 22:25:36');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `docs`
--

CREATE TABLE `docs` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `system` tinyint(1) NOT NULL DEFAULT '0',
  `_lft` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `_rgt` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `docs`
--

INSERT INTO `docs` (`id`, `type`, `title`, `slug`, `content`, `system`, `_lft`, `_rgt`, `parent_id`, `created_at`, `updated_at`) VALUES
(1, 'Index', 'Documentation', NULL, 'Welcome to the documentation!', 1, 1, 2, NULL, '2019-04-12 11:46:16', '2019-04-12 11:46:16'),
(2, '404', 'Page Not Found', NULL, 'Sorry, the page was not found.', 1, 3, 4, NULL, '2019-04-12 11:46:16', '2019-04-12 11:46:16');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_11_21_000000_create_permissions_table', 1),
(4, '2018_11_21_100000_create_roles_table', 1),
(5, '2018_11_21_200000_create_users_columns', 1),
(6, '2018_11_21_300000_create_activity_logs_table', 1),
(7, '2018_11_21_400000_create_docs_table', 1),
(8, '2018_11_21_500000_create_settings_table', 1),
(9, '2019_04_05_014013_create_qusco_usuarios_table', 1),
(10, '2019_04_05_045228_create_qusco_tipo_residuos_table', 1),
(11, '2019_04_05_184036_create_qusco_ubicacions_table', 1),
(12, '2019_04_05_184056_create_qusco_ruta_horarios_table', 1),
(13, '2019_04_05_184142_create_qusco_datos_usuarios_table', 1),
(14, '2019_04_05_184206_create_qusco_recolectados_table', 1),
(15, '2019_04_10_014345_create_qusco_ventas_table', 1),
(16, '2019_04_11_152142_create_qusco_rutas_table', 1),
(17, '2019_04_11_160501_create_qusco_areas_table', 1),
(18, '2019_04_11_161542_create_qusco_horarios_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `group` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `permissions`
--

INSERT INTO `permissions` (`id`, `group`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Admin Panel', 'Access Admin Panel', '2019-04-12 11:46:09', '2019-04-12 11:46:09'),
(2, 'Roles', 'Create Roles', '2019-04-12 11:46:10', '2019-04-12 11:46:10'),
(3, 'Roles', 'Read Roles', '2019-04-12 11:46:10', '2019-04-12 11:46:10'),
(4, 'Roles', 'Update Roles', '2019-04-12 11:46:11', '2019-04-12 11:46:11'),
(5, 'Roles', 'Delete Roles', '2019-04-12 11:46:11', '2019-04-12 11:46:11'),
(6, 'Users', 'Create Users', '2019-04-12 11:46:13', '2019-04-12 11:46:13'),
(7, 'Users', 'Read Users', '2019-04-12 11:46:13', '2019-04-12 11:46:13'),
(8, 'Users', 'Update Users', '2019-04-12 11:46:13', '2019-04-12 11:46:13'),
(9, 'Users', 'Delete Users', '2019-04-12 11:46:13', '2019-04-12 11:46:13'),
(10, 'Activity Logs', 'Read Activity Logs', '2019-04-12 11:46:15', '2019-04-12 11:46:15'),
(11, 'Docs', 'Create Docs', '2019-04-12 11:46:16', '2019-04-12 11:46:16'),
(12, 'Docs', 'Read Docs', '2019-04-12 11:46:16', '2019-04-12 11:46:16'),
(13, 'Docs', 'Update Docs', '2019-04-12 11:46:16', '2019-04-12 11:46:16'),
(14, 'Docs', 'Delete Docs', '2019-04-12 11:46:16', '2019-04-12 11:46:16'),
(15, 'Settings', 'Update Settings', '2019-04-12 11:46:17', '2019-04-12 11:46:17');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permission_role`
--

CREATE TABLE `permission_role` (
  `id` int(10) UNSIGNED NOT NULL,
  `permission_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `permission_role`
--

INSERT INTO `permission_role` (`id`, `permission_id`, `role_id`) VALUES
(1, 1, 2),
(2, 12, 2),
(3, 1, 3),
(4, 12, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permission_user`
--

CREATE TABLE `permission_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `permission_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `qusco_areas`
--

CREATE TABLE `qusco_areas` (
  `id` int(10) UNSIGNED NOT NULL,
  `idrut` int(11) NOT NULL,
  `lat` double NOT NULL,
  `lon` double NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `qusco_areas`
--

INSERT INTO `qusco_areas` (`id`, `idrut`, `lat`, `lon`, `created_at`, `updated_at`) VALUES
(1, 1, -11.903872391046134, -77.03361510923156, '2019-04-13 20:26:11', '2019-04-13 20:26:11'),
(2, 1, -11.905552083925645, -77.03573941877136, '2019-04-13 20:26:12', '2019-04-13 20:26:12'),
(3, 1, -11.906076985821477, -77.03235983541259, '2019-04-13 20:26:12', '2019-04-13 20:26:12'),
(4, 2, -11.901142867977887, -77.04389333417663, '2019-04-14 05:46:07', '2019-04-14 05:46:07'),
(5, 2, -11.901279344782221, -77.04468190362701, '2019-04-14 05:46:08', '2019-04-14 05:46:08'),
(6, 2, -11.900959149094643, -77.04545169761428, '2019-04-14 05:46:08', '2019-04-14 05:46:08'),
(7, 2, -11.900568089915469, -77.04461350729713, '2019-04-14 05:46:08', '2019-04-14 05:46:08'),
(8, 2, -11.899715106532884, -77.04420447042236, '2019-04-14 05:46:08', '2019-04-14 05:46:08'),
(9, 2, -11.900040552816117, -77.04374849488983, '2019-04-14 05:46:08', '2019-04-14 05:46:08'),
(10, 2, -11.901247850141154, -77.04209088971862, '2019-04-14 05:46:08', '2019-04-14 05:46:08'),
(11, 2, -11.901646781991802, -77.03711807420501, '2019-04-14 05:46:08', '2019-04-14 05:46:08'),
(12, 2, -11.901083815493232, -77.03596103629121, '2019-04-14 05:46:09', '2019-04-14 05:46:09'),
(13, 2, -11.90220056141937, -77.037250172999, '2019-04-14 05:46:09', '2019-04-14 05:46:09'),
(14, 2, -11.904979939704376, -77.0375110178257, '2019-04-14 05:46:09', '2019-04-14 05:46:09'),
(15, 2, -11.905163655870023, -77.03790396144637, '2019-04-14 05:46:09', '2019-04-14 05:46:09'),
(16, 2, -11.902985299055313, -77.03866168549308, '2019-04-14 05:46:09', '2019-04-14 05:46:09'),
(17, 2, -11.90299054812887, -77.04117893865356, '2019-04-14 05:46:09', '2019-04-14 05:46:09'),
(18, 2, -11.905142659743094, -77.04264342477569, '2019-04-14 05:46:09', '2019-04-14 05:46:09'),
(19, 2, -11.905111165549657, -77.04301356961975, '2019-04-14 05:46:09', '2019-04-14 05:46:09'),
(20, 2, -11.90271759616989, -77.0426273315216, '2019-04-14 05:46:10', '2019-04-14 05:46:10'),
(21, 2, -11.901604789192984, -77.0443010299469, '2019-04-14 05:46:10', '2019-04-14 05:46:10'),
(22, 5, -11.903242503540016, -77.03492402723083, '2019-04-15 06:55:57', '2019-04-15 06:55:57'),
(23, 5, -11.906370930440264, -77.0406103103424, '2019-04-15 06:55:57', '2019-04-15 06:55:57'),
(24, 5, -11.906391926472299, -77.03646897962341, '2019-04-15 06:55:57', '2019-04-15 06:55:57'),
(25, 18, -11.901331835842562, -77.03693031957397, '2019-05-17 10:05:32', '2019-05-17 10:05:32'),
(26, 18, -11.905804036962177, -77.04109310796508, '2019-05-17 10:05:32', '2019-05-17 10:05:32'),
(27, 18, -11.905468099528235, -77.03502058675537, '2019-05-17 10:05:32', '2019-05-17 10:05:32'),
(28, 19, -11.904901204166803, -77.04236983945617, '2019-05-17 10:11:10', '2019-05-17 10:11:10'),
(29, 19, -11.907189774487394, -77.03569650342712, '2019-05-17 10:11:10', '2019-05-17 10:11:10'),
(30, 19, -11.905258138421136, -77.0346665351654, '2019-05-17 10:11:10', '2019-05-17 10:11:10'),
(31, 20, -11.907315750275641, -77.04236983945617, '2019-05-17 10:13:42', '2019-05-17 10:13:42'),
(32, 20, -11.905573080020947, -77.04140424421081, '2019-05-17 10:13:43', '2019-05-17 10:13:43'),
(33, 20, -11.907378738147848, -77.03247785260925, '2019-05-17 10:13:43', '2019-05-17 10:13:43');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `qusco_datos_usuarios`
--

CREATE TABLE `qusco_datos_usuarios` (
  `id` int(10) UNSIGNED NOT NULL,
  `dniruc` int(11) NOT NULL,
  `direcion` text COLLATE utf8mb4_unicode_ci,
  `email` text COLLATE utf8mb4_unicode_ci,
  `nombre` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `clave` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `telefono` int(11) NOT NULL,
  `latitude` double NOT NULL,
  `longitude` double NOT NULL,
  `id_ruta` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `qr_code` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `qusco_datos_usuarios`
--

INSERT INTO `qusco_datos_usuarios` (`id`, `dniruc`, `direcion`, `email`, `nombre`, `clave`, `telefono`, `latitude`, `longitude`, `id_ruta`, `id_user`, `qr_code`, `created_at`, `updated_at`) VALUES
(1, 23123, 'sadasd', 'juan@juan.com', 'Juan Diego', '1234', 949246255, -11.11, -11.11, 0, 2, '1', '2019-04-12 11:48:38', '2019-05-17 08:04:12'),
(2, 77777, 'sdasas', 'pedro@pedro.com', 'asdasd', '1234', 4545345, -11.11, -11.11, 1, 1, '10002001', '2019-04-12 11:49:00', '2019-04-12 11:49:00'),
(3, 7889854, 'av San Gatos', 'pedro3@pedro.com', 'Pedro Lopez', '123', 94958678, -11.11, -11.11, 0, 2, '1', '2019-05-17 08:04:59', '2019-05-17 10:36:57');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `qusco_horarios`
--

CREATE TABLE `qusco_horarios` (
  `id` int(10) UNSIGNED NOT NULL,
  `idrut` int(11) NOT NULL,
  `ultimo_d` text COLLATE utf8mb4_unicode_ci,
  `hora` text COLLATE utf8mb4_unicode_ci,
  `h1` text COLLATE utf8mb4_unicode_ci,
  `h2` text COLLATE utf8mb4_unicode_ci,
  `h3` text COLLATE utf8mb4_unicode_ci,
  `h4` text COLLATE utf8mb4_unicode_ci,
  `h5` text COLLATE utf8mb4_unicode_ci,
  `h6` text COLLATE utf8mb4_unicode_ci,
  `h7` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `qusco_horarios`
--

INSERT INTO `qusco_horarios` (`id`, `idrut`, `ultimo_d`, `hora`, `h1`, `h2`, `h3`, `h4`, `h5`, `h6`, `h7`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12:12', NULL, '2019-04-13 20:26:11', '2019-04-13 20:26:11'),
(2, 2, NULL, NULL, '14:00', '09:00', NULL, '09:00', NULL, NULL, NULL, '2019-04-14 05:46:07', '2019-05-17 09:39:50'),
(3, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '14:04', NULL, '2019-04-15 06:55:57', '2019-04-15 06:55:57'),
(4, 6, NULL, NULL, NULL, '09:00', NULL, NULL, NULL, NULL, NULL, '2019-05-17 09:46:49', '2019-05-17 09:46:49'),
(5, 9, NULL, NULL, NULL, '09:00', NULL, NULL, NULL, NULL, NULL, '2019-05-17 09:47:25', '2019-05-17 09:47:25'),
(6, 11, NULL, NULL, NULL, '09:00', NULL, NULL, NULL, NULL, NULL, '2019-05-17 09:47:51', '2019-05-17 09:47:51'),
(7, 13, NULL, NULL, '09:00', NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-17 10:02:33', '2019-05-17 10:02:33'),
(8, 14, NULL, NULL, '09:00', NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-17 10:03:41', '2019-05-17 10:03:41'),
(9, 15, NULL, NULL, '09:00', NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-17 10:03:54', '2019-05-17 10:03:54'),
(10, 17, NULL, NULL, '09:00', NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-17 10:04:08', '2019-05-17 10:04:08'),
(11, 18, NULL, NULL, '09:00', NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-17 10:05:32', '2019-05-17 10:05:32'),
(12, 19, NULL, NULL, NULL, NULL, NULL, NULL, '12:00', NULL, NULL, '2019-05-17 10:11:10', '2019-05-17 10:11:10'),
(13, 20, NULL, NULL, '09:00', '08:00', NULL, NULL, NULL, NULL, NULL, '2019-05-17 10:13:42', '2019-05-18 22:47:13');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `qusco_operaciones`
--

CREATE TABLE `qusco_operaciones` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_user` int(11) NOT NULL,
  `cantidad` double DEFAULT NULL,
  `id_ruta` int(11) NOT NULL,
  `inicio` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `fin` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `qusco_operaciones`
--

INSERT INTO `qusco_operaciones` (`id`, `id_user`, `cantidad`, `id_ruta`, `inicio`, `fin`, `created_at`, `updated_at`) VALUES
(1, 2, 11, 1, '11/11/11', '', '2019-04-12 11:49:12', NULL),
(2, 2, 22, 1, '', '', '2019-04-12 12:49:12', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `qusco_recolectados`
--

CREATE TABLE `qusco_recolectados` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_user` int(11) NOT NULL,
  `cantidad` double NOT NULL,
  `ruta` text COLLATE utf8mb4_unicode_ci,
  `id_ruta` int(11) NOT NULL,
  `id_tipo` int(11) NOT NULL,
  `id_quien_recogio` int(11) NOT NULL,
  `fecha` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `qusco_recolectados`
--

INSERT INTO `qusco_recolectados` (`id`, `id_user`, `cantidad`, `ruta`, `id_ruta`, `id_tipo`, `id_quien_recogio`, `fecha`, `created_at`, `updated_at`) VALUES
(1, 10002001, 11, 'tomas', 1, 1, 1, '11/11/11', '2019-04-12 11:49:12', NULL),
(2, 10002001, 22, 'tomas', 1, 1, 1, '', '2019-04-12 12:49:12', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `qusco_rutas`
--

CREATE TABLE `qusco_rutas` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombreruta` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ubilat` double DEFAULT NULL,
  `ubilon` double DEFAULT NULL,
  `estado` int(11) NOT NULL,
  `inicio` text COLLATE utf8mb4_unicode_ci,
  `fin` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `qusco_rutas`
--

INSERT INTO `qusco_rutas` (`id`, `nombreruta`, `ubilat`, `ubilon`, `estado`, `inicio`, `fin`, `created_at`, `updated_at`) VALUES
(1, 'tomas', -11.33, -74.68, 0, NULL, NULL, '2019-04-13 20:26:11', '2019-04-13 20:26:11'),
(2, 'test', 34, 34, 0, NULL, NULL, '2019-04-14 05:46:07', '2019-04-14 05:46:07'),
(3, 'nueva ruta', -11, -74, 0, NULL, NULL, '2019-04-15 06:54:31', '2019-04-15 06:54:31'),
(5, 'nueva rutas', -11, -74, 0, NULL, NULL, '2019-04-15 06:55:56', '2019-04-15 06:55:56'),
(6, 'Rutaza', -11.2, -74.34, 0, NULL, NULL, '2019-05-17 09:46:48', '2019-05-17 09:46:48'),
(9, 'Rutazata', -16.3, -80, 0, NULL, NULL, '2019-05-17 09:47:25', '2019-05-17 09:47:25'),
(11, 'Rutazata2', -16.3, -80, 0, NULL, NULL, '2019-05-17 09:47:50', '2019-05-17 09:47:50'),
(13, 'tereda', -12.56, -47.56, 0, NULL, NULL, '2019-05-17 10:02:33', '2019-05-17 10:02:33'),
(14, 'tereda45', -12.56, -47.56, 0, NULL, NULL, '2019-05-17 10:03:41', '2019-05-17 10:03:41'),
(15, 'tereda452', -12.56, -47.56, 0, NULL, NULL, '2019-05-17 10:03:54', '2019-05-17 10:03:54'),
(17, 'tereda422', -12.56, -47.56, 0, NULL, NULL, '2019-05-17 10:04:08', '2019-05-17 10:04:08'),
(18, 'tera422', -12.56, -47.56, 0, NULL, NULL, '2019-05-17 10:05:32', '2019-05-17 10:05:32'),
(19, 'rewww', -12.56, -74.63, 0, NULL, NULL, '2019-05-17 10:11:10', '2019-05-17 10:11:10'),
(20, 'tren', NULL, NULL, 0, NULL, NULL, '2019-05-17 10:13:42', '2019-05-17 10:13:42');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `qusco_ruta_horarios`
--

CREATE TABLE `qusco_ruta_horarios` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre_ruta` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `h_inicio` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `h_fin` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_mapa` int(11) NOT NULL,
  `dias` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `qusco_tipo_residuos`
--

CREATE TABLE `qusco_tipo_residuos` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_quien` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `qusco_tipo_residuos`
--

INSERT INTO `qusco_tipo_residuos` (`id`, `nombre`, `id_quien`, `created_at`, `updated_at`) VALUES
(1, 'papel', 'general', '2019-04-12 11:49:12', '2019-04-12 11:49:12'),
(2, 'vidrio', 'general', '2019-04-12 11:49:22', '2019-04-12 11:49:22'),
(3, 'plastico', NULL, '2019-04-14 05:30:46', '2019-04-14 05:30:46'),
(4, 'primero', NULL, '2019-04-14 19:14:24', '2019-04-14 19:14:24'),
(5, 'segundo', NULL, '2019-04-14 19:14:32', '2019-04-14 19:14:32');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `qusco_ubicacions`
--

CREATE TABLE `qusco_ubicacions` (
  `id` int(10) UNSIGNED NOT NULL,
  `latitude` double NOT NULL,
  `longitude` double NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_ruta` int(11) NOT NULL,
  `direccion` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `qusco_usuarios`
--

CREATE TABLE `qusco_usuarios` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `id_tipo` int(11) NOT NULL,
  `cantidad` double NOT NULL,
  `mes` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `qusco_ventas`
--

CREATE TABLE `qusco_ventas` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_ruta` int(11) NOT NULL,
  `id_tipo` int(11) NOT NULL,
  `valor_venta` double NOT NULL,
  `fecha` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `admin` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`id`, `name`, `admin`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 1, '2019-04-12 11:46:10', '2019-04-12 11:46:10'),
(2, 'administrador', 0, '2019-04-12 11:47:17', '2019-04-12 11:47:17'),
(3, 'adm_distritos', 0, '2019-05-18 21:01:16', '2019-05-18 21:01:16');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `role_user`
--

CREATE TABLE `role_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `role_user`
--

INSERT INTO `role_user` (`id`, `role_id`, `user_id`) VALUES
(1, 1, 1),
(2, 2, 2),
(3, 3, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `settings`
--

INSERT INTO `settings` (`id`, `key`, `value`, `created_at`, `updated_at`) VALUES
(1, 'example', 'Hello World', '2019-04-12 11:46:17', '2019-04-12 11:46:17');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `timezone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'UTC'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `timezone`) VALUES
(1, 'Admin', 'admin@example.com', NULL, '$2y$10$ON/EeDx6dqyF7.ZT4OFnlOm3UWOBpwbC.VB/v48uQGl6jR4HArmWi', 'OubZJdDDXv4QB4XChFWwWD8yrt0ltf6sLgWrwykYALguiOlq9I1p4W7nUvpu', '2019-04-12 11:46:13', '2019-04-12 11:46:40', 'America/Lima'),
(2, 'jose', 'jose@jose.com', NULL, '$2y$10$2W6bN8Es7XrfZVxQC1m97.T6VDRLkLwxZP5VDLrJzDSLN2q4eoXDS', 'kWhWqcxkdc0bLTzYVPwHOgvIKhXtF4jIV4xQeil4cz93occqQoajoisNttco', '2019-04-12 11:47:38', '2019-04-12 11:47:53', 'America/Lima'),
(3, 'manhatan', 'manhattan@manhattan.com', NULL, '$2y$10$4rlKhSMA/JKb0v2APVO5wuVOOIP./CFlG.e2GpwOqUpaFJCySov/6', 'zYIN5MKJuArzaO5jgSyetu1FawDw55CRIvHFvA3tjTvrbwr6JsW9J0k5HWXr', '2019-05-18 21:02:37', '2019-05-18 21:04:53', 'America/Lima');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `activity_logs`
--
ALTER TABLE `activity_logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `activity_logs_user_id_index` (`user_id`),
  ADD KEY `activity_logs_model_id_index` (`model_id`),
  ADD KEY `activity_logs_message_index` (`message`),
  ADD KEY `activity_logs_created_at_index` (`created_at`);

--
-- Indices de la tabla `disamd_capas`
--
ALTER TABLE `disamd_capas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `disamd_mapa`
--
ALTER TABLE `disamd_mapa`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `disamd_proyectos`
--
ALTER TABLE `disamd_proyectos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `disamd_rutas`
--
ALTER TABLE `disamd_rutas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `disamd_sectors`
--
ALTER TABLE `disamd_sectors`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `docs`
--
ALTER TABLE `docs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `docs__lft__rgt_parent_id_index` (`_lft`,`_rgt`,`parent_id`),
  ADD KEY `docs_type_index` (`type`),
  ADD KEY `docs_system_index` (`system`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indices de la tabla `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permission_role_permission_id_index` (`permission_id`),
  ADD KEY `permission_role_role_id_index` (`role_id`);

--
-- Indices de la tabla `permission_user`
--
ALTER TABLE `permission_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permission_user_permission_id_index` (`permission_id`),
  ADD KEY `permission_user_user_id_index` (`user_id`);

--
-- Indices de la tabla `qusco_areas`
--
ALTER TABLE `qusco_areas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `qusco_datos_usuarios`
--
ALTER TABLE `qusco_datos_usuarios`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `qusco_datos_usuarios_dniruc_unique` (`dniruc`);

--
-- Indices de la tabla `qusco_horarios`
--
ALTER TABLE `qusco_horarios`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `qusco_operaciones`
--
ALTER TABLE `qusco_operaciones`
  ADD PRIMARY KEY (`id`),
  ADD KEY `qusco_recolectados_id_user_index` (`id_user`),
  ADD KEY `qusco_recolectados_id_ruta_index` (`id_ruta`);

--
-- Indices de la tabla `qusco_recolectados`
--
ALTER TABLE `qusco_recolectados`
  ADD PRIMARY KEY (`id`),
  ADD KEY `qusco_recolectados_id_user_index` (`id_user`),
  ADD KEY `qusco_recolectados_id_ruta_index` (`id_ruta`),
  ADD KEY `qusco_recolectados_id_tipo_index` (`id_tipo`);

--
-- Indices de la tabla `qusco_rutas`
--
ALTER TABLE `qusco_rutas`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `qusco_rutas_nombreruta_unique` (`nombreruta`);

--
-- Indices de la tabla `qusco_ruta_horarios`
--
ALTER TABLE `qusco_ruta_horarios`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `qusco_tipo_residuos`
--
ALTER TABLE `qusco_tipo_residuos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `qusco_ubicacions`
--
ALTER TABLE `qusco_ubicacions`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `qusco_usuarios`
--
ALTER TABLE `qusco_usuarios`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `qusco_ventas`
--
ALTER TABLE `qusco_ventas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `roles_name_index` (`name`),
  ADD KEY `roles_admin_index` (`admin`);

--
-- Indices de la tabla `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_user_role_id_index` (`role_id`),
  ADD KEY `role_user_user_id_index` (`user_id`);

--
-- Indices de la tabla `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `settings_key_index` (`key`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_name_index` (`name`),
  ADD KEY `users_timezone_index` (`timezone`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `activity_logs`
--
ALTER TABLE `activity_logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT de la tabla `disamd_capas`
--
ALTER TABLE `disamd_capas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `disamd_mapa`
--
ALTER TABLE `disamd_mapa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT de la tabla `disamd_proyectos`
--
ALTER TABLE `disamd_proyectos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `disamd_rutas`
--
ALTER TABLE `disamd_rutas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `disamd_sectors`
--
ALTER TABLE `disamd_sectors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `docs`
--
ALTER TABLE `docs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT de la tabla `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de la tabla `permission_role`
--
ALTER TABLE `permission_role`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `permission_user`
--
ALTER TABLE `permission_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `qusco_areas`
--
ALTER TABLE `qusco_areas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT de la tabla `qusco_datos_usuarios`
--
ALTER TABLE `qusco_datos_usuarios`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `qusco_horarios`
--
ALTER TABLE `qusco_horarios`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `qusco_operaciones`
--
ALTER TABLE `qusco_operaciones`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `qusco_recolectados`
--
ALTER TABLE `qusco_recolectados`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `qusco_rutas`
--
ALTER TABLE `qusco_rutas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT de la tabla `qusco_ruta_horarios`
--
ALTER TABLE `qusco_ruta_horarios`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `qusco_tipo_residuos`
--
ALTER TABLE `qusco_tipo_residuos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `qusco_ubicacions`
--
ALTER TABLE `qusco_ubicacions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `qusco_usuarios`
--
ALTER TABLE `qusco_usuarios`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `qusco_ventas`
--
ALTER TABLE `qusco_ventas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `role_user`
--
ALTER TABLE `role_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
