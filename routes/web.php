<?php
# @Author: bndg
# @Date:   2019-04-03T11:26:49-05:00
# @Last modified by:   bndg
# @Last modified time: 2019-11-12T17:16:29-05:00



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\Http\Controllers\PdfController;
Route::get('/', function () {
    return view('welcome');
});
Route::get('logout', 'GeneralController@logout')->name('logout');

Route::get('operaciones2', 'QuscoOperacionesController@index')->name('operacionesview');
Route::get('ubicacion/{user}', 'QuscoUbicacionController@index')->name('ubicacionview');
Route::get('rutahorario/{user}', 'QuscoRutaHorarioController@index')->name('rutahorarioview');
Route::get('datos/{user}', 'QuscoDatosUsuariosController@index')->name('datosview');
Route::get('notificaciones/', 'QuscoDatosUsuariosController@notificacionesView')->name('notificacionesview');
Route::post('d_notificacion/', 'QuscoDatosUsuariosController@dNotificacion')->name('d_notificacion');
Route::get('cambio_usuarios','QuscoDatosUsuariosController@cambioUsuarios')->name('cambiousuariosview');
Route::post('cambio_rutas','QuscoDatosUsuariosController@cambioRutas')->name('cambio_rutas');
Route::post('cambio_user_final','QuscoDatosUsuariosController@cambioUserFinal')->name('cambio_user_final');
Route::post('las_rutas','QuscoDatosUsuariosController@lasRutas')->name('las_rutas');
Route::post('las_reci','QuscoDatosUsuariosController@lasRecolectores')->name('las_reci');
Route::post('c_asignacion','QuscoDatosUsuariosController@cAsignacion')->name('c_asignacion');
Route::post('d_asignacion','QuscoDatosUsuariosController@dAsignacion')->name('d_asignacion');

Route::post('n_distrito','QuscoDatosUsuariosController@nDistrito')->name('n_distrito');
Route::get('n_distritoview','QuscoDatosUsuariosController@nDistritoView')->name('nuevodistritoview');

Route::post('generar_qr/', 'QuscoDatosUsuariosController@generarQr')->name('generar_qr');


Route::post('up_user/', 'QuscoDatosUsuariosController@up_user')->name('up_user');

Route::post('descarga/', 'QuscoDatosUsuariosController@descarga')->name('descarga');



Route::post('d_usuario/', 'QuscoDatosUsuariosController@d_usuario')->name('d_usuario');
Route::get('recolectados/', 'QuscoRecolectadosController@index')->name('recolectadosview');


Route::get('recicladores/', 'QuscoDatosUsuariosController@recicladores')->name('recicladoresview');
Route::post('createreciclador/', 'QuscoDatosUsuariosController@create')->name('createreciclador');;
Route::get('create_reciclador/', 'QuscoDatosUsuariosController@create_reciclador')->name('create_recicladorview');

Route::post('sel_rutas/', 'QuscoDatosUsuariosController@sel_rutas')->name('sel_rutas');




Route::post('/up/reciclador', 'QuscoDatosUsuariosController@u_reciclador')->name('u_reciclador');
Route::post('/up/mapa', 'QuscoRutasController@u_mapa')->name('u_mapa');


Route::get('ruta/', 'QuscoRutasController@rutas')->name('rutaview');
Route::post('createruta/', 'QuscoRutasController@create')->name('createruta');
Route::get('create_ruta/', 'QuscoRutasController@create_ruta')->name('create_rutaview');
Route::post('area_ruta/', 'QuscoRutasController@area_ruta')->name('area_ruta');
Route::post('d_ruta_fin/','QuscoRutasController@d_ruta')->name('del_ruta');

Route::get('asignacion_rutas/', 'QuscoRutasController@asignacionRutas')->name('asignacionrutasview');
Route::post('createruta/', 'QuscoRutasController@create')->name('createruta');


Route::get('tiporesiduo/', 'QuscoTipoResiduoController@index')->name('tiporesiduoview');
Route::post('createtiporesiduo/', 'QuscoTipoResiduoController@create')->name('createtiporesiduo');
Route::post('del_tipo/', 'QuscoTipoResiduoController@deleteTipo')->name('del_tipo');
Route::get('create_tiporesiduo/', 'QuscoTipoResiduoController@create_tiporesiduo')->name('create_tiporesiduoview');

Route::get('mapa/', 'DisamdMapaController@index')->name('mapaview');
Route::post('createmapa/', 'DisamdMapaController@create')->name('createmapa');;
Route::get('create_mapa/', 'DisamdMapaController@create_mapa')->name('create_mapaview');
Route::post('area_mapa/', 'DisamdMapaController@area_mapa')->name('area_mapa');

Route::get('capas/', 'DisamdCapasController@index')->name('capasview');
Route::post('create_capa/', 'DisamdCapasController@create')->name('create_capa');
Route::post('sel_sector/', 'DisamdCapasController@sel_sector')->name('sel_sector');
Route::post('c_capas/', 'DisamdCapasController@create')->name('c_capas');



Route::get('proyectos/', 'DisamdProyectoController@index')->name('proyectoview');
Route::post('create_proyecto/', 'DisamdProyectoController@create')->name('c_proyecto');

Route::get('sectors/', 'DisamdSectorController@index')->name('sectorview');
Route::post('c_sectores/', 'DisamdSectorController@create')->name('c_sectores');
Route::get('naval/', 'Formato2Controller@index')->name('navalview');

Route::get('rutausuarios/', 'QuscoDatosUsuariosController@rutausuariosView')->name('rutausuariosview');
Route::post('rutasdistrito/', 'QuscoDatosUsuariosController@rutasDistrito')->name('rutas_distrito');
Route::post('usuariosDistritos/', 'QuscoDatosUsuariosController@usuariosDistritos')->name('usuarios_distritos');
Route::post('asignarAreaUser/', 'QuscoDatosUsuariosController@asignarAreaUser')->name('asignar_area_user');


Route::get('customers/', 'PdfController@index')->name('pdfIndex');
Route::get('customers/pdf','PdfController@download');

//Route::resource('operaciones','QuscoUsuariosController');
