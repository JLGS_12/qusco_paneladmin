<?php
# @Author: bndg
# @Date:   2019-11-12T14:24:58-05:00
# @Last modified by:   bndg
# @Last modified time: 2019-11-12T15:14:29-05:00




namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;

use Kjjdion\LaravelAdminPanel\Traits\AdminUser;
use Kjjdion\LaravelAdminPanel\Traits\DynamicFillable;
use Kjjdion\LaravelAdminPanel\Traits\UserTimezone;

class qusco_ruta_recolectores extends Model
{
    use Notifiable, AdminUser, DynamicFillable, UserTimezone;
    //
    protected $fillable = [
        'idRuta', 'idReco',
    ];
}
