<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class qusco_recolectados extends Model
{
    //

    public function qusco_tipo_residuo()
    {
        return $this->hasMany('App\qusco_tipo_residuo','id');
    }

    public function qusco_datos_usuarios()
    {
        return $this->hasMany('App\qusco_datos_usuarios','id')->select(['id', 'dniruc']);
    }
}
