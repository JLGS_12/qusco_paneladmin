<?php
# @Author: bndg
# @Date:   2019-04-05T09:54:04-05:00
# @Last modified by:   bndg
# @Last modified time: 2019-04-05T10:18:02-05:00




namespace App;

use Illuminate\Database\Eloquent\Model;

class qusco_usuarios extends Model
{
    //
    protected $fillable = [
        'id_tipo', 'cantidad'
    ];


    public function qusco_tipo_residuo()
    {
        return $this->belongsTo('App\qusco_tipo_residuo','id');
    }
}
