<?php
# @Author: bndg
# @Date:   2019-04-11T10:21:42-05:00
# @Last modified by:   bndg
# @Last modified time: 2019-04-11T13:44:09-05:00




namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;

use Kjjdion\LaravelAdminPanel\Traits\AdminUser;
use Kjjdion\LaravelAdminPanel\Traits\DynamicFillable;
use Kjjdion\LaravelAdminPanel\Traits\UserTimezone;

class qusco_rutas extends Model
{
    use Notifiable, AdminUser, DynamicFillable, UserTimezone;
    //
    protected $fillable = [
        'nombreruta', 'ubilat', 'ubilon',
    ];

    public function qusco_horarios()
    {
        return $this->hasMany('App\qusco_horarios','idrut');
    }
}
