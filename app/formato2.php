<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;

use Kjjdion\LaravelAdminPanel\Traits\AdminUser;
use Kjjdion\LaravelAdminPanel\Traits\DynamicFillable;
use Kjjdion\LaravelAdminPanel\Traits\UserTimezone;

class formato2 extends Model
{
  use Notifiable, AdminUser, DynamicFillable, UserTimezone;
  //
  protected $fillable = [
      'id',  'usuario',  'lat',  'lon',  'time',  'encry',  'id_operador',
      'd0',  'd1',  'd2',  'd3',  'd4',  'd5',  'd6',  'd7',  'd8',  'd9',
      'd10',  'd11',  'd12',  'd13',  'd14',  'd15',  'd16',  'd17',  'd18',
      'd19',  'd20',  'd21',  'd22',  'd23',  'd24',  'd25',  'd26',  'd27',
      'd28',  'd29',  'd30',  'd31',  'd32',  'd33',  'd34',  'd35'
  ];


}
