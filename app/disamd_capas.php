<?php

namespace App;

use Illuminate\Database\Eloquent\Model;



use Illuminate\Notifications\Notifiable;


use Kjjdion\LaravelAdminPanel\Traits\AdminUser;
use Kjjdion\LaravelAdminPanel\Traits\DynamicFillable;
use Kjjdion\LaravelAdminPanel\Traits\UserTimezone;

class disamd_capas extends Model
{
    //
    use Notifiable, AdminUser, DynamicFillable, UserTimezone;
    //
    protected $fillable = [
        'nom_capa', 'descripcion', 'estado','idsector',
    ];
}
