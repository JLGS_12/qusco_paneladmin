<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class qusco_tipo_residuo extends Model
{
    //


    protected $fillable = [
        'nombre', 'id_quien','idDistrito'
    ];

    public function qusco_recolectados()
    {
        return $this->hasMany('App\qusco_recolectados','id_tipo');
    }


}
