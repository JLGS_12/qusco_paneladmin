<?php
# @Author: bndg
# @Date:   2019-04-05T13:40:58-05:00
# @Last modified by:   bndg
# @Last modified time: 2019-04-12T12:07:44-05:00




namespace App\Http\Controllers;

use App\qusco_ruta_horario;
use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class QuscoRutaHorarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function __construct()
     {
         /*$this->middleware(['auth_admin', 'can:Access Admin Panel'])->except(['frontend']);
         $this->middleware('intend_url')->only(['index', 'read']);
         $this->middleware('can:Create Docs')->only(['createForm', 'create']);*/
         $this->middleware('can:Read Docs')->only(['index', 'read']);
         /*$this->middleware('can:Update Docs')->only(['updateForm', 'update', 'move']);
         $this->middleware(['can:Delete Docs', 'not_system_doc'])->only('delete');*/
     }


    public function index()
    {
        //
        $name = auth()->user()->id;
        $role = auth()->user()->roles[0]->name;
       if($role ==='administrador'){
         $operaciones = qusco_ruta_horario::latest()->paginate(15);

         return view('ruta_horario.index',compact('operaciones'))
             ->with('i', (request()->input('page', 1) - 1) * 5);

       }else{
           return view('lap::backend.dashboard');
       }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\qusco_ruta_horario  $qusco_ruta_horario
     * @return \Illuminate\Http\Response
     */
    public function show(qusco_ruta_horario $qusco_ruta_horario)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\qusco_ruta_horario  $qusco_ruta_horario
     * @return \Illuminate\Http\Response
     */
    public function edit(qusco_ruta_horario $qusco_ruta_horario)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\qusco_ruta_horario  $qusco_ruta_horario
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, qusco_ruta_horario $qusco_ruta_horario)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\qusco_ruta_horario  $qusco_ruta_horario
     * @return \Illuminate\Http\Response
     */
    public function destroy(qusco_ruta_horario $qusco_ruta_horario)
    {
        //
    }
}
