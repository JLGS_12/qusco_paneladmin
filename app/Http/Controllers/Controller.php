<?php
# @Author: bndg
# @Date:   2019-04-03T12:00:18-05:00
# @Last modified by:   bndg
# @Last modified time: 2019-04-03T15:17:45-05:00




namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
   {
       $this->middleware(['auth', 'can:Admin']);
   }

    public function inicio() {

        return view('operaciones/index');
    }
}
