<?php
# @Author: bndg
# @Date:   2019-04-05T09:54:04-05:00
# @Last modified by:   bndg
# @Last modified time: 2019-04-12T12:08:06-05:00




namespace App\Http\Controllers;

use App\qusco_tipo_residuo;
use App\qusco_usuario_distrito;
use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class QuscoTipoResiduoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function __construct()
     {
         /*$this->middleware(['auth_admin', 'can:Access Admin Panel'])->except(['frontend']);
         $this->middleware('intend_url')->only(['index', 'read']);
         $this->middleware('can:Create Docs')->only(['createForm', 'create']);*/
         $this->middleware('can:Read Docs')->only(['index', 'read']);
         /*$this->middleware('can:Update Docs')->only(['updateForm', 'update', 'move']);
         $this->middleware(['can:Delete Docs', 'not_system_doc'])->only('delete');*/
     }



    public function index()
    {
        //
        $name = auth()->user()->id;
        $role = auth()->user()->roles[0]->name;
      if($role ==='administrador'){
        $idDistrito = qusco_usuario_distrito::where('idUsuario',$name)->get(['idDistrito'])[0]->idDistrito;
         $operaciones = qusco_tipo_residuo::where('idDistrito',$idDistrito)->latest()->paginate(15);

         return view('tipo_residuo.index',compact('operaciones'))
             ->with('i', (request()->input('page', 1) - 1) * 5);

       }else{
           return view('lap::backend.dashboard');
       }
    }




    public function create_tiporesiduo()
    {
        return view('tipo_residuo.create');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function create()
     {
       $name = auth()->user()->id;
   $idDistrito = qusco_usuario_distrito::where('idUsuario',$name)->get(['idDistrito'])[0]->idDistrito;
  // echo $idDistrito;
         $this->validate(request(), [
             'nombre' => 'required',
             //'id_quien' => 'required',
         ]);
         $data = array_merge(request()->all(), [
             'idDistrito' => $idDistrito
         ]);
         $id = qusco_tipo_residuo::create($data);
         flash(['success', 'Nuevo tipo Creado!'  ]);
         if (request()->input('_submit') == 'redirect') {
             return response()->json(['redirect' => session()->pull('url.intended', route('tiporesiduoview') )]);
         }
         else {
             return response()->json(['reload_page' => true]);
         }
     }

     public function deleteTipo(Request $req){
       $role = auth()->user()->roles[0]->name;
       if($role ==='administrador'){
         $name = auth()->user()->id;
         $idDistrito = qusco_usuario_distrito::where('idUsuario',$name)->get(['idDistrito'])[0]->idDistrito;
         $id = $req->id;
         $operaciones = qusco_tipo_residuo::where('id', $id)->
         where('idDistrito', $idDistrito)->
         delete();
         return response()->json("Actualizacion completada");
       }else{
         return response()->json("error");

       }
     }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\qusco_tipo_residuo  $qusco_tipo_residuo
     * @return \Illuminate\Http\Response
     */
    public function show(qusco_tipo_residuo $qusco_tipo_residuo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\qusco_tipo_residuo  $qusco_tipo_residuo
     * @return \Illuminate\Http\Response
     */
    public function edit(qusco_tipo_residuo $qusco_tipo_residuo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\qusco_tipo_residuo  $qusco_tipo_residuo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, qusco_tipo_residuo $qusco_tipo_residuo)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\qusco_tipo_residuo  $qusco_tipo_residuo
     * @return \Illuminate\Http\Response
     */
    public function destroy(qusco_tipo_residuo $qusco_tipo_residuo)
    {
        //
    }



}
