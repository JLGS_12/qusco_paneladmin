<?php

namespace App\Http\Controllers;

use App\qusco_areas;
use Illuminate\Http\Request;

class QuscoAreasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\qusco_areas  $qusco_areas
     * @return \Illuminate\Http\Response
     */
    public function show(qusco_areas $qusco_areas)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\qusco_areas  $qusco_areas
     * @return \Illuminate\Http\Response
     */
    public function edit(qusco_areas $qusco_areas)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\qusco_areas  $qusco_areas
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, qusco_areas $qusco_areas)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\qusco_areas  $qusco_areas
     * @return \Illuminate\Http\Response
     */
    public function destroy(qusco_areas $qusco_areas)
    {
        //
    }
}
