<?php
# @Author: bndg
# @Date:   2019-04-05T13:41:44-05:00
# @Last modified by:   bndg
# @Last modified time: 2019-11-12T17:37:47-05:00




namespace App\Http\Controllers;

use App\qusco_datos_usuarios;
use App\qusco_usuario_distrito;
use App\qusco_rutas;
use App\qusco_distrito;
use App\qusco_ruta_recolectores;
use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\DB;
use App\User;
use Illuminate\Support\Facades\Hash;

class QuscoDatosUsuariosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function __construct()
     {
         /*$this->middleware(['auth_admin', 'can:Access Admin Panel'])->except(['frontend']);
         $this->middleware('intend_url')->only(['index', 'read']);
         $this->middleware('can:Create Docs')->only(['createForm', 'create']);*/
         $this->middleware('can:Read Docs')->only(['index', 'read']);
         /*$this->middleware('can:Update Docs')->only(['updateForm', 'update', 'move']);
         $this->middleware(['can:Delete Docs', 'not_system_doc'])->only('delete');*/
     }

    public function index()
    {
        //

        $name = auth()->user()->id;
        $role = auth()->user()->roles[0]->name;
        // $idDistrito = qusco_usuario_distrito::where('idUsuario',$name)->get(['idDistrito'])[0]->idDistrito;
        $idDistrito = auth()->user()->id;
       if($role ==='administrador'){
         $operaciones = qusco_datos_usuarios::select('qusco_datos_usuarios.*','qusco_rutas.nombreruta')->
         join('qusco_rutas','qusco_datos_usuarios.id_ruta',"=",'qusco_rutas.id')->
         where('id_user','!=', 2)->
         where('qusco_datos_usuarios.idDistrito','=', $idDistrito)->
         get();
         return view('datos.index',compact('operaciones'));
            // ->with('i', (request()->input('page', 1) - 1) * 20);

       }else{
           return view('lap::backend.dashboard');
       }

    }

    public function notificacionesView()
    {
        //

        $name = auth()->user()->id;
        $role = auth()->user()->roles[0]->name;
        $idDistrito = qusco_usuario_distrito::where('idUsuario',$name)->get(['idDistrito'])[0]->idDistrito;
       if($role ==='administrador'){
         $operaciones = qusco_datos_usuarios::select('qusco_datos_usuarios.*','qusco_rutas.nombreruta')->join('qusco_rutas','qusco_datos_usuarios.id_ruta',"=",'qusco_rutas.id')->where('id_user','=', 3)->where('qusco_datos_usuarios.idDistrito','=', $idDistrito)->get();//->latest('dniruc')->paginate(20);
         return view('notificaciones.index',compact('operaciones'));
            // ->with('i', (request()->input('page', 1) - 1) * 20);

       }else{
           return view('lap::backend.dashboard');
       }

    }


    public function recicladores()
    {
        $name = auth()->user()->id;
        $role = auth()->user()->roles[0]->name;
          $idDistrito = qusco_usuario_distrito::where('idUsuario',$name)->get(['idDistrito'])[0]->idDistrito;
        if($role ==='administrador'){
          $operaciones = qusco_datos_usuarios::select('qusco_datos_usuarios.*')->
        //  join('qusco_rutas','qusco_datos_usuarios.id_ruta',"=",'qusco_rutas.id')->
          where('id_user', 2)->get();//->latest()->paginate(20);
          return view('recicladores.index',compact('operaciones'));
              //->with('i', (request()->input('page', 1) - 1) * 20);
       }else{
           return view('lap::backend.dashboard');
       }
    }

    public function create_reciclador()
    {
        return view('recicladores.create');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $name = auth()->user()->id;
    $idDistrito = qusco_usuario_distrito::where('idUsuario',$name)->get(['idDistrito'])[0]->idDistrito;

        $this->validate(request(), [
            'nombre' => 'required',
            'email' => 'required|email',//|unique:qusco_datos_usuarios',
            'clave' => 'required',//|confirmed',
              'direcion' => 'required',//|confirmed',
              'dniruc' => 'required',//|confirmed',
              'telefono' => 'required',//|confirmed',
              'clave_confirmation' => 'required',//|confirmed',

        ]);

        $data = array_merge(request()->all(), [
            //'clave' => Hash::make(request()->input('clave')),
            'latitude' => -11.11,
            'longitude' => -11.11,
            'id_user' => 2,
            'qr_code' => 1,
            'idDistrito' => $idDistrito
        ]);
        try {
          $user = qusco_datos_usuarios::create($data);

          flash(['success', 'Reciclador Creado!']);
        } catch (\Exception $e) {
          //flash('Message')->error();
          //return response()->json(['sad'=>'asd']);
         flash([ 'danger','DNI/RUC o email duplicados']);
        }



        if (request()->input('_submit') == 'redirect') {
            return response()->json(['redirect' => session()->pull('url.intended', route('recicladoresview'))]);
        }
        else {
            return response()->json(['reload_page' => true]);
        }
    }

    public function u_reciclador(Request $req){
      $role = auth()->user()->roles[0]->name;
      if($role ==='administrador'){
        $dni = $req->dni;
          $update1 =['nombre' =>  $req->nombre ,
                      'clave' =>  $req->clave ,
                      'email' =>  $req->email ,
                    'telefono' =>  $req->telefono ];
          $operaciones = qusco_datos_usuarios::where('dniruc', $dni)->update($update1);
        return response()->json("Actualizacion completada" );
      }
      return response()->json("error");
    }

    public function up_user(Request $req){
      $role = auth()->user()->roles[0]->name;
      if($role ==='administrador'){
        $name = auth()->user()->id;
      $idDistrito = qusco_usuario_distrito::where('idUsuario',$name)->get(['idDistrito'])[0]->idDistrito;
            $update1 =[
                        'nombre' => $req->nombre,
                        'dniruc' =>  $req->dni,
                        'email' =>  $req->email,
                        'telefono' =>  $req->telefono,
                        'clave' =>  $req->clave,
                        'direcion' =>  $req->direccion,
                     ];
             if(!empty($req->dni)){
                 $operaciones = qusco_datos_usuarios::where('dniruc', $req->dni)->
                 where('idDistrito',$idDistrito)->
                 update($update1);
             }else{
                 $operaciones = qusco_datos_usuarios::where('email', $req->email)->
                 where('idDistrito',$idDistrito)->
                 update($update1);
             }

          return response()->json("Actualizacion completada");
        }
        return response()->json("error");
    //return response()->json($req->all());
    }

    public function generarQr(Request $req){
      $name = auth()->user()->id;
      $role = auth()->user()->roles[0]->name;
      $idDistrito = qusco_usuario_distrito::where('idUsuario',$name)->get(['idDistrito'])[0]->idDistrito;
      if($role ==='administrador'){
        $dni=$req->dni;
        try {
          $operaciones = qusco_datos_usuarios::
          where('dniruc','=', $dni)->where('idDistrito','=', $idDistrito)->get(['qr_code'])[0];
          return response()->json($operaciones);
         } catch (Exception $e) {
             return response()->json("error");
         }

     }else{
    //   flash(['success', 'Reciclador Creado!']);
       return response()->json("error");
        // return view('lap::backend.dashboard');
     }

  //    return response()->json("holi");
    }

    public function d_usuario(Request $req){
      $name = auth()->user()->id;
      $role = auth()->user()->roles[0]->name;
      $idDistrito = qusco_usuario_distrito::where('idUsuario',$name)->get(['idDistrito'])[0]->idDistrito;
      if($role ==='administrador'){
      $dni = $req->id;
        $operaciones = qusco_datos_usuarios::where('dniruc', $dni)->delete();
      return response()->json("Actualizacion completada");
    }else{
      return response()->json("error");

    }
    }

    public function dNotificacion(Request $req){
      $name = auth()->user()->id;
      $role = auth()->user()->roles[0]->name;
      $idDistrito = qusco_usuario_distrito::where('idUsuario',$name)->get(['idDistrito'])[0]->idDistrito;
      if($role ==='administrador'){
      $dni = $req->id;
      $update1 = ["id_user"=>"1"];
        $operaciones = qusco_datos_usuarios::where('dniruc', $dni)->update($update1);
      return response()->json("Actualizacion completada");
    }else{
      return response()->json("error");

    }
    }

    public function cambioUsuarios ( Request $req){

      $operaciones = qusco_datos_usuarios::select('qusco_datos_usuarios.*','qusco_rutas.nombreruta')->join('qusco_rutas','qusco_datos_usuarios.id_ruta',"=",'qusco_rutas.id')->where('id_user','!=', 2)->get();//->latest()->paginate(20);
      return view('cambio.index',compact('operaciones'));
        //  ->with('i', (request()->input('page', 1) - 1) * 20);

    }
    public function cambioRutas(Request $req){

      $operaciones = qusco_rutas::select('id','nombreruta')->latest()->paginate(25);
      return response()->json($operaciones);
    }

    public function lasRutas(Request $req){
        $name = auth()->user()->id;
        $idDistrito = qusco_usuario_distrito::where('idUsuario',$name)->get(['idDistrito'])[0]->idDistrito;
      $operaciones = qusco_rutas::select('id','nombreruta')->where('idDistrito',$idDistrito)->latest()->paginate(45);
      return response()->json($operaciones);
    }

    public function lasRecolectores(Request $req){
        $name = auth()->user()->id;
        $idDistrito = qusco_usuario_distrito::where('idUsuario',$name)->get(['idDistrito'])[0]->idDistrito;
        $operaciones = qusco_datos_usuarios::select('id','dniruc','nombre')->
        //where('idDistrito',$idDistrito)->
        where('id_user',2)->
        latest()->paginate(45);
        return response()->json($operaciones);
    }
    public function cAsignacion(Request $req){
        $name = auth()->user()->id;
        $idDistrito = qusco_usuario_distrito::where('idUsuario',$name)->get(['idDistrito'])[0]->idDistrito;
        $data=['idRuta'=>$req->idRuta, 'idReco'=>$req->idReco];
        $user = qusco_ruta_recolectores::create($data);
    }

    public function dAsignacion(Request $req){
      $name = auth()->user()->id;
      $role = auth()->user()->roles[0]->name;
      $idDistrito = qusco_usuario_distrito::where('idUsuario',$name)->get(['idDistrito'])[0]->idDistrito;
      if($role ==='administrador'){
      $id = $req->id;
        $operaciones = qusco_ruta_recolectores::where('id', $id)->delete();
      return response()->json("Actualizacion completada");
    }else{
      return response()->json("error");

    }
    }

    public function cambioUserFinal (Request $req){

      $idDistrito = qusco_rutas::where('id',$req->id_ruta)->get(['idDistrito'])[0]->idDistrito;
      $update1 =[
                  'id_ruta' => $req->id_ruta,
                  'idDistrito' => $idDistrito,

               ];
      $operaciones = qusco_datos_usuarios::where('dniruc', $req->dni)->update($update1);

    return response()->json("Actualizacion completada");
    }

    public function rutausuariosView(Request $req){
      $id = auth()->user()->id;
      $role = auth()->user()->roles[0]->name;
      $idDistrito = qusco_usuario_distrito::where('idUsuario',$id)->get(['idDistrito'])[0]->idDistrito;
     if($role ==='administrador'){
       $operaciones = qusco_datos_usuarios::select('qusco_datos_usuarios.*','qusco_rutas.nombreruta')->join('qusco_rutas','qusco_datos_usuarios.id_ruta',"=",'qusco_rutas.id')->where('id_user','!=', 2)->where('qusco_datos_usuarios.idDistrito','=', $idDistrito)->get();//->latest('dniruc')->paginate(20);
       //,compact('operaciones')
       return view('rutausuarios.index');
          // ->with('i', (request()->input('page', 1) - 1) * 20);
     }else{
         return view('lap::backend.dashboard');
     }

    }

    public function rutasDistrito(Request $req){
      $id = auth()->user();
      /* $rutas = qusco_rutas::where('idDistrito',2)->get();      
      return response()->json($rutas); */

      try {
        //$ubi = DB::table('qusco_areas')->select(['idrut','lat','lon'])->orderBy('idrut')->where('idrut','!=',0)->get();
        $ubi = DB::table('qusco_rutas')->
        select(['idrut','nombreruta','lat','lon','idDistrito'])->
        orderBy('qusco_areas.id')->
        join('qusco_areas','qusco_rutas.id','=','qusco_areas.idrut')->where('idDistrito',2)->get();

      } catch (\Exception $e) {
          return response()->json(['error'=>'Hubo un Error']);
      }
      return response()->json($ubi);
    }
    public function usuariosDistritos(Request $req){
      $operaciones = qusco_datos_usuarios::select('qusco_datos_usuarios.*')->
     // join('qusco_rutas','qusco_datos_usuarios.id_ruta','=','qusco_rutas.id')->
      where('id_user','!=', 2)->
      whereNotIn('id_ruta', function($query) {
        $query->select('id')
              ->from('qusco_rutas');
      })->get();
      return response()->json($operaciones);
    }

    public function asignarAreaUser(Request $req){
      $idDistrito = auth()->user()->id;
      $idUser = $req->idUser;
      $idArea = $req->idArea;
      $operaciones = qusco_datos_usuarios::where('id',$idUser)->update(['id_ruta'=>$idArea,'idDistrito'=>$idDistrito]);
      return response()->json(['sx'=>$req->idArea]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\qusco_datos_usuarios  $qusco_datos_usuarios
     * @return \Illuminate\Http\Response
     */
    public function show(qusco_datos_usuarios $qusco_datos_usuarios)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\qusco_datos_usuarios  $qusco_datos_usuarios
     * @return \Illuminate\Http\Response
     */
    public function edit(qusco_datos_usuarios $qusco_datos_usuarios)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\qusco_datos_usuarios  $qusco_datos_usuarios
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, qusco_datos_usuarios $qusco_datos_usuarios)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\qusco_datos_usuarios  $qusco_datos_usuarios
     * @return \Illuminate\Http\Response
     */
    public function destroy(qusco_datos_usuarios $qusco_datos_usuarios)
    {
        //
    }


    public function sel_rutas(Request $request){

      $id2 = DB::table('qusco_rutas')->select('id','nombreruta')->get();
      return response()->json($id2);

    }

    public function descarga(Request $req){
      /*header("Content-type: application/vnd.ms-excel; name='excel'");
      header("Content-Disposition: filename=ficheroExcel.xls");
      header("Pragma: no-cache");
      header("Expires: 0");*/
      return view('excel.index')
          ->with('sol',$req->datos_a_enviar);
      return $req->datos_a_enviar;
      echo $_POST['datos_a_enviar'];
    }

    public function nDistritoView(Request $req){
      $email = auth()->user()->email;
      if($email=="miguel@miguel.com"){
        $name = auth()->user()->id;
        $role = auth()->user()->roles[0]->name;
        $idDistrito = qusco_usuario_distrito::where('idUsuario',$name)->get(['idDistrito'])[0]->idDistrito;
       if($role ==='administrador'){
         $operaciones = DB::table('qusco_usuario_distritos')->select('users.email','qusco_distritos.Distrito')->
         join('users','users.id','=','qusco_usuario_distritos.idUsuario')->
         join('qusco_distritos','qusco_distritos.id','=','qusco_usuario_distritos.idDistrito')->
         get();
         return view('nuevoDistrito.index',compact('operaciones'));
       }else{
           return view('lap::backend.dashboard');
       }

      }

    }

    public function nDistrito(Request $req){
      $email = auth()->user()->email;

      if($email=="miguel@miguel.com"){
        $tipo = $req->tipo;
        if($tipo=="new"){

          $nuevo = User::create([
              'name' => $req->name,
              'email' => $req->email,
              'password' => Hash::make($req->password),
          ]);
          $distrito = qusco_distrito::create([
            'Distrito'=>$req->Distrito
          ]);
          DB::table('role_user')->insert([
            'role_id'=>'2','user_id'=>$nuevo->id
          ]);
          $final = qusco_usuario_distrito::create([
            'idDistrito' => $distrito->id,
            'idUsuario' => $nuevo->id
          ]);
          return response()->json(['mensaje'=>'exitoso']);
        }else if($tipo ="update" ){
          User::where('email',$req->email)->update(['password' => Hash::make($req->password)]);
          return response()->json(['mensaje'=>'exitoso']);
        }
      }
    }
}
