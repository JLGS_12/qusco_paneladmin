<?php

namespace App\Http\Controllers;

use App\qusco_distrito;
use Illuminate\Http\Request;

class QuscoDistritoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\qusco_distrito  $qusco_distrito
     * @return \Illuminate\Http\Response
     */
    public function show(qusco_distrito $qusco_distrito)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\qusco_distrito  $qusco_distrito
     * @return \Illuminate\Http\Response
     */
    public function edit(qusco_distrito $qusco_distrito)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\qusco_distrito  $qusco_distrito
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, qusco_distrito $qusco_distrito)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\qusco_distrito  $qusco_distrito
     * @return \Illuminate\Http\Response
     */
    public function destroy(qusco_distrito $qusco_distrito)
    {
        //
    }
}
