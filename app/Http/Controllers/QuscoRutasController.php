<?php
# @Author: bndg
# @Date:   2019-04-11T10:21:49-05:00
# @Last modified by:   bndg
# @Last modified time: 2019-11-12T15:28:42-05:00




namespace App\Http\Controllers;

use App\qusco_rutas;
use App\qusco_horarios;
use App\qusco_areas;
use App\qusco_usuario_distrito;
use App\qusco_datos_usuarios;
use App\qusco_ruta_recolectores;
use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;



class QuscoRutasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function __construct()
     {
         /*$this->middleware(['auth_admin', 'can:Access Admin Panel'])->except(['frontend']);
         $this->middleware('intend_url')->only(['index', 'read']);
         $this->middleware('can:Create Docs')->only(['createForm', 'create']);*/
         $this->middleware('can:Read Docs')->only(['index', 'read']);
         /*$this->middleware('can:Update Docs')->only(['updateForm', 'update', 'move']);
         $this->middleware(['can:Delete Docs', 'not_system_doc'])->only('delete');*/
     }

      public function d_ruta(Request $req){
        //  return response()->json($req);
      $role = auth()->user()->roles[0]->name;

      if($role ==='administrador'){
      $id = $req->id;

        $operaciones = qusco_rutas::where('id', $id)->delete();
      return response()->json("Actualizacion completada");
    }else{
      return response()->json("error");

    }
    }

    public function index()
    {
        //
            $name = auth()->user()->id;
            $role = auth()->user()->roles[0]->name;
            $idDistrito = qusco_usuario_distrito::where('idUsuario',$name)->get(['idDistrito'])[0]->idDistrito;
           if($role ==='administrador'){
             $operaciones = qusco_rutas::select('qusco_rutas.*','qusco_horarios.h6')->join('qusco_horarios','qusco_rutas.id','=','qusco_horarios.idrut' )->where('idDistrito','=', $idDistrito)->latest()->paginate(15);

             return view('rutas.index',compact('operaciones'))
                 ->with('i', (request()->input('page', 1) - 1) * 5);

           }else{
               return view('lap::backend.dashboard');
           }
    }

    public function rutas()
    {
        $name = auth()->user()->id;
        $role = auth()->user()->roles[0]->name;
        $idDistrito = qusco_usuario_distrito::where('idUsuario',$name)->get(['idDistrito'])[0]->idDistrito;
        if($role ==='administrador'){
         $operaciones = qusco_rutas::select('qusco_rutas.*','qusco_horarios.h1'
         ,'qusco_horarios.h2','qusco_horarios.h3','qusco_horarios.h4','qusco_horarios.h5'
         ,'qusco_horarios.h6','qusco_horarios.h7')->join('qusco_horarios','qusco_rutas.id','=','qusco_horarios.idrut' )->where('idDistrito','=', $idDistrito)->latest()->paginate(15);
         return view('rutas.index',compact('operaciones'))
             ->with('i', (request()->input('page', 1) - 1) * 5);
       }else{
           return view('lap::backend.dashboard');
       }
    }

    public function asignacionRutas(){
        $name = auth()->user()->id;
        $role = auth()->user()->roles[0]->name;
        $idDistrito = qusco_usuario_distrito::where('idUsuario',$name)->get(['idDistrito'])[0]->idDistrito;
        if($role ==='administrador'){

        $operaciones  = qusco_ruta_recolectores::select('qusco_ruta_recolectores.id','qusco_rutas.nombreruta','qusco_datos_usuarios.nombre')->
                        join('qusco_rutas','qusco_ruta_recolectores.idRuta','=','qusco_rutas.id')->
                        join('qusco_datos_usuarios','qusco_ruta_recolectores.idReco','=','qusco_datos_usuarios.id')->
                        Where('qusco_rutas.idDistrito',$idDistrito)->
                        get();

         return view('rutas_recolectores.index',compact('operaciones'));
       }else{
           return view('lap::backend.dashboard');
       }
    }


    public function create_ruta()
    {
        return view('rutas.create');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
      //  $name = auth()->user()->id;

        $name = auth()->user()->id;
        $role = auth()->user()->roles[0]->name;

       if($role ==='administrador')
        {    $idDistrito = qusco_usuario_distrito::where('idUsuario',$name)->get(['idDistrito'])[0]->idDistrito;
          $this->validate(request(), [
            'nombreruta' => 'required',

            'area' => 'required',//|confirmed',
        ]);
        $data = array_merge( [
            'nombreruta'=> request()->input('nombreruta'),
            'estado' => 0,
            'idDistrito' =>  $idDistrito
        ]);

        $id = qusco_rutas::create($data)->id;

        $data2 = array_merge( [
            'idrut'=> $id,
            'h1'=> request()->input('h1'),
            'h2'=> request()->input('h2'),
            'h3'=> request()->input('h3'),
            'h4'=> request()->input('h4'),
            'h5'=> request()->input('h5'),
            'h6'=> request()->input('h6'),
            'h7'=> request()->input('h7'),

        ]);

        $user = qusco_horarios::create($data2);


        $area = json_decode(request()->input('area'),true);
        foreach($area as $item) {
          error_log($item["lat"]);
          $data3 = [];
          $data3 = array_merge( [
              'idrut'=> $id,
              'lat'=> $item["lat"],
              'lon'=> $item["lng"]
          ]);
          $user = qusco_areas::create($data3);
        }

        flash(['success', 'User created! ' .$id ]);

        if (request()->input('_submit') == 'redirect') {
            return response()->json(['redirect' => session()->pull('url.intended', route('rutaview'))]);
        }
        else {
            return response()->json(['reload_page' => true]);
        }}
    }


    public function u_mapa(Request $req){
      $name = auth()->user()->id;
      $role = auth()->user()->roles[0]->name;
     if($role ==='administrador')
        {$nombre = $req->nombre;
      $id = qusco_rutas::select('id')->where('nombreruta', $nombre)->first();
          $update1 =['h1' =>  $req->h1 ,
                    'h2' =>  $req->h2 ,
                  'h3' =>  $req->h3 ,
                'h4' =>  $req->h4 ,
              'h5' =>  $req->h5 ,
            'h6' =>  $req->h6 ,
          'h7' =>  $req->h7  ];
          $operaciones = qusco_horarios::where('idrut', $id->id)->update($update1);
        return response()->json("Actualizacion completada" .$id);}
    }

    public function area_ruta(Request $req){
         $name = auth()->user()->id;
      $role = auth()->user()->roles[0]->name;
     if($role ==='administrador'){
       $nombre = $req->nombre;
       $id = qusco_rutas::select('id')->where('nombreruta', $nombre)->first()->id;
       $id2 = qusco_areas::select('lat','lon')->where('idrut', $id)->get();
       return response()->json($id2);
     }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\qusco_rutas  $qusco_rutas
     * @return \Illuminate\Http\Response
     */
    public function show(qusco_rutas $qusco_rutas)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\qusco_rutas  $qusco_rutas
     * @return \Illuminate\Http\Response
     */
    public function edit(qusco_rutas $qusco_rutas)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\qusco_rutas  $qusco_rutas
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, qusco_rutas $qusco_rutas)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\qusco_rutas  $qusco_rutas
     * @return \Illuminate\Http\Response
     */
    public function destroy(qusco_rutas $qusco_rutas)
    {
        //
    }
}
