<?php

namespace App\Http\Controllers;

use App\disamd_rutas;
use Illuminate\Http\Request;

class DisamdRutasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\disamd_rutas  $disamd_rutas
     * @return \Illuminate\Http\Response
     */
    public function show(disamd_rutas $disamd_rutas)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\disamd_rutas  $disamd_rutas
     * @return \Illuminate\Http\Response
     */
    public function edit(disamd_rutas $disamd_rutas)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\disamd_rutas  $disamd_rutas
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, disamd_rutas $disamd_rutas)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\disamd_rutas  $disamd_rutas
     * @return \Illuminate\Http\Response
     */
    public function destroy(disamd_rutas $disamd_rutas)
    {
        //
    }
}
