<?php

namespace App\Http\Controllers;

use App\qusco_venta;
use Illuminate\Http\Request;

class QuscoVentaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\qusco_venta  $qusco_venta
     * @return \Illuminate\Http\Response
     */
    public function show(qusco_venta $qusco_venta)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\qusco_venta  $qusco_venta
     * @return \Illuminate\Http\Response
     */
    public function edit(qusco_venta $qusco_venta)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\qusco_venta  $qusco_venta
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, qusco_venta $qusco_venta)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\qusco_venta  $qusco_venta
     * @return \Illuminate\Http\Response
     */
    public function destroy(qusco_venta $qusco_venta)
    {
        //
    }
}
