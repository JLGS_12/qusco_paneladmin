<?php

namespace App\Http\Controllers;

use App\disamd_proyecto;
use App\disamd_rutas;
use App\disamd_capas;
use Illuminate\Http\Request;

class DisamdProyectoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
     {
         /*$this->middleware(['auth_admin', 'can:Access Admin Panel'])->except(['frontend']);
         $this->middleware('intend_url')->only(['index', 'read']);
         $this->middleware('can:Create Docs')->only(['createForm', 'create']);*/
         $this->middleware('can:Read Docs')->only(['index', 'read']);
         /*$this->middleware('can:Update Docs')->only(['updateForm', 'update', 'move']);
         $this->middleware(['can:Delete Docs', 'not_system_doc'])->only('delete');*/
     }
    public function index()
    {
      $name = auth()->user()->id;
      $role = auth()->user()->roles[0]->name;
     if($role ==='adm_distritos'){
      $operaciones = disamd_proyecto::latest()->paginate(15);
      return view('amdproyecto.index', compact('operaciones' ))->with('i', (request()->input('page', 1) - 1) * 5);

     }else{
         return view('lap::backend.dashboard');
     }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $req)
    {
        //

        $this->validate(request(), [
            'nom' => 'required',
            'pre' => 'required',//|confirmed',
            'sec' => 'required',//|confirmed',
            'ini' => 'required',//|confirmed',
            'fin' => 'required',//|confirmed',
            'des' => 'required',//|confirmed',
        ]);

        $data = array_merge([
            'nom_proyecto'=> $req->nom,
            'pro_presupuesto'=> doubleval($req->pre),
            'id_sector' => intval($req->sec),
            'inicio' => $req->ini,
            'fin' => $req->fin,
            'descripcion' => $req->des,
            'estado' => 1,
        ]);
         //return response()->json("Actualizacion completada" .json_encode($data));
       //Log::info('Name entered is in fact Tim');
        //$id = DB::table("disamd_sector")->insert($data);

       $id = disamd_proyecto::create($data);
        return response()->json("Actualizacion completada" );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\disamd_proyecto  $disamd_proyecto
     * @return \Illuminate\Http\Response
     */
    public function show(disamd_proyecto $disamd_proyecto)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\disamd_proyecto  $disamd_proyecto
     * @return \Illuminate\Http\Response
     */
    public function edit(disamd_proyecto $disamd_proyecto)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\disamd_proyecto  $disamd_proyecto
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, disamd_proyecto $disamd_proyecto)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\disamd_proyecto  $disamd_proyecto
     * @return \Illuminate\Http\Response
     */
    public function destroy(disamd_proyecto $disamd_proyecto)
    {
        //
    }
}
