<?php

namespace App\Http\Controllers;

use App\qusco_operaciones;
use App\qusco_usuario_distrito;
use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class QuscoOperacionesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function __construct()
     {
         /*$this->middleware(['auth_admin', 'can:Access Admin Panel'])->except(['frontend']);
         $this->middleware('intend_url')->only(['index', 'read']);
         $this->middleware('can:Create Docs')->only(['createForm', 'create']);*/
         $this->middleware('can:Read Docs')->only(['index', 'read']);
         /*$this->middleware('can:Update Docs')->only(['updateForm', 'update', 'move']);
         $this->middleware(['can:Delete Docs', 'not_system_doc'])->only('delete');*/
     }


     public function index()
     {
         //
         $name = auth()->user()->id;
         $role = auth()->user()->roles[0]->name;
        if($role ==='administrador'){
            $idDistrito = qusco_usuario_distrito::where('idUsuario',$name)->get(['idDistrito'])[0]->idDistrito;
          //$operaciones = qusco_operaciones::latest()->paginate(15);
          $operaciones = qusco_operaciones::
          select('qusco_operaciones.*','qusco_datos_usuarios.dniruc','qusco_rutas.nombreruta')->
          join('qusco_datos_usuarios', 'qusco_operaciones.id_user', '=', 'qusco_datos_usuarios.id')->
          join('qusco_rutas', 'qusco_operaciones.id_ruta', '=', 'qusco_rutas.id')->
          where('qusco_rutas.idDistrito','=', $idDistrito)->
          latest('qusco_datos_usuarios.created_at')->get();//->paginate(15);
          return view('operaciones.index',compact('operaciones'));
              //->with('i', (request()->input('page', 1) - 1) * 15);

        }else{
            return view('lap::backend.dashboard');
        }
        //return view('operaciones/index');

        //return view('lap::backend.dashboard');
     }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\qusco_operaciones  $qusco_operaciones
     * @return \Illuminate\Http\Response
     */
    public function show(qusco_operaciones $qusco_operaciones)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\qusco_operaciones  $qusco_operaciones
     * @return \Illuminate\Http\Response
     */
    public function edit(qusco_operaciones $qusco_operaciones)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\qusco_operaciones  $qusco_operaciones
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, qusco_operaciones $qusco_operaciones)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\qusco_operaciones  $qusco_operaciones
     * @return \Illuminate\Http\Response
     */
    public function destroy(qusco_operaciones $qusco_operaciones)
    {
        //
    }
}
