<?php
# @Author: bndg
# @Date:   2019-04-03T12:00:18-05:00
# @Last modified by:   bndg
# @Last modified time: 2019-04-04T17:14:03-05:00




namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\qusco_usuarios;

class OperadorController extends Controller
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
        /*$this->middleware(['auth_admin', 'can:Access Admin Panel'])->except(['frontend']);
        $this->middleware('intend_url')->only(['index', 'read']);
        $this->middleware('can:Create Docs')->only(['createForm', 'create']);*/
        $this->middleware('can:Read Docs')->only(['index', 'read']);
        /*$this->middleware('can:Update Docs')->only(['updateForm', 'update', 'move']);
        $this->middleware(['can:Delete Docs', 'not_system_doc'])->only('delete');*/
    }
    public function inicio() {

         $name = auth()->user()->id;
        if(auth()->user()->roles[0]->id===2){
          $operaciones = qusco_usuarios::where('id_usuario', $name)->latest()->paginate(5);

          return view('operaciones.index',compact('operaciones'))
              ->with('i', (request()->input('page', 1) - 1) * 5);

        }else{
            return view('lap::backend.dashboard');
        }
        //return view('operaciones/index');
        return view('operaciones/index');
        //return view('lap::backend.dashboard');
    }


}
