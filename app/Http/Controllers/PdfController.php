<?php

namespace App\Http\Controllers;

use App\formato2;
use Illuminate\Http\Request;

use Dompdf\Dompdf;
use PDF;

class PdfController  extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
     {
         /*$this->middleware(['auth_admin', 'can:Access Admin Panel'])->except(['frontend']);
         $this->middleware('intend_url')->only(['index', 'read']);
         $this->middleware('can:Create Docs')->only(['createForm', 'create']);*/
         $this->middleware('can:Read Docs')->only(['index', 'read']);
         /*$this->middleware('can:Update Docs')->only(['updateForm', 'update', 'move']);
         $this->middleware(['can:Delete Docs', 'not_system_doc'])->only('delete');*/
     }
    public function index(){
        return view('pdf.index'); // ,compact('operaciones')
    }

    public function download(){
        /* $pdf = new Dompdf();
        $pdf->loadHTML('<h1>Styde.net</h1>'); */
        $pdf = PDF::loadView('pdf.index'); //, $data
        $pdf->save(storage_path('app').'_filename.pdf');
        return $pdf->download('mi-archivo.pdf');
    }



   /*  public function download()
    {
        $data = [
            'titulo' => 'Styde.net'
        ];

        return PDF::loadView('vista-pdf', $data)
            ->stream('archivo.pdf');
    } */
}
