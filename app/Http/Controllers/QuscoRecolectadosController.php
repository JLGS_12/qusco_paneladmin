<?php
# @Author: bndg
# @Date:   2019-04-05T13:42:08-05:00
# @Last modified by:   bndg
# @Last modified time: 2019-07-03T09:41:37-05:00




namespace App\Http\Controllers;

use App\qusco_recolectados;
use App\qusco_tipo_residuo;
use App\qusco_datos_usuarios;
use App\qusco_rutas;
use App\qusco_usuario_distrito;
use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class QuscoRecolectadosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function __construct()
     {
         /*$this->middleware(['auth_admin', 'can:Access Admin Panel'])->except(['frontend']);
         $this->middleware('intend_url')->only(['index', 'read']);
         $this->middleware('can:Create Docs')->only(['createForm', 'create']);*/
         $this->middleware('can:Read Docs')->only(['index', 'read']);
         /*$this->middleware('can:Update Docs')->only(['updateForm', 'update', 'move']);
         $this->middleware(['can:Delete Docs', 'not_system_doc'])->only('delete');*/
     }

    public function index()
    {
        //
        $role = auth()->user()->roles[0]->name;
       if($role ==='usuario'){
         // $id_user = auth()->user()->id;
         // $operaciones = qusco_recolectados::where('id_user', $id_user)->latest()->paginate(15);
         //
         // return view('recolectado.index',compact('operaciones'))
         //     ->with('i', (request()->input('page', 1) - 1) * 5);

       }else if($role ==='recolector'){
         // $id_user = auth()->user()->id;
         // $operaciones = qusco_recolectados::where('id_quien_recogio', $id_user)->latest()->paginate(15);
         // return view('recolectado.index',compact('operaciones'))
         //     ->with('i', (request()->input('page', 1) - 1) * 5);
       }else if($role ==='administrador'){
         $id_user = auth()->user()->id;
         $idDistrito = qusco_usuario_distrito::where('idUsuario',$id_user)->get(['idDistrito'])[0]->idDistrito;
         //$operaciones = qusco_recolectados::select('qusco_recolectados.*','qusco_tipo_residuos.nombre','qusco_datos_usuarios.dniruc')->join('qusco_datos_usuarios', 'qusco_recolectados.id_quien_recogio', '=', 'qusco_datos_usuarios.id','or', 'qusco_recolectados.id_user', '=', 'qusco_datos_usuarios.id')->join('qusco_tipo_residuos', 'qusco_recolectados.id_tipo', '=', 'qusco_tipo_residuos.id')->latest('qusco_tipo_residuos.created_at')->paginate(6);
         $operaciones = qusco_recolectados::
         select('fecha','nombreruta','cantidad','id_quien_recogio',
         'qusco_tipo_residuos.nombre','qusco_datos_usuarios.dniruc',
         'qusco_datos_usuarios.id_user','qusco_datos_usuarios.nombre as nombreU')->
            join('qusco_tipo_residuos','qusco_recolectados.id_tipo', '=', 'qusco_tipo_residuos.id')->
            join('qusco_datos_usuarios', 'qusco_recolectados.id_user', '=', 'qusco_datos_usuarios.qr_code')->
            join('qusco_rutas','qusco_datos_usuarios.id_ruta','=','qusco_rutas.id')->
              where('qusco_datos_usuarios.idDistrito','=', $idDistrito)->orderBy('fecha', 'desc')->get();
              //latest('qusco_recolectados.created_at')->paginate(20);
        $recis1 = qusco_datos_usuarios::select('nombre','dniruc')->where('id_user',2)->get();
        $recis = [];
        foreach ($recis1 as $key => $value) {
          $recis[$value->dniruc]=$value->nombre;
        }
        //dd($recis);
         return view('recolectado.index',compact('operaciones','recis'));

            // ->with('i', (request()->input('page', 1) - 1) * 20);
       }else {
           return view('lap::backend.dashboard');
       }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\qusco_recolectados  $qusco_recolectados
     * @return \Illuminate\Http\Response
     */
    public function show(qusco_recolectados $qusco_recolectados)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\qusco_recolectados  $qusco_recolectados
     * @return \Illuminate\Http\Response
     */
    public function edit(qusco_recolectados $qusco_recolectados)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\qusco_recolectados  $qusco_recolectados
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, qusco_recolectados $qusco_recolectados)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\qusco_recolectados  $qusco_recolectados
     * @return \Illuminate\Http\Response
     */
    public function destroy(qusco_recolectados $qusco_recolectados)
    {
        //
    }
}
