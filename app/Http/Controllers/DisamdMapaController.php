<?php

namespace App\Http\Controllers;

use App\disamd_mapa;
use App\disamd_rutas;
use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\DB;

class DisamdMapaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function __construct()
     {
         /*$this->middleware(['auth_admin', 'can:Access Admin Panel'])->except(['frontend']);
         $this->middleware('intend_url')->only(['index', 'read']);
         $this->middleware('can:Create Docs')->only(['createForm', 'create']);*/
         $this->middleware('can:Read Docs')->only(['index', 'read']);
         /*$this->middleware('can:Update Docs')->only(['updateForm', 'update', 'move']);
         $this->middleware(['can:Delete Docs', 'not_system_doc'])->only('delete');*/
     }

    public function index()
    {
        //
            $name = auth()->user()->id;
            $role = auth()->user()->roles[0]->name;
           if($role ==='adm_distritos'){
          //   $operaciones = DB::table('disamd_rutas')->get();
            $operaciones = disamd_rutas::latest()->paginate(15);
            return view('amdmapa.index', compact('operaciones' ))->with('i', (request()->input('page', 1) - 1) * 5);

           }else{
               return view('lap::backend.dashboard');
           }
    }




    public function create_mapa()
    {  $name = auth()->user()->id;
      $role = auth()->user()->roles[0]->name;
      if($role ==='adm_distritos'){
        return view('amdmapa.create');
      }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $name = auth()->user()->id;
        $role = auth()->user()->roles[0]->name;
       if($role ==='adm_distritos')
        {$this->validate(request(), [
            'nombreruta' => 'required',
            'area' => 'required',//|confirmed',
        ]);
        $data = array_merge( [
            'nombreruta'=> request()->input('nombreruta'),
            'estado' => 0,
        ]);

      $id = disamd_rutas::create($data)->id;


        $area = json_decode(request()->input('area'),true);
        foreach($area as $item) {
          error_log($item["lat"]);
          $data3 = [];
          $data3 = array_merge( [
              'idrut'=> $id,
              'lat'=> $item["lat"],
              'lon'=> $item["lng"]
          ]);
          $user = disamd_mapa::create($data3);
        }

        flash(['Completo!', 'Mapa creado! ' .$id ]);

        if (request()->input('_submit') == 'redirect') {
            return response()->json(['redirect' => session()->pull('url.intended', route('mapaview'))]);
        }
        else {
            return response()->json(['reload_page' => true]);
        }}
    }


    public function u_mapa(Request $req){
      $name = auth()->user()->id;
      $role = auth()->user()->roles[0]->name;
     if($role ==='disamd_rutas222')
        {$nombre = $req->nombre;
      $id = disamd_mapa::select('id')->where('nombreruta', $nombre)->first();
          $update1 =['nombreruta' =>  $req->nombreruta ,
                    ];
          $operaciones = disamd_mapa::where('idrut', $id->id)->update($update1);
        }
        return response()->json("Actualizacion completada");
    }


    public function area_mapa (Request $req){
      $name = auth()->user()->id;
      $role = auth()->user()->roles[0]->name;
     if($role ==='adm_distritos'){
       $nombre = $req->nombre;
       $id = disamd_rutas::select('id')->where('nombreruta', $nombre)->first()->id;
       $id2 = DB::table('disamd_mapas')->select('lat','lon')->where('idrut', $id)->get();
       return response()->json($id2);
     }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\disamd_mapa  $disamd_mapa
     * @return \Illuminate\Http\Response
     */
    public function show(disamd_mapa $disamd_mapa)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\disamd_mapa  $disamd_mapa
     * @return \Illuminate\Http\Response
     */
    public function edit(disamd_mapa $disamd_mapa)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\disamd_mapa  $disamd_mapa
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, disamd_mapa $disamd_mapa)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\disamd_mapa  $disamd_mapa
     * @return \Illuminate\Http\Response
     */
    public function destroy(disamd_mapa $disamd_mapa)
    {
        //
    }
}
