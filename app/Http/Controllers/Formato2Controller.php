<?php

namespace App\Http\Controllers;

use App\formato2;
use Illuminate\Http\Request;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\DB;

class Formato2Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function __construct()
     {
         /*$this->middleware(['auth_admin', 'can:Access Admin Panel'])->except(['frontend']);
         $this->middleware('intend_url')->only(['index', 'read']);
         $this->middleware('can:Create Docs')->only(['createForm', 'create']);*/
         $this->middleware('can:Read Docs')->only(['index', 'read']);
         /*$this->middleware('can:Update Docs')->only(['updateForm', 'update', 'move']);
         $this->middleware(['can:Delete Docs', 'not_system_doc'])->only('delete');*/
     }

    public function index()
    {
        //


                $name = auth()->user()->id;
                $role = auth()->user()->roles[0]->name;
               if($role ==='naval'){
                  $operaciones = DB::table("formato2")->latest()->paginate(15);

                  return view('naval.index',compact('operaciones'))
                      ->with('i', (request()->input('page', 1) - 1) * 5);

                }else{
                    return view('lap::backend.dashboard');
                }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\formato2  $formato2
     * @return \Illuminate\Http\Response
     */
    public function show(formato2 $formato2)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\formato2  $formato2
     * @return \Illuminate\Http\Response
     */
    public function edit(formato2 $formato2)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\formato2  $formato2
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, formato2 $formato2)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\formato2  $formato2
     * @return \Illuminate\Http\Response
     */
    public function destroy(formato2 $formato2)
    {
        //
    }
}
