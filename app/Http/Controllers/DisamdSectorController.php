<?php

namespace App\Http\Controllers;

use App\disamd_sector;
use App\disamd_rutas;
use App\disamd_capas;
use Illuminate\Http\Request;



use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\DB;

class DisamdSectorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
     {
         /*$this->middleware(['auth_admin', 'can:Access Admin Panel'])->except(['frontend']);
         $this->middleware('intend_url')->only(['index', 'read']);
         $this->middleware('can:Create Docs')->only(['createForm', 'create']);*/
         $this->middleware('can:Read Docs')->only(['index', 'read']);
         /*$this->middleware('can:Update Docs')->only(['updateForm', 'update', 'move']);
         $this->middleware(['can:Delete Docs', 'not_system_doc'])->only('delete');*/
     }
    public function index()
    {
      $name = auth()->user()->id;
      $role = auth()->user()->roles[0]->name;
     if($role ==='adm_distritos'){
      $operaciones = disamd_sector::latest()->paginate(15);
      return view('amdsector.index', compact('operaciones' ))->with('i', (request()->input('page', 1) - 1) * 5);

     }else{
         return view('lap::backend.dashboard');
     }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $req)
    {
        //

       // $this->info('Creating sample users...' );
       
        $this->validate(request(), [
            'nom' => 'required',
            'des' => 'required',//|confirmed',
            'pre' => 'required',//|confirmed',
        ]);

        $data = array_merge([
            'nom_sector'=> $req->nom,
            'descripcion' => $req->des,
            'presupuesto' => doubleval($req->pre),
            'estado' => 1,
        ]);
         //return response()->json("Actualizacion completada" .json_encode($data));
       //Log::info('Name entered is in fact Tim');
        //$id = DB::table("disamd_sector")->insert($data);

       $id = disamd_sector::create($data);
        return response()->json("Actualizacion completada" );

        /*flash(['Completo!', 'Sector creado! ' ]);

        if (request()->input('_submit') == 'redirect') {
            return response()->json(['redirect' => session()->pull('url.intended', route('sectorview'))]);
        }
        else {
            return response()->json(['reload_page' => true]);
        }*/
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\disamd_sector  $disamd_sector
     * @return \Illuminate\Http\Response
     */
    public function show(disamd_sector $disamd_sector)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\disamd_sector  $disamd_sector
     * @return \Illuminate\Http\Response
     */
    public function edit(disamd_sector $disamd_sector)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\disamd_sector  $disamd_sector
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, disamd_sector $disamd_sector)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\disamd_sector  $disamd_sector
     * @return \Illuminate\Http\Response
     */
    public function destroy(disamd_sector $disamd_sector)
    {
        //
    }
}
