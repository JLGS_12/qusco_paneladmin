<?php
# @Author: bndg
# @Date:   2019-04-03T15:04:29-05:00
# @Last modified by:   bndg
# @Last modified time: 2019-04-03T15:33:14-05:00




namespace App\Http\Controllers\Operador;

use Kjjdion\LaravelAdminPanel\Controllers\BackendController as LapBackendController;

class BackendController extends LapBackendController
{
    public function dashboard()
    {
        return view('lap::backend.dashboard');
    }

    public function settingsForm()
    {
        return view('lap::backend.settings');
    }

    public function settingsRules()
    {
        return [
            'example' => 'required',
        ];
    }
}
