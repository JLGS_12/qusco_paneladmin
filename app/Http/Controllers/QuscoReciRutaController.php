<?php

namespace App\Http\Controllers;

use App\qusco_reci_ruta;
use Illuminate\Http\Request;

class QuscoReciRutaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\qusco_reci_ruta  $qusco_reci_ruta
     * @return \Illuminate\Http\Response
     */
    public function show(qusco_reci_ruta $qusco_reci_ruta)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\qusco_reci_ruta  $qusco_reci_ruta
     * @return \Illuminate\Http\Response
     */
    public function edit(qusco_reci_ruta $qusco_reci_ruta)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\qusco_reci_ruta  $qusco_reci_ruta
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, qusco_reci_ruta $qusco_reci_ruta)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\qusco_reci_ruta  $qusco_reci_ruta
     * @return \Illuminate\Http\Response
     */
    public function destroy(qusco_reci_ruta $qusco_reci_ruta)
    {
        //
    }
}
