<?php
# @Author: bndg
# @Date:   2019-04-03T12:00:18-05:00
# @Last modified by:   bndg
# @Last modified time: 2019-04-03T14:23:46-05:00




namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class GeneralController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function logout(Request $request){
        $request->session()->regenerate(true);
        session()->forget('some_data');
        session()->flush();
        return view('welcome');
    }
}
