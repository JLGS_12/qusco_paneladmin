<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;

use Kjjdion\LaravelAdminPanel\Traits\AdminUser;
use Kjjdion\LaravelAdminPanel\Traits\DynamicFillable;
use Kjjdion\LaravelAdminPanel\Traits\UserTimezone;

class disamd_rutas extends Model
{
    //
    use Notifiable, AdminUser, DynamicFillable, UserTimezone;
    //
    protected $fillable = [
        'nombreruta', 'estado'
    ];
}
