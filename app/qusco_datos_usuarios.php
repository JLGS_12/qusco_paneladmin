<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;

use Kjjdion\LaravelAdminPanel\Traits\AdminUser;
use Kjjdion\LaravelAdminPanel\Traits\DynamicFillable;
use Kjjdion\LaravelAdminPanel\Traits\UserTimezone;

class qusco_datos_usuarios extends Model
{

    use Notifiable, AdminUser, DynamicFillable, UserTimezone;
    //
    protected $fillable = [
        'nombre', 'clave', 'clave',
    ];


    public function qusco_tipo_residuo()
    {
        return $this->hasMany('App\qusco_tipo_residuo','id');
    }

    public function qusco_recolectados()
    {
        return $this->hasMany('App\qusco_recolectados','id_user');
    }
}
