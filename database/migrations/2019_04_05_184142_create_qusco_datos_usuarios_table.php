<?php
# @Author: bndg
# @Date:   2019-04-05T13:41:42-05:00
# @Last modified by:   bndg
# @Last modified time: 2019-04-05T14:53:52-05:00




use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuscoDatosUsuariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('qusco_datos_usuarios', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('dniruc')->unique();
            $table->text('direcion')->nullable();
            $table->text('email')->nullable();
            $table->text('nombre');
            $table->text('clave');
            $table->integer('telefono')->nullable();
            $table->double('latitude');
            $table->double('longitude');
            $table->integer('id_ruta');
            $table->integer('id_user');
            $table->integer('idDistrito')->nullable();
            $table->text('qr_code')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('qusco_datos_usuarios');
    }
}
