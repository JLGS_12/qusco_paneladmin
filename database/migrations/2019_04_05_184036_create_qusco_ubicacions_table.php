<?php
# @Author: bndg
# @Date:   2019-04-05T13:40:36-05:00
# @Last modified by:   bndg
# @Last modified time: 2019-04-05T14:53:59-05:00




use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuscoUbicacionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('qusco_ubicacions', function (Blueprint $table) {
            $table->increments('id');
            $table->double('latitude');
            $table->double('longitude');
            $table->integer('id_user');
            $table->integer('id_ruta');
            $table->text('direccion')->nullable();;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('qusco_ubicacions');
    }
}
