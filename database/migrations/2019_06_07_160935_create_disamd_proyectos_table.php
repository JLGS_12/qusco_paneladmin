<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDisamdProyectosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('disamd_proyectos', function (Blueprint $table) {
            $table->increments('id');
            $table->text('nom_proyecto');
            $table->double('pro_presupuesto');
            $table->integer('id_sector');
            $table->text('inicio');
            $table->text('fin');
            $table->text('descripcion');
            $table->integer('estado');
            $table->timestamps();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('disamd_proyectos');
    }
}
