<?php
# @Author: bndg
# @Date:   2019-04-11T11:05:01-05:00
# @Last modified by:   bndg
# @Last modified time: 2019-04-11T11:24:36-05:00




use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuscoAreasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('qusco_areas', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('idrut');
            $table->double('lat');
            $table->double('lon');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('qusco_areas');
    }
}
