<?php
# @Author: bndg
# @Date:   2019-04-05T13:42:06-05:00
# @Last modified by:   bndg
# @Last modified time: 2019-04-05T14:56:33-05:00




use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuscoRecolectadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('qusco_recolectados', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_user')->index();
            $table->double('cantidad');
            $table->integer('id_ruta')->index();
            $table->integer('id_tipo')->index();
            $table->integer('id_quien_recogio');
            $table->text('ruta');
            $table->text('fecha');
            $table->timestamps();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('qusco_recolectados');
    }
}
