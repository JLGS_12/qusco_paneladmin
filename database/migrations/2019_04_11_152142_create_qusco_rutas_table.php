<?php
# @Author: bndg
# @Date:   2019-04-11T10:21:42-05:00
# @Last modified by:   bndg
# @Last modified time: 2019-08-12T13:43:36-05:00




use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuscoRutasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('qusco_rutas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombreruta')->unique();
            $table->double('ubilat')->nullable();
            $table->double('ubilon')->nullable();
            $table->integer('estado');
            $table->text('inicio')->nullable();
            $table->text('fin')->nullable();
            $table->integer('idDistrito')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('qusco_rutas');
    }
}
