<?php
# @Author: bndg
# @Date:   2019-04-11T11:15:42-05:00
# @Last modified by:   bndg
# @Last modified time: 2019-04-11T18:18:45-05:00




use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuscoHorariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('qusco_horarios', function (Blueprint $table) {
            $table->increments('id');
                        $table->integer('idrut');
                        $table->text('ultimo_d')->nullable();
                        $table->text('hora')->nullable();
                        $table->text('h1')->nullable();
                        $table->text('h2')->nullable();
                        $table->text('h3')->nullable();
                        $table->text('h4')->nullable();
                        $table->text('h5')->nullable();
                        $table->text('h6')->nullable();
                        $table->text('h7')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('qusco_horarios');
    }
}
