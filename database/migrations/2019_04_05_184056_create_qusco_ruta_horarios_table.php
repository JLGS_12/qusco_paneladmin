<?php
# @Author: bndg
# @Date:   2019-04-05T13:40:56-05:00
# @Last modified by:   bndg
# @Last modified time: 2019-04-05T14:48:35-05:00




use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuscoRutaHorariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('qusco_ruta_horarios', function (Blueprint $table) {
            $table->increments('id');
            $table->text('nombre_ruta');
            $table->text('h_inicio');
            $table->text('h_fin');
            $table->integer('id_mapa');
            $table->text('dias');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('qusco_ruta_horarios');
    }
}
