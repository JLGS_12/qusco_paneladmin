<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDisamdMapasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('disamd_mapas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('idcapas')->default(0);
            $table->integer('idrut');
            $table->double('area')->default(0);
            $table->double('lat');
            $table->double('lon');
            $table->integer('estado')->default(1);
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('disamd_mapas');
    }
}
