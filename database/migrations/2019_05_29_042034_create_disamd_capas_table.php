<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDisamdCapasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('disamd_capas', function (Blueprint $table) {
            $table->increments('id');
            $table->text('nom_capa');
            $table->integer('idsector');
            $table->integer('maestro')->default(0);
            $table->integer('esclavo')->default(0);
            $table->text('descripcion');
              $table->integer('estado');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('disamd_capas');
    }
}
