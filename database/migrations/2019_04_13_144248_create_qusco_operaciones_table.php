<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuscoOperacionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('qusco_operaciones', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_user');
            $table->double('cantidad')->nullable();
            $table->integer('id_ruta');
            $table->text('inicio');
            $table->text('fin')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('qusco_operaciones');
    }
}
