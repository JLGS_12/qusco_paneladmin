<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuscoVentasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('qusco_ventas', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('id_user');
          $table->integer('id_ruta');
          $table->integer('id_tipo');
          $table->double('valor_venta');
          $table->text('fecha');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('qusco_ventas');
    }
}
