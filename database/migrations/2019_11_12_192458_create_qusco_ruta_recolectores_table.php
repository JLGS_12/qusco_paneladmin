<?php
# @Author: bndg
# @Date:   2019-11-12T14:24:58-05:00
# @Last modified by:   bndg
# @Last modified time: 2019-11-12T14:27:44-05:00




use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuscoRutaRecolectoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('qusco_ruta_recolectores', function (Blueprint $table) {
            $table->increments('id');
              $table->integer('idRuta');
              $table->integer('idReco');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('qusco_ruta_recolectores');
    }
}
