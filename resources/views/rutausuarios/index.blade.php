@extends('lap::layouts.auth')

@section('title', 'Ruta y horario')
@section('child-content')
    <h2>@yield('title')</h2>

   <!--  <div class="row mb-3">
        <div class="col-md-auto mt-2 mt-md-0">
                <a href="{{ route('create_rutaview') }}" class="btn btn-success">Nueva Ruta</a>
        </div>
    </div> -->

    <div  style="height:76vh" id="mapa"></div>
   
   
@endsection

@section('css')
<link rel="stylesheet" href="https://unpkg.com/leaflet@1.5.1/dist/leaflet.css"
  integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ=="
  crossorigin=""/>
  <style>
    .none{
    display:none
  }

  </style>

@stop
@section('javascript')
<script src="https://unpkg.com/leaflet@1.5.1/dist/leaflet.js"
     integrity="sha512-GffPMF3RvMeYyc1LWMHtK8EbPv0iNZ8/oTtHPx9/cc2ILxQ+u905qIwdpULaqDkyBKgOaB57QTMg7ztg8Jm2Og=="
     crossorigin=""></script>
   <script>
   


    function d_ruta(obj){
         let a = obj.querySelectorAll("td");
        m_input=a[1].innerText;
        let modal = document.getElementById("m_de_user");
        modal.querySelector(".t_title").innerText="Desasignar esta ruta";
        modal.querySelector(".t_mesaje").innerText="¿Realmente deseas eliminar la ruta "+ m_input+"?";
        abrir(modal)

        modal.querySelectorAll("button")[0].setAttribute("td",obj.id)
    }
    function delete_ruta(obj){
        dataF = {id : obj.getAttribute("td")};
        $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });
        console.log(dataF);
        $.post('{{ route("del_ruta") }}', dataF , function(response) {
            console.log(response)
            location.reload();

          });
    }
   
      {{-- let a = "{{json_encode($operaciones[0])}}"; --}}
      //document.getElementById("");
      //console.log(a);
      let u_mapa = (obj) =>{
        console.log(obj);
        let a = obj.querySelectorAll("td");
        let modal = document.getElementById("m_up_mapa");
        let m_inputs = modal.querySelectorAll("input");
        abrir(modal)
        m_inputs[0].value=a[1].innerText;

        modal.querySelector("button").setAttribute("td",a[0].innerText)
      }

      let up_save = (obj) =>{
        let m_inputs = obj.querySelectorAll("input");

        console.log(m_inputs[3].checked)

        let dataF = {nombre : m_inputs[0].value,
                      h1: m_inputs[1].value ,
                    h2: m_inputs[2].value ,
                    h3: m_inputs[3].value ,
                  h4: m_inputs[4].value ,
                  h5: m_inputs[5].value ,
                h6: m_inputs[6].value ,
                h7: m_inputs[7].value };
        dataF = $.param(dataF) ;
        console.log(dataF)
        $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });
        $.post('{{ route("u_mapa") }}', dataF , function(response) {
            console.log(response)
            location.reload();

          });
      }

       var marker ;

     var l1 = -11.900200;
     var l2 = -77.041463;
     var codigo ="";
     var rutcod="";
     var nombreruta="";
     let configurado =false;
     var mypath = new Array();

      var mapa = L.map('mapa', {
         minZoom: 8,
         maxZoom: 18,
         center: [l1, l2],
         zoom: 16,
     })
     //var mapa = L.map('mapa').setView([l1, l2], 12);

   let elMapa = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
       attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
   }).addTo(mapa);

   elMapa.on('load',async (e)=>{
     
   })
   const allAreas = new L.FeatureGroup();
    function graficado(val){
      mypath = new Array();
      let tam = val.length;
      if (tam != 0) {

      for (var i = 0; i < tam; i++) {

          mypath.push([parseFloat(val[i].lat),parseFloat(val[i].lon)])
      }
      mapa.panTo(new L.LatLng(val[tam-1].lat, val[tam-1].lon));
      let polygon = L.polygon(mypath, {color: 'red'});

      polygon.addTo(mapa);
      allAreas.addLayer(polygon)
      polygon.on('click', function(event){
          var poli = event.target;
          allAreas.getLayers().forEach(otrasAreas=>{
            otrasAreas.setStyle({
                color: 'red'
            });})
          poli.setStyle({
              color: 'green'
          });
      });
      let popUp = `<div style="display:grid;text-align:center">
                <h6>${val[0].nombreruta}</h6>
                <button class="btn2" >
                  Asignar área!
                </button> `;
      let pop = polygon.bindPopup();
        var content = L.DomUtil.create('div', 'content');
        content.innerHTML= popUp
        pop.setPopupContent(content)
        L.DomEvent.addListener(content, 'click', function(event){
            if (event.target.tagName==='BUTTON') {
              asignarAreaToUser(val[0].idrut,val[0].nombreruta,polygon)
            }
        }, this);
      }
    }

    async function loadMapas(){
      let url = '{{ route("rutas_distrito") }}';
      let data = {};
      let options =  {
        method: 'POST', // or 'PUT'
        body: JSON.stringify(data), // data can be `string` or {object}!
        headers:{
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      }
      try {
      let respuesta = await fetch(url,options);
      let datos = await respuesta.json();
      let idrut = datos[0].idrut;
      let puntosAcumulados = [];
      for (const posicion of datos) {
        if(idrut != posicion.idrut){
          console.log(puntosAcumulados)
          graficado(puntosAcumulados)
          idrut = posicion.idrut;
          puntosAcumulados = [];
        }
        puntosAcumulados.push(posicion)
      }
      } catch (error) {
        console.error('Error:', error)
      }
      getUsuarios();
    }
    async function getUsuarios(){
      let url = '{{ route("usuarios_distritos") }}';
      let data = {};
      let options =  {
        method: 'POST', // or 'PUT'
        body: JSON.stringify(data), // data can be `string` or {object}!
        headers:{
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      }
      try {
        let respuesta = await fetch(url,options);
        let personas = await respuesta.json();
        console.log(personas)
        for (const persona of personas) {
          putUsuarios(persona)
        }
      } catch (error) {
        console.error('Error:', error)
      }
    }
    //let arrayCasas = []
    async function putUsuarios(user){
      let reciIcon = L.icon({
          iconUrl: '{{ asset("images/casaubicacion.png")}}',
          iconSize:     [38, 58],
          iconAnchor:   [38, 58],
          popupAnchor:  [-19, -60]
      });
      let casa = L.marker([user.latitude, user.longitude],{icon: reciIcon,draggable:false});
      let popUp = `<div style="display:grid;text-align:center">
                    <h6>${user.nombre}</h6>
                    <button class="btn2" onclick="asignarArea('${user.id}')">
                      Asignar área!
                    </button> `;
      casa.bindPopup(popUp).openPopup();
      mapa.addLayer(casa);
      allMarkers.addLayer(casa)
    }
    const allMarkers = new L.FeatureGroup();
    loadMapas();
    async function asignarArea(id){
      if(finalArea){
        doAsignarAreaUser(id,finalArea)
      }else{
        alert('Primero debes seleccionar un área.')
      }
      console.log(id)
    }
    let finalArea = null;
    async function asignarAreaToUser(id,ruta,polygon){
      for (const poligon of allAreas.getLayers()) {
        mapa.removeLayer(poligon);
      }
      polygon.addTo(mapa);
      let pop = polygon.bindPopup();
      console.log(pop)
      let popUp = `<div style="display:grid;text-align:center">
                <h6>${ruta}</h6>
                 `;
        var content = L.DomUtil.create('div', 'content');
        content.innerHTML= popUp
        pop.setPopupContent(content)
        /* L.DomEvent.addListener(content, 'click', function(event){
            if (event.target.tagName==='BUTTON') {
             console.log('yata')
            }
        }, this); */
      finalArea = id;
    }
    async function doAsignarAreaUser(idUser,idArea){
      let url = '{{ route("asignar_area_user") }}';
      let data = {
        idUser,idArea 
      };
      let options =  {
        method: 'POST', // or 'PUT'
        body: JSON.stringify(data), // data can be `string` or {object}!
        headers:{
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
          'Content-Type': 'application/json'
        }
      }
      try {
        let respuesta = await fetch(url,options);
        let personas = await respuesta.json();
        if(alert('Se asigno el usuario a la ruta mencionada')){}
        else    window.location.reload(); 
        //alert('se asigno el usuario a la ruta mencionada')
      } catch (error) {
        console.error('Error:', error)
      }
    }
   </script>
@stop
