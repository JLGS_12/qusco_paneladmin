@extends('lap::layouts.auth')

@section('title', 'Accesos')
@section('child-content')

    <h2>@yield('title')</h2>
    <div class="row mb-3">
        <div class="col-md-auto mt-2 mt-md-0">
                <a href="#" onclick="abrir(document.getElementById('m_new_user'))" class="btn btn-success">Nuevo Acceso</a>
        </div>
    </div>
    <div class="card">
        <div class="card-body">
          <table>

          </table>
          <table class="table table-bordered" id="Exportar_a_Excel">
            <thead>
              <tr>
                  <th>N°</th>
                  <th>Email</th>
                  <th>Distrito</th>
                  <th></th>
              </tr>
            </thead>

              @foreach ($operaciones as $i=>$product )

              <tr>
                  <td>{{ ++$i }}</td>
                  <td>{{ $product->email }}</td>
                  <td>{{ $product->Distrito }}</td>
                  <td>
                    <a  class="btn btn-link text-secondary p-1" title="Eliminar"><i onclick="d_usuario(this.parentElement.parentElement.parentElement)" class="fal fa-lg fa-trash"></i></a>
                    <a  class="btn btn-link text-secondary p-1" title="Actualizar"><i onclick="u_usuario(this.parentElement.parentElement.parentElement)" class="fal fa-lg fa-edit"></i></a>
                  </td>

              </tr>
              @endforeach
          </table>

        </div>

    </div>

    <div onclick="afuera(this)" id="m_up_user" class="modalx">
         <div onclick="adentro(event)" class="modal-contenido">
           <div><b>Email: </b><input id="email" type="email" disabled></div>
           <div><b>Clave: </b><input id="password" type="text"></div>
           <button class="btn1" onclick="up_save(this.parentElement)">Guardar</button>
         </div>
   </div>

   <div onclick="afuera(this)" id="m_new_user" class="modalx">
        <div onclick="adentro(event)" class="modal-contenido">
          <div><b>Email: </b><input id="email" type="email" ></div>
          <div><b>Nombre: </b><input id="name" type="text"></div>
          <div><b>Distrito: </b><input id="Distrito" type="text"></div>
          <div><b>Clave: </b><input id="password" type="text"></div>
          <button class="btn1" onclick="new_save(this.parentElement)">Guardar</button>
        </div>
  </div>

@endsection

@section('javascript')
<script type="text/javascript">
$(document).ready(function() {


    $('#Exportar_a_Excel').DataTable({
      "language": {
           "lengthMenu": "Mostrar _MENU_ filas por página",
           "zeroRecords": "No se encontró ese dato",
           "info": "Página _PAGE_ de _PAGES_",
           "infoEmpty": "Dato no disponible",
           "infoFiltered": "(filtered from _MAX_ total records)",

       }
    });
});

function u_usuario(obj){
  let modal = document.getElementById("m_up_user");
  let m_inputs = modal.querySelectorAll("input");
  let a = obj.querySelectorAll("td");
  let email = a[1].innerText;
  m_inputs[0].value=email;
  abrir(modal)
}

let up_save = (obj)=>{
    let dataF = {
    email : obj.querySelector("#email").value,
    password : obj.querySelector("#password").value,
    tipo:"update"}
    dataF = $.param(dataF) ;

    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    $.post('{{ route("n_distrito") }}', dataF , function(response) {

        location.reload();

      }).fail(function() {

})
  afuera(obj)
}

let new_save = (obj)=>{
    let dataF = {
    email : obj.querySelector("#email").value,
    password : obj.querySelector("#password").value,
    Distrito : obj.querySelector("#Distrito").value,
    name : obj.querySelector("#name").value,
    tipo:"new"}
    dataF = $.param(dataF) ;

    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    $.post('{{ route("n_distrito") }}', dataF , function(response) {

      location.reload();

      }).fail(function() {

})
  afuera(obj)
}
</script>
@endsection
