@extends('lap::layouts.auth')

@section('title', 'Proyectos')
@section('child-content')
    <h2>@yield('title')</h2>

    <div class="row mb-3">
        <div class="col-md-auto mt-2 mt-md-0">
                <a onclick="f_nuevo_proyecto()" class="btn btn-success">Nuevo Proyecto</a>
        </div>
    </div>

    <div class="card">
      <div class="card-body">
        <table class="table table-bordered">
            <tr>
                <th>N°</th>
                <th>Proyecto</th>
                <th>Presupuesto</th>
                <th>Sector</th>
                <th>Inicio</th>
                <th>Fin</th>
                <th>Descripcion</th>
                 <th></th>
            </tr>
            @php
              $estado="";
            @endphp
            @foreach ($operaciones as $product )

            <tr>
                <td>{{ ++$i }}</td>
                <td class="r_capa">{{ $product->nom_proyecto }}</td>
                <td>{{ $product->pro_presupuesto }}</td>
                <td>{{$product->id_sector}}</td>
                <td>{{$product->inicio}}</td>
                <td>{{$product->fin}}</td>
                <td>{{$product->descripcion}}</td>
                <td><a  class="btn btn-link text-secondary p-1" title="Update"><i onclick="d_capa(this.parentElement.parentElement.parentElement)" class="fal fa-lg fa-trash"></i></a></td>
            </tr>
            @endforeach
        </table>
        {{ $operaciones->links() }}
      </div>

    </div>
    <div onclick="afuera(this)" id="m_nuevo_proyecto" class="modalx">
         <div onclick="adentro(event)" class="modal-contenido">
            <div><b>Nombre: </b><input id="nom" type="text" ></div>
           <div><b>Presupuesto: </b><input id="pre" type="number" ></div>
           <div><b>Sector: </b><select id="sec" type="text" > <option value="0">Seleccionar</option> </select></div>
           <div><b>Inicio: </b><input id="ini" type="date" ></div>
           <div><b>Fin: </b><input id="fin" type="date" ></div>
           <div><b>Descripción: </b><input id="des" type="text" ></div>
           <button class="btn1" onclick="up_save(this.parentElement)">Guardar</button>
         </div>
   </div>

   <div onclick="afuera(this)" id="m_mensaje_capa" class="modalx">
        <div style="height:80vh" onclick="adentro(event)" class="modal-contenido">
          <div class="m_title"></div>
          <br>
          <div class="m_mesage"></div>
        </div>
  </div>




@endsection

@section('css')

  <style>
    .none{
    display:none
  }

  </style>
@stop
@section('javascript')

   <script>
   let f_nuevo_proyecto=()=>{
     let modal = document.getElementById("m_nuevo_proyecto");

      abrir(modal);
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });
      $.post('{{ route("sel_sector") }}', {} , function(response) {
        console.log(response);
        response.forEach((item)=>{
          let op = modal.querySelector("option");
          let sel = modal.querySelector("select");
          let cop = op.cloneNode(true);
          sel.appendChild(cop);
          cop.innerText=item.nom_sector;
          cop.value=item.id;
        });
        }).fail(
      function(jqXHR, textStatus, errorThrown) {
        console.log(jqXHR.responseText);
       });
   }

   let d_capa=(obj)=>{
    let a = obj.querySelector(".r_capa").innerText;
    open_mesaje("Eliminar Capa","¿Deseas eliminar la capa "+a+"?");
   }
   let open_mesaje=(title,message)=>{
    let mod = document.getElementById("m_mensaje_capa");
    mod.querySelector(".m_title").innerText=title;
    mod.querySelector(".m_message").innerText=message;
    abrir(mod);
   }

      let up_save=(obj)=>{
      let nom = obj.querySelector("#nom").value;
      let des = obj.querySelector("#des").value;
      let pre = obj.querySelector("#pre").value;
      let ini = obj.querySelector("#ini").value;
      let fin = obj.querySelector("#fin").value;
      let a = obj.querySelector("#sec");
      let sec = a.options[a.selectedIndex].value;

    let dataF = {nom:nom,
                des:des,
                pre:pre,
                ini:ini,
              fin:fin,
            sec:sec};
    dataF = $.param(dataF) ;
    console.log(dataF)
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    $.post('{{ route("c_proyecto") }}', dataF , function(response) {
        console.log(response)
        location.reload();

      }).fail(
    function(jqXHR, textStatus, errorThrown) {
          console.log(jqXHR.responseText);
     });
   }
   </script>
@stop
