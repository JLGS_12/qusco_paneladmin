@extends('lap::layouts.auth')

@section('title', 'Ruta y horario')
@section('child-content')
    <h2>@yield('title')</h2>

    <div class="card">
      <div class="card-body">
        <table class="table table-bordered">
            <tr>
                <th>N°</th>
                <th>Nombre de Ruta</th>
                <th>Inicio</th>
                <th>Fin</th>
                <th>Dias</th>
                <th>Mapa</th>
            </tr>

            @foreach ($operaciones as $product )

            <tr>
                <td>{{ ++$i }}</td>
                <td>{{ $product->nombre_ruta }}</td>
                <td>{{ $product->h_inicio }}</td>
                <td>{{ $product->h_fin }}</td>
                <td>  {{ $product->dias }}     </td>
                <td>   <a href="#">Ver mapa</a>  </td>

            </tr>
            @endforeach
        </table>
        {{ $operaciones->links() }}
      </div>
    </div>



@endsection
