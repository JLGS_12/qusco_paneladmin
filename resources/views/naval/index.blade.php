@extends('lap::layouts.auth')

@section('title', 'Registros')
@section('child-content')

    <h2>@yield('title')</h2>
    <div class="row mb-3">

    </div>
    <div class="card">
        <div class="card-body" style="overflow:auto">
          <table class="table table-bordered" border="1"  bordercolor="#666666" id="Exportar_a_Excel">
              <tr>
                  <th>N°</th>
                    <th>Solicitud</th>
                  <th>PDF</th>
                  <th>Coordenadas</th>
                  <th>Creado</th>

                  <th>Embarcacion</th>
                  <th>Nombres Apellidos</th>
                  <th>DNI</th>
                  <th>Existe</th>
                  <th>No Existe</th>
                  <th>Operativa</th>
                  <th>No Operativa</th>
                  <th>Eslora</th>
                  <th>Manga</th>
                  <th>Puntal</th>
                  <th>Color del Casco</th>
                  <th>Color de superestructura</th>
                  <th>Material Casco</th>
                  <th>Material Superestructura</th>
                  <th>Marca Motor</th>
                  <th>Modelo</th>
                  <th>Serie</th>
                  <th>Potencia</th>
                  <th>Grabado</th>
              </tr>

              @foreach ($operaciones as $product )

              <tr>
                  <td>{{ ++$i }}</td>
                      <td>{{ $product->d14 }}</td>
                    <td> <a href="https://qupaperu.com/formato/Controlador/uploads/">Link</a> </td>
                    <td> {{ $product->lat }} , {{ $product->lon }} </td>
                    <td>{{ $product->time }}</td>


                  <td>{{ $product->d0 }}</td>
                  <td>{{ $product->d15 }}</td>
                  <td>{{ $product->d16 }}</td>
                  <td>{{ $product->d17 }}</td>
                  <td>{{ $product->d18 }}</td>
                  <td>{{ $product->d19 }}</td>
                  <td>{{ $product->d20 }}</td>
                  <td>{{ $product->d1 }}</td>
                  <td>{{ $product->d2 }}</td>
                  <td>{{ $product->d3 }}</td>
                  <td>{{ $product->d4 }}</td>
                  <td>{{ $product->d5 }}</td>
                  <td>{{ $product->d6 }}</td>
                  <td>{{ $product->d7 }}</td>
                  <td>{{ $product->d8 }}</td>
                  <td>{{ $product->d9 }}</td>
                  <td>{{ $product->d10 }}</td>
                  <td>{{ $product->d11 }} {{$product->d12}}</td>
                  <td>{{ $product->d13 }}</td>

              </tr>
              @endforeach
          </table>
          {{ $operaciones->links() }}
        </div>

    </div>
    <form action="{{route('descarga')}}" method="post" target="_blank" id="FormularioExportacion">
<p>Exportar a Excel  <img src="{{url('/export_to_excel.jpg')}}" class="botonExcel" /></p>
<input type="hidden" id="datos_a_enviar" name="datos_a_enviar" />
<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
</form>



@endsection

@section('javascript')
<script type="text/javascript">
$(document).ready(function() {
   $(".botonExcel").click(function(event) {
   $("#datos_a_enviar").val( $("<div>").append( $("#Exportar_a_Excel").eq(0).clone()).html());
   $("#FormularioExportacion").submit();
});
});
</script>
@stop
