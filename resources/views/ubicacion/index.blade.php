@extends('lap::layouts.auth')

@section('title', 'Mpa de recolección')
@section('child-content')
    <h2>@yield('title')</h2>

    <div class="card">
        <div class="card-body">
          <table class="table table-bordered">
              <tr>
                  <th>N°</th>
                  <th>Usuario</th>
                  <th>Tipo</th>
                  <th width="280px">Cantidad</th>
                  <th width="280px">Fecha</th>
              </tr>

              @foreach ($operaciones as $product )

              <tr>
                  <td>{{ ++$i }}</td>
                  <td>{{ auth()->user()->name }}</td>
                  <td>{{ $product }}</td>
                  <td>{{ $product->cantidad }}</td>
                  <td>  {{ $product->mes }}     </td>
              </tr>
              @endforeach
          </table>
          {{ $operaciones->links() }}
        </div>

    </div>



@endsection
