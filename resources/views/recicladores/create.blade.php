@extends('lap::layouts.auth')

@section('title', 'Nuevo reciclador')
@section('child-content')
    <h2>@yield('title')</h2>

    <form method="POST" action="{{ route('createreciclador') }}" novalidate data-ajax-form>
        @csrf
        <div class="list-group">
            <div class="list-group-item">
                <div class="form-group row mb-0">
                    <label for="name" class="col-md-2 col-form-label">Nombre</label>
                    <div class="col-md-8">
                        <input type="text" name="nombre" id="nombre" class="form-control">
                    </div>
                </div>
            </div>
            <div class="list-group-item">
                <div class="form-group row mb-0">
                    <label for="name" class="col-md-2 col-form-label">DNI/RUC</label>
                    <div class="col-md-8">
                        <input type="text" min="10000000" max="99999999" name="dniruc" id="dniruc" class="form-control">
                    </div>
                </div>
            </div>
            <div class="list-group-item">
                <div class="form-group row mb-0">
                    <label for="name" class="col-md-2 col-form-label">Ruta</label>
                    <div class="col-md-8">
                        {{-- <input type="text" name="nombre" id="id_ruta" value="Por defecto" class="form-control" disabled> --}}
                        <select class="" id="id_ruta" name="id_ruta">

                        </select>
                    </div>
                </div>
            </div>

            <div class="list-group-item">
                <div class="form-group row mb-0">
                    <label for="name" class="col-md-2 col-form-label">Dirección</label>
                    <div class="col-md-8">
                        <input type="text" name="direcion" id="direcion" class="form-control">
                    </div>
                </div>
            </div>

            <div class="list-group-item">
                <div class="form-group row mb-0">
                    <label for="email" class="col-md-2 col-form-label">Email</label>
                    <div class="col-md-8">
                        <input type="email" name="email" id="email" class="form-control">
                    </div>
                </div>
            </div>

            <div class="list-group-item">
                <div class="form-group row mb-0">
                    <label for="password" class="col-md-2 col-form-label">Clave</label>
                    <div class="col-md-8">
                        <input type="password" pattern="[0-9]*" inputmode="numeric" name="clave" id="clave" class="form-control">
                    </div>
                </div>
            </div>

            <div class="list-group-item">
                <div class="form-group row mb-0">
                    <label for="password_confirmation" class="col-md-2 col-form-label">Confirmar Clave</label>
                    <div class="col-md-8">
                        <input type="password" pattern="[0-9]*" inputmode="numeric" name="clave_confirmation" id="clave_confirmation" class="form-control">
                    </div>
                </div>
            </div>

            <div class="list-group-item">
                <div class="form-group row mb-0">
                    <label for="name" class="col-md-2 col-form-label">telefono</label>
                    <div class="col-md-8">
                        <input type="text" min="100000000" max="999999999" name="telefono" id="telefono" class="form-control">
                    </div>
                </div>
            </div>


            <div class="list-group-item bg-light text-left text-md-right pb-1">
          
                <button type="submit" name="_submit" class="btn btn-success mb-2" value="redirect">Guardar &amp; y retroceder</button>
            </div>
        </div>
    </form>
@endsection

@section('javascript')
    <script type="text/javascript">
    elementos=[]
    let buscar =()=>{
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });
      $.post('{{ route("las_rutas") }}', {"s":"s"}, function(response) {
         console.log(response)
         temp1 = response.data;
         temp2 = document.getElementById("id_ruta")
         temp1.forEach(item=>{
           a = document.createElement("option")
           a.value=item.id
           a.innerText=item.nombreruta
           temp2.appendChild(a)
           elementos.push(item.nombreruta);
         })

        }).fail(
      function(jqXHR, textStatus, errorThrown) {

       });


    }

    buscar();
    </script>
@endsection
