@extends('lap::layouts.auth')

@section('title', 'Recicladores')
@section('child-content')

    <h2>@yield('title')</h2>


    <div class="row mb-3">

        <div class="col-md-auto mt-2 mt-md-0">
                <a href="{{ route('create_recicladorview') }}" class="btn btn-success">Nuevo Reciclador</a>
        </div>
    </div>

       <div onclick="afuera(this)" id="m_de_user" class="modalx">
        <div onclick="adentro(event)" class="modal-contenido" style="width: fit-content;">
          <center > <h2 class="t_title"></h2> </center>
          <h3 class="t_mesaje">

          </h3>
          <br>
          <div><button class="btn1" style="" onclick="delete_u(this)">Si!</button></div>
          <div><button class="btn1" style="" onclick="afuera(this.parentElement.parentElement)">No!</button></div>
        </div>
  </div>
    <div class="card">
        <div class="card-body">

          <table class="table table-bordered"  border="1"  bordercolor="#666666" id="Exportar_a_Excel">
              <thead>
                <tr>
                    <th><a href="#">N°</a></th>
                    <th><a href="#">Nombre</a></th>
                    <th><a href="#">DNI/RUC</a></th>
                    <th><a href="#">Clave</a></th>
                    <th><a href="#">Email</a></th>
                    <th><a href="#">Telefono</a></th>

                    <th></th>
                </tr>
              </thead>
              @php
                $estado="";
              @endphp
              @foreach ($operaciones as $i=>$product )

              <tr id="{{$product->id}}">
                  <td>{{ ++$i }}</td>
                  <td>{{ $product->nombre }}</td>
                  <td>{{ $product->dniruc }}</td>
                  <td> <a style="cursor:pointer;color:blue" onclick="ver_cla(this.parentElement.parentElement)">Ver </a> <div hidden id="clav">{{$product->clave}}</div></td>
                  <td>{{ $product->email }}</td>
                  <td>{{ $product->telefono }}</td>

                  <td>
                    <a  class="btn btn-link text-secondary p-1" title="Update"><i onclick="u_reciclador(this.parentElement.parentElement.parentElement)" class="fal fa-lg fa-edit"></i></a>
                    <a class="btn btn-link text-secondary p-1" title="Eliminar"><i onclick="d_usuario(this.parentElement.parentElement.parentElement)" class="fal fa-lg fa-trash"></i></a>

                  </td>


              </tr>
              @endforeach
          </table>
          {{-- {{ $operaciones->links() }} --}}
        </div>

    </div>
    <form action="{{route('descarga')}}" method="post" target="_blank" id="FormularioExportacion" style="cursor:pointer">
<p>Exportar a Excel  <img src="{{url('/export_to_excel.jpg')}}" class="botonExcel" /></p>
<input type="hidden" id="datos_a_enviar" name="datos_a_enviar" />
<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
</form>
     <div onclick="afuera(this)" id="m_up_reci" class="modalx">
          <div onclick="adentro(event)" class="modal-contenido">
            <div><b>Nombre: </b><input type="text"></div>
            <div><b>DNI: </b><input type="text" disabled></div>
             <div><b>Clave: </b><input type="text" ></div>
            <div><b>Email: </b><input type="text" ></div>
            <div><b>Telefono: </b><input type="text"></div>
            <button class="btn1" onclick="up_save(this.parentElement)">Guardar</button>
          </div>
    </div>

@endsection

@section("css")

@stop
@section("javascript")
  <script>

    let ver_cla =(obj)=>{
      let cla = obj.querySelector("#clav");
      if(cla.hasAttribute("hidden")){
        cla.removeAttribute("hidden");
        obj.querySelector("a").innerText = "Ocultar";
      }else{
        obj.querySelector("a").innerText = "Ver";
        cla.setAttribute("hidden",true)
      }
    }

     function delete_u(obj){
        dataF = {id : obj.getAttribute("td")};
        $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });
        $.post('{{ route("d_usuario") }}', dataF , function(response) {
            console.log(response)
            //afuera(m_de_user)
            location.reload();

          });


      }

    function d_usuario(obj){
        let a = obj.querySelectorAll("td");
        console.log(a)
        m_input=a[2].innerText;
        let modal = document.getElementById("m_de_user");
        modal.querySelector(".t_title").innerText="Eliminar Reciclador";
        modal.querySelector(".t_mesaje").innerText="¿Realmente eliminar el reciclador "+ m_input+"?";
        abrir(modal)

        modal.querySelectorAll("button")[0].setAttribute("td",m_input)

      }

    function u_reciclador(obj){

      let a = obj.querySelectorAll("td");
      let modal = document.getElementById("m_up_reci");
      let m_inputs = modal.querySelectorAll("input");

      m_inputs[0].value=a[1].innerText;
      m_inputs[1].value=a[2].innerText;
      m_inputs[2].value=obj.querySelector("#clav").innerText;
      m_inputs[3].value=a[4].innerText;
      m_inputs[4].value=a[5].innerText;

      modal.querySelector("button").setAttribute("td",a[0].innerText)


       abrir(modal)
    }

    let up_save = (obj) =>{

        let m_inputs = obj.querySelectorAll("input");

        let dataF = {nombre : m_inputs[0].value,
                      dni: m_inputs[1].value ,
                      clave: m_inputs[2].value ,
                      email: m_inputs[3].value ,
                    telefono: m_inputs[4].value };
        dataF = $.param(dataF) ;

        $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });
        $.post('{{ route("u_reciclador") }}', dataF , function(response) {

            location.reload();

          });



    }


    $(document).ready(function() {
       $(".botonExcel").click(function(event) {
       $("#datos_a_enviar").val( $("<div>").append( $("#Exportar_a_Excel").eq(0).clone()).html());
       $("#FormularioExportacion").submit();
  });
  $('#Exportar_a_Excel').DataTable({
    "language": {
         "lengthMenu": "Mostrar _MENU_ filas por página",
         "zeroRecords": "No se encontró ese dato",
         "info": "Página _PAGE_ de _PAGES_",
         "infoEmpty": "Dato no disponible",
         "infoFiltered": "(filtered from _MAX_ total records)",

     },
     responsive:  false
  });

  });
  </script>
@stop
