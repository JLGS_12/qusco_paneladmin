@extends('lap::layouts.auth')

@section('title', 'Operaciones')
@section('child-content')
    <h2>@yield('title')</h2>

    <div class="card">
        <div class="card-body">

            <table class="table table-bordered">
                <tr>
                    <th>N°</th>
                    <th>Reciclador</th>
                    <th>Ruta</th>
                    <th width="280px">Inició</th>
                    <th width="280px">Terminó</th>
                </tr>

                @foreach ($operaciones as $i=>$product )

                <tr>
                    <td>{{ ++$i }}</td>
                    <td>{{ $product->dniruc }}</td>
                    <td>{{ $product->nombreruta }}</td>
                    <td>{{ $product->inicio }}</td>
                    <td>{{ $product->fin }}</td>
                </tr>
                @endforeach
            </table>
            {{-- {{ $operaciones->links() }} --}}


        </div>
        <div class="">


        </div>
    </div>



@endsection
