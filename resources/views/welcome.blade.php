<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Qupa</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->

  <!-- Theme Made By www.w3schools.com - No Copyright -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>



<!-- face -->

<script>
  // This is called with the results from from FB.getLoginStatus().
  function statusChangeCallback(response) {
    console.log('statusChangeCallback');
    console.log(response);

    if (response.status === 'connected') {
      // Logged into your app and Facebook.
      testAPI();
    } else if (response.status === 'not_authorized') {
      // The person is logged into Facebook, but not your app.
      document.getElementById('status').innerHTML = 'Please log ' +
        'into this app.';
    } else {
      // The person is not logged into Facebook, so we're not sure if
      // they are logged into this app or not.
      document.getElementById('status').innerHTML = 'Please log ' +
        'into Facebook.';
    }
  }


  function checkLoginState() {
    FB.getLoginStatus(function(response) {
      statusChangeCallback(response);
    });
  }

  window.fbAsyncInit = function() {
  FB.init({
    appId      : '643309375821322',
    cookie     : true,  // enable cookies to allow the server to access
                        // the session
    xfbml      : true,  // parse social plugins on this page
    version    : 'v2.5' // use graph api version 2.5
  });



  FB.getLoginStatus(function(response) {
    statusChangeCallback(response);
  });

  };

  // Load the SDK asynchronously
  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));

  // Here we run a very simple test of the Graph API after login is
  // successful.  See statusChangeCallback() for when this call is made.
  function testAPI() {
    console.log('Welcome!  Fetching your information.... ');
    FB.api('/me', function(response) {
      console.log('Successful login for: ' + response.name);
      document.getElementById('status').innerHTML =
        'Thanks for logging in, ' + response.name + '!';
    });
  }
</script>
</head>




  <style>

.margin {margin-bottom: 20px;}
.margin2 {margin top: -10px;}
.bot{
margin-top: -22px;
}


  body {
      font: 20px Montserrat, sans-serif;
      line-height: 1.8;
      color: #f5f6f7;
  }


  .bg-1 {
      background-color: #1abc9c; /* Green */
      color: #ffffff;
  }
  .bg-2 {
      background-color: #474e5d; /* Dark Blue */
      color: #ffffff;
  }
  .bg-3 {
      background-color: #ffffff; /* White */
      color: #555555;
  }
  .bg-4 {
      background-color: #2f2f2f; /* Black Gray */
      color: #fff;
  }
  .container-fluid {
      padding-top: 40px;
      padding-bottom: 40px;
  }

  .navbar {
      padding-top: 15px;
      padding-bottom: 15px;
      border: 0;
      border-radius: 0;
      margin-bottom: 0;
      font-size: 12px;
      letter-spacing: 5px;
  }
  .blogin {
      margin-left: 15px;
  }
  .navbar-nav  li a:hover {
      color: #1abc9c !important;
  }
  .navbar-brand{
    padding-top: 5px;
  }

@media screen and (max-width: 650px) {

  .bot{
    width: 30%;
      }
     h2 {
        font-size: 19px;
    }
     h3 {
        font-size: 17px;
    }
    p {font-size: 16px;}
    .margin{margin-bottom: 15px;}

.container-fluid {
      padding-top: 30px;
      padding-bottom: 30px;
  }

.margin{margin-bottom: 10px;}


}
@media screen and (max-width: 500px) {
    h2 {
        font-size: 18px;
    }
     h3 {
        font-size: 16px;
    }
    p {font-size: 14px;}
    .margin{margin-bottom: 10px;}

.container-fluid {
      padding-top: 20px;
      padding-bottom: 20px;
  }

.margin{margin-bottom: 10px;}

.bot{

    width: 40%;


  }
}





h5{
    font-size: 1.4em;
}
  </style>

<body>

<!-- Navbar -->
<nav class="navbar navbar-default">
  <div class="container">
    <div class="navbar-header">

      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>

      <!--<a class="navbar-brand" href="#">Nosotros</a>
      <!-- Trigger the modal with a button -->
      <div class="navbar-brand">
      <button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal"><span class="glyphicon glyphicon-comment"></span>&nbsp;Comunicate con nosotros</button>

    </div>

    </div>

      <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right">

        <li ><a target="_blank" href="https://play.google.com/store/apps/details?id=com.qupa.deckblank.qupa" ><img   width="25%"  src={{ asset("disponible.png")}}  alt="google-play" class="bot" > </a></li>

      </ul>
    </div>

  </div>
</nav>


<div class="container-fluid bg-1 text-center">



   <h2 >Reciclar con Qupa es facil, y hasta divertido</h2>

<div >

  <img src={{ asset("banner.png")}} class="img-rounded" alt="Cinque Terre" width="100%" >

<h3 class="margin">Qupa te ayuda a reciclar y en el camino mejoras el trabajo de las personas que se ocupan de lo que nosotros desechamos</h3>
</div>





<!-- Second Container -->
<div class="container-fluid bg-2 text-center">
  <h1><span class="glyphicon glyphicon-info-sign"></span></h1>
  <h3 class="margin">¿Acerca de Qupa? </h3>
  <p>El proyecto Qupa es una herramienta para ayudar a todos los involucrados en el proceso de de generación y recolección de productos reciclables, sumate al proyecto somos una iniciativa civil que cambiara la manera de desechar lo que llamamos basura  . </p>

</div>


<!-- Third Container (Grid) -->
<div class="container-fluid bg-3 text-center">
  <h2 class="margin">Nuestro plan de trabajo</h2><br>
  <div class="row">
    <div class="col-sm-4">
       <center><img src={{ asset("qupa.png")}} class="img-responsive margin" style="width:30%" alt="Image"></center>
      <h4>¿Qué queremos lograr?</h4>

      <p>Dar a la importante labor del reciclaje una herramienta tecnologica que integre a todos los involucrados en la generación y recolección de residuos reciclables .</p>

    </div>
    <div class="col-sm-4">
      <center><img src={{ asset("qupa.png")}} class="img-responsive margin" style="width:30%" alt="Image"></center>
      <h4>¿Cómo lo lograremos?</h4>
      <p>Con la ayuda de nuestros aliados estrategicos, los recicladores, llevaremos el metodo Qupa a todo el pais para que reciclar sea facil y hasta divertido.</p>

    </div>
    <div class="col-sm-4">
     <center><img src={{ asset("qupa.png")}} class="img-responsive margin" style="width:30%" alt="Image"></center>
      <h4>¿Dónde puedes encontrarnos?</h4>
      <p>Si quieres participar de esta iniciativa civil escribenos a deckblank@gmail.com o siguenos en Facebook @qupaPeru.</p>

    </div>
  </div>
</div>

<!-- Footer -->
<footer class="container-fluid bg-4 text-center">



  <p>Qupa powered by <a href="#">DECKBLANKCORP</a></p>
</footer>

</body>

<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="bg-3 text-left">

   <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Comunicate con nosotros.</h4>
      </div>
      <div class="modal-body">



<form action="" method="POST">
<div class="form-group">
  <label for="usr">Codigo Qupa/Email:</label>
  <input maxlength="40" type="text" class="form-control" id="usr"  >
</div>
<div class="form-group">
  <label for="usr">Mensaje:</label>
  <textarea maxlength="250" class="form-control" style="max-height:30vh"name="Text1" cols="40" rows="5"  id="des"></textarea>
</div>

<!--<button type="submit" class="btn btn-success"> <h4>Iniciar Sesión</h1></button> -->

</form>


      </div>
      <div class="modal-footer">
       <!-- <button type="button" class="btn btn-no" data-dismiss="modal"> <h4>Cerrar</h4></button>-->
        <center><button type="submit" class="btn btn-success" onclick="$('#myModal').modal('toggle');alert('Gracias por informarnos')" > <h4>Enviar</h1></button></center>
      </div>

      </div>

    </div>


  </div>
</div>


</html>
