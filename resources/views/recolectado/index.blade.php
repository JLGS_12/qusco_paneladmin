@extends('lap::layouts.auth')

@section('title', 'Recolectado')
@section('child-content')
    <h2>@yield('title')</h2>
    <div class="card">
        <div class="card-body">
          <div class="card-body">
            {{-- {{$operaciones}} --}}
            <table class="table table-bordered"  border="1"  bordercolor="#666666" id="Exportar_a_Excel">
              <thead>
                <tr>
                    <th><a href="#">N°</a></th>
                    <th><a href="#">Nombre/Razon</a></th>
                    <th><a href="#">DNI/RUC</a></th>
                    <th><a href="#">Recolector</a></th>
                    <th><a href="#">DNI Recolector</a></th>
                    <th><a href="#">Ruta</a></th>
                    <th><a href="#">Tipo</a></th>
                    <th><a href="#">Cantidad</a></th>
                    <th><a href="#">Fecha</a></th>
                </tr>
              </thead>
                @foreach ($operaciones as $i=>$product )

                    <tr>
                      <td>{{ ++$i }}</td>
                      <td>{{$product->nombreU}}</td>
                      <td>{{$product->dniruc}}</td>
                      <td>{{ $recis[$product->id_quien_recogio] ?? '' }}</td>
                      <td>{{ $product->id_quien_recogio }}</td>
                      <td>{{ $product->nombreruta }}</td>
                      <td>{{ $product->nombre }}</td>
                      <td>{{ $product->cantidad }}</td>
                      <td>{{ $product->fecha }}</td>
                   </tr>


                @endforeach
            </table>
            {{-- {{ $operaciones->links() }} --}}
          </div>
        </div>

        <form action="{{route('descarga')}}" method="post" target="_blank" id="FormularioExportacion" style="cursor:pointer">
    <p>Exportar a Excel  <img src="{{url('/export_to_excel.jpg')}}" class="botonExcel" /></p>
    <input type="hidden" id="datos_a_enviar" name="datos_a_enviar" />
    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
    </form>
    </div>


@endsection

@section('javascript')
<script type="text/javascript">


$(document).ready(function() {
   $(".botonExcel").click(function(event) {
   $("#datos_a_enviar").val( $("<div>").append( $("#Exportar_a_Excel").eq(0).clone()).html());
   $("#FormularioExportacion").submit();

});

$('#Exportar_a_Excel').DataTable({
  "language": {
       "lengthMenu": "Mostrar _MENU_ filas por página",
       "zeroRecords": "No se encontró ese dato",
       "info": "Página _PAGE_ de _PAGES_",
       "infoEmpty": "Dato no disponible",
       "infoFiltered": "(filtered from _MAX_ total records)",

   }
});

});
</script>

@stop
