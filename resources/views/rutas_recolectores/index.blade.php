@extends('lap::layouts.auth')

@section('title', 'Asignacion de ruta')
@section('child-content')
<style >
/*the container must be positioned relative:*/
.autocomplete {
position: relative;
display: inline-block;
width: 100%;
}

/* input {
border: 1px solid transparent;
padding: 10px;
font-size: 16px;
}

input[type=text] {
background-color: #f1f1f1;
width: 100%;
}

input[type=submit] {
background-color: #f1f1f1;
} */

.autocomplete-items {
position: absolute;
border: 1px solid #d4d4d4;
border-bottom: none;
border-top: none;
z-index: 99;
/*position the autocomplete items to be the same width as the container:*/
top: 100%;
left: 0;
right: 0;
}

.autocomplete-items div {
padding: 10px;
cursor: pointer;
background-color: #fff;
border-bottom: 1px solid #d4d4d4;
}

/*when hovering an item:*/
.autocomplete-items div:hover {
background-color: #e9e9e9;
}

/*when navigating through the items using the arrow keys:*/
.autocomplete-active {
background-color: DodgerBlue !important;
color: #ffffff;
}
</style>
    <h2>@yield('title')</h2>

    <div class="row mb-3">
        <div class="col-md-auto mt-2 mt-md-0">
                <a href="#" onclick="u_mapa()" class="btn btn-success">Nueva Asignación</a>
        </div>
    </div>

    <div class="card">
      <div class="card-body">
        <table class="table table-bordered"  width="100%" border="1"  bordercolor="#666666" id="Exportar_a_Excel">
            <thead>
                <tr>
                    <th>N°</th>
                    <th>Ruta</th>
                    <th>Recolector</th>
                    <th></th>
                </tr>
            </thead>

            @foreach ($operaciones as $i=>$product )
            <tr id="{{$product->id}}">
                <td>{{ $i+1 }}</td>
                <td>{{ $product->nombreruta }}</td>
                <td>{{$product->nombre}}</td>
                <td>
                  <a  class="btn btn-link text-secondary p-1" title="Eliminar"><i onclick="d_usuario(this.parentElement.parentElement.parentElement)" class="fal fa-lg fa-trash"></i></a>
                </td>
            </tr>
            @endforeach
        </table>
      </div>

    </div>
    <div onclick="afuera(this)" id="m_up_mapa" class="modalx">
         <div onclick="adentro(event)" class="modal-contenido">
             <center > <h2 class="t_title">Nueva asignación</h2> </center>
             <div  >

               </div>
          <div class="autocomplete"><b>Reciclador: </b>
              <input id="id_reco" type="text" style="width:100%;"  placeholder="Recicladores">
          </div>

          <div><b>Ruta: </b>
              <select class="" name="id_ruta" id="id_ruta">

              </select>
          </div>

           <button class="btn1" onclick="up_save(this.parentElement)">Guardar</button>
         </div>
   </div>
   <div onclick="afuera(this)" id="m_de_user" class="modalx">
        <div onclick="adentro(event)" class="modal-contenido" style="width: fit-content;">
          <center > <h2 class="t_title"></h2> </center>
          <h3 class="t_mesaje">

          </h3>
          <br>
          <div><button class="btn1" style="" onclick="delete_u(this)">Si!</button></div>
          <div><button class="btn1" style="" onclick="afuera(this.parentElement.parentElement)">No!</button></div>
        </div>
  </div>

    <div onclick="afuera(this)" id="m_ver_mapa" class="modalx">
        <div style="height:80vh" onclick="adentro(event)" class="modal-contenido">
           <div  style="height:76vh" id="mapa"></div>
        </div>
  </div>
@endsection


@section('javascript')

   <script>
      let a = "{{$operaciones}}";



      let u_mapa = () =>{


        let modal = document.getElementById("m_up_mapa");
        buscar_reci();
        buscar();


        abrir(modal)



      }

      let up_save = (obj) =>{
        let m_inputs = obj.querySelectorAll("select");


        ruta = document.getElementById("id_ruta")
        reci = document.getElementById("id_reco")

        let dataF = {idRuta : ruta[ruta.selectedIndex].value,
                      idReco: elementos[losReciclaroes.indexOf(reci.value)]
                    };
        dataF = $.param(dataF) ;

        $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });
        $.post('{{ route("c_asignacion") }}', dataF , function(response) {

            location.reload();

          });
      }

      function delete_u(obj){
        dataF = {id : obj.getAttribute("td")};
        $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });
        $.post('{{ route("d_asignacion") }}', dataF , function(response) {
            console.log(response)
          //  location.reload();

          });

      }
      function d_usuario(obj){
        let a = obj.querySelectorAll("td");

        m_input=a[2].innerText;
        let modal = document.getElementById("m_de_user");
        modal.querySelector(".t_title").innerText="Desasignar esta ruta";
        modal.querySelector(".t_mesaje").innerText="¿Realmente deseas desasignar a "+ m_input+" la ruta " +a[1].innerText+"?";
        abrir(modal)

        modal.querySelectorAll("button")[0].setAttribute("td",obj.id)

      }


      $(document).ready(function() {


         var table = $('#Exportar_a_Excel').DataTable({
           "language": {
                "lengthMenu": "Mostrar _MENU_ filas por página",
                "zeroRecords": "No se encontraron datos",
                "info": "Página _PAGE_ de _PAGES_",
                "infoEmpty": "Dato no disponible",
                "infoFiltered": "(filtered from _MAX_ total records)",
            },
            responsive: {
              details: false
          }
         });

      });


      elementos=[]

      let buscar =()=>{
          if(!document.getElementById("id_ruta").length){
            $.ajaxSetup({
              headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              }
            });
            $.post('{{ route("las_rutas") }}', {"s":"s"}, function(response) {

               temp1 = response.data;
               temp2 = document.getElementById("id_ruta")
               temp1.forEach(item=>{
                 a = document.createElement("option")
                 a.value=item.id
                 a.innerText=item.nombreruta
                 temp2.appendChild(a)
                // elementos.push(item.nombreruta);
               })

              }).fail(
            function(jqXHR, textStatus, errorThrown) {

             });
         }

      }
      var losReciclaroes=[];
      let buscar_reci = ()=>{
          if(!losReciclaroes.length){
              $.ajaxSetup({
                headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
              });
              $.post('{{ route("las_reci") }}', {"s":"s"}, function(response) {

                 temp1 = response.data;


                 temp1.forEach(item=>{
                   losReciclaroes.push(item.nombre)
                   elementos.push(item.id);
                 })

                }).fail(
              function(jqXHR, textStatus, errorThrown) {

               });
          }



      }


      function autocomplete(inp, arr) {
        /*the autocomplete function takes two arguments,
        the text field element and an array of possible autocompleted values:*/
        var currentFocus;
        /*execute a function when someone writes in the text field:*/
        inp.addEventListener("input", function(e) {
            var a, b, i, val = this.value;
            /*close any already open lists of autocompleted values*/
            closeAllLists();
            if (!val) { return false;}
            currentFocus = -1;
            /*create a DIV element that will contain the items (values):*/
            a = document.createElement("DIV");
            a.setAttribute("id", this.id + "autocomplete-list");
            a.setAttribute("class", "autocomplete-items");
            /*append the DIV element as a child of the autocomplete container:*/
            this.parentNode.appendChild(a);
            /*for each item in the array...*/
            for (i = 0; i < arr.length; i++) {
              /*check if the item starts with the same letters as the text field value:*/
              if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
                /*create a DIV element for each matching element:*/
                b = document.createElement("DIV");
                /*make the matching letters bold:*/
                b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
                b.innerHTML += arr[i].substr(val.length);
                /*insert a input field that will hold the current array item's value:*/
                b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
                /*execute a function when someone clicks on the item value (DIV element):*/
                b.addEventListener("click", function(e) {
                    /*insert the value for the autocomplete text field:*/
                    inp.value = this.getElementsByTagName("input")[0].value;
                    /*close the list of autocompleted values,
                    (or any other open lists of autocompleted values:*/
                    closeAllLists();
                });
                a.appendChild(b);
              }
            }
        });
        /*execute a function presses a key on the keyboard:*/
        inp.addEventListener("keydown", function(e) {
            var x = document.getElementById(this.id + "autocomplete-list");
            if (x) x = x.getElementsByTagName("div");
            if (e.keyCode == 40) {
              /*If the arrow DOWN key is pressed,
              increase the currentFocus variable:*/
              currentFocus++;
              /*and and make the current item more visible:*/
              addActive(x);
            } else if (e.keyCode == 38) { //up
              /*If the arrow UP key is pressed,
              decrease the currentFocus variable:*/
              currentFocus--;
              /*and and make the current item more visible:*/
              addActive(x);
            } else if (e.keyCode == 13) {
              /*If the ENTER key is pressed, prevent the form from being submitted,*/
              e.preventDefault();
              if (currentFocus > -1) {
                /*and simulate a click on the "active" item:*/
                if (x) x[currentFocus].click();
              }
            }
        });
        function addActive(x) {
          /*a function to classify an item as "active":*/
          if (!x) return false;
          /*start by removing the "active" class on all items:*/
          removeActive(x);
          if (currentFocus >= x.length) currentFocus = 0;
          if (currentFocus < 0) currentFocus = (x.length - 1);
          /*add class "autocomplete-active":*/
          x[currentFocus].classList.add("autocomplete-active");
        }
        function removeActive(x) {
          /*a function to remove the "active" class from all autocomplete items:*/
          for (var i = 0; i < x.length; i++) {
            x[i].classList.remove("autocomplete-active");
          }
        }
        function closeAllLists(elmnt) {
          /*close all autocomplete lists in the document,
          except the one passed as an argument:*/
          var x = document.getElementsByClassName("autocomplete-items");
          for (var i = 0; i < x.length; i++) {
            if (elmnt != x[i] && elmnt != inp) {
              x[i].parentNode.removeChild(x[i]);
            }
          }
        }
        /*execute a function when someone clicks in the document:*/
        document.addEventListener("click", function (e) {
            closeAllLists(e.target);
        });
      }
autocomplete(document.getElementById("id_reco"), losReciclaroes);

   </script>
@stop
