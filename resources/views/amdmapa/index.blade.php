@extends('lap::layouts.auth')

@section('title', 'Mapa')
@section('child-content')
    <h2>@yield('title')</h2>

    <div class="row mb-3">
        <div class="col-md-auto mt-2 mt-md-0">
                <a href="{{ route('create_mapaview') }}" class="btn btn-success">Nuevo Mapa</a>
        </div>
    </div>

    <div class="card">
      <div class="card-body">
        <table class="table table-bordered">
            <tr>
                <th>N°</th>
                <th>Mapa</th>
                <th>Estado</th>
                <th>Mapa</th>
            </tr>
            @php
              $estado="";
            @endphp
            @foreach ($operaciones as $product )
              @php
                if($product->estado==0){
                  $estado="Sin Operacion";
                }else {
                  $estado="En Operacion";
                }
              @endphp
            <tr>
                <td>{{ ++$i }}</td>
                <td>{{ $product->nombreruta }}</td>
                <td>{{ $estado }}</td>
                <td>  <a onclick="ver_mapa(this.parentElement.parentElement)" style="cursor:pointer;color:blue">Ver mapa</a>  </td>
                <td><a  class="btn btn-link text-secondary p-1" title="Update"><i onclick="u_mapa(this.parentElement.parentElement.parentElement)" class="fal fa-lg fa-edit"></i></a></td>
            </tr>
            @endforeach
        </table>
        {{ $operaciones->links() }}
      </div>

    </div>
    <div onclick="afuera(this)" id="m_up_mapa" class="modalx">
         <div onclick="adentro(event)" class="modal-contenido">
           <div><b>Nombre: </b><input type="text" ></div>
           <button class="btn1" onclick="up_save(this.parentElement)">Guardar</button>
         </div>
   </div>

   <div onclick="afuera(this)" id="m_ver_mapa" class="modalx">
        <div style="height:80vh" onclick="adentro(event)" class="modal-contenido">
           <div  style="height:76vh" id="mapa"></div>
        </div>
  </div>

  <div class="none" style="min-width:auto;text-align: center;" id="copiar">
    <div class="" style="background-color:RGB(255,255,255,0.8)">
      <h5 id="cp_area">Area:</h5>
      <button id="a74" type="button" class="btn1 "  onClick="F9(this.id)" >Guardar</button>
    </div>

  </div>


@endsection

@section('css')
  <link rel="stylesheet" href="https://unpkg.com/leaflet@1.5.1/dist/leaflet.css"
  integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ=="
  crossorigin=""/>
  <style>
    .none{
    display:none
  }

  </style>
@stop
@section('javascript')


	<script src="https://unpkg.com/leaflet@1.5.1/dist/leaflet.js"
	   integrity="sha512-GffPMF3RvMeYyc1LWMHtK8EbPv0iNZ8/oTtHPx9/cc2ILxQ+u905qIwdpULaqDkyBKgOaB57QTMg7ztg8Jm2Og=="
	   crossorigin=""></script>
     <script src='https://npmcdn.com/@turf/turf/turf.min.js'></script>
   <script>
   let fijar_aqui = '<div style="min-width:auto;text-align: center;"><h4>Fijar aqu&iacute</h4> <button id="a74" type="button" class="btn1 "  onClick="F9(this.id)" >Si</button> </div>';
      let a = "{{json_encode($operaciones[0])}}";
      //document.getElementById("");
      console.log("hola");
      let u_mapa = (obj) =>{
        console.log(obj);
        let a = obj.querySelectorAll("td");
        let modal = document.getElementById("m_up_mapa");
        let m_inputs = modal.querySelectorAll("input");
        abrir(modal)
        m_inputs[0].value=a[1].innerText;

        modal.querySelector("button").setAttribute("td",a[0].innerText)
      }

      let up_save = (obj) =>{
        let m_inputs = obj.querySelectorAll("input");

        console.log(m_inputs[3].checked)

        let dataF = {nombre : m_inputs[0].value,
                      h1: m_inputs[1].value ,
                    h2: m_inputs[2].value ,
                    h3: m_inputs[3].value ,
                  h4: m_inputs[4].value ,
                  h5: m_inputs[5].value ,
                h6: m_inputs[6].value ,
                h7: m_inputs[7].value };
        dataF = $.param(dataF) ;
        console.log(dataF);

        $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });
        $.post('{{ route("u_mapa") }}', dataF , function(response) {
            console.log(response)
            location.reload();

          });
      }
      var marker ;
     var l1 = -11.900200;
     var l2 = -77.041463;
     var codigo = "";
     var rutcod = "";
     var nombreruta="";
     let configurado = false;
     var mypath = new Array();
      var mapa = L.map('mapa', {
         minZoom: 8,
         maxZoom: 18,
         center: [l1, l2],
         zoom: 16,
     })
     //var mapa = L.map('mapa').setView([l1, l2], 12);

   L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
       attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
   }).addTo(mapa);
   let ver_mapa = (obj)=>{
     let dataF = {nombre : obj.querySelectorAll("td")[1].innerText};
     $.ajaxSetup({
       headers: {
         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
       }
     });
     $.post('{{ route("area_mapa") }}', dataF , function(response) {
         console.log(response)
         graficado(response)
          calc_area(response)


       });

   }


   function graficado(val){
if(mypath.length>0){
  mapa.removeLayer(polygon)
  mypath = new Array();
}

let tam = val.length;


if (tam != 0) {

 for (var i = 0; i < tam; i++) {

    mypath.push([parseFloat(val[i].lat),parseFloat(val[i].lon)])
 }
mapa.panTo(new L.LatLng(val[tam-1].lat, val[tam-1].lon));
 polygon = L.polygon(mypath, {color: 'red'});

polygon.addTo(mapa);
polygon.bindPopup(fijar_aqui).openPopup();
abrir(document.getElementById("m_ver_mapa"));
}
}

L.Control.Watermark = L.Control.extend({
    onAdd: function(map) {
        var img = L.DomUtil.create('img');

        img.src = '{{ asset('qupa.png') }}';
        img.style.width = '100px';

        return img;
    },

    onRemove: function(map) {
        // Nothing to do here
    }
});

L.control.watermark = function(opts) {
    return new L.Control.Watermark(opts);
}

L.control.watermark({ position: 'bottomleft' }).addTo(mapa);
a = document.querySelector(".leaflet-bottom.leaflet-left");
b = document.getElementById("copiar")
a.append(b);
b.classList.add("leaflet-control");
b.classList.remove("none");


let calc_area =(data)=>{

    let  array1  = [];
    let a = [];
    data.forEach(item=>{
      array1.push([item.lon, item.lat])
    })
    array1.push([data[0].lon, data[0].lat]);

    //array1 = [[[-5.6659, 40.9641],	[-5.6664, 40.9669],	[-5.6616, 40.9660],	[-5.6611, 40.9637],	[-5.6659, 40.9641]]];
		var polygon = turf.polygon([array1]);

		var area = turf.area(polygon);
		console.log(area);
    document.getElementById("cp_area").innerText="Area: "+ area + " m2"
}
   </script>
@stop
