@extends('lap::layouts.auth')

@section('title', 'Usuarios')
@section('child-content')
    <h2>@yield('title')</h2>
    <div class="card">
        <div class="card-body ">
          <table class="table table-bordered"  width="100%" border="1"  bordercolor="#666666" id="Exportar_a_Excel">
            <thead>
              <tr>
                  <th ><a href="#">N°</a></th>
                  <th ><a href="#">Nombre</a></th>
                  <th ><a href="#">DNI/RUC</a></th>
                  <th ><a href="#">Ruta</a></th>
                  <th></th>
              </tr>
            </thead>

              @foreach ($operaciones as $i=>$product )

              <tr>
                  <td>{{ ++$i }}</td>
                  <td>{{ $product->nombre }}</td>
                  <td>{{ $product->dniruc }}</td>
                  <td>  {{ $product->nombreruta }}     </td>
                  <td>

                    <a  class="btn btn-link text-secondary p-1" title="Actualizar"><i onclick="u_usuario(this.parentElement.parentElement.parentElement)" class="fal fa-lg fa-edit"></i></a>
                  </td>
              </tr>
              @endforeach
          </table>
          {{-- {{ $operaciones->links() }} --}}
        </div>

    </div>

    <div onclick="afuera(this)" id="m_de_user" class="modalx">
         <div onclick="adentro(event)" class="modal-contenido" style="width: fit-content;">
           <center > <h2 class="t_title"></h2> </center>
           <h3 class="t_mesaje">

           </h3>
           <br>
           <div><button class="btn1" style="" onclick="delete_u(this)">Si!</button></div>
           <div><button class="btn1" style="" onclick="afuera(this.parentElement.parentElement)">No!</button></div>
         </div>
   </div>

   <div onclick="afuera(this)" id="m_up_user" class="modalx">
        <div onclick="adentro(event)" class="modal-contenido">
          <div><b>Nombre: </b><div id="nombre"></div></div>
          <div><b>DNI/RUC: </b><div id="dni"></div></div>
          <div><b>Ruta: </b>
            <select id="rut_list" name="id_ruta">
            </select>  </div>
          <button class="btn1" onclick="up_save(this.parentElement)">Guardar</button>
        </div>
  </div>

  <div onclick="afuera(this)" id="m_ver_qr" class="modalx">
       <div onclick="adentro(event)" class="modal-contenido">
         <div><b>Ver QR: </b><input id="el_qr" type="text"></div>
         <div style="text-align:center"class="">
           <canvas id="qr"></canvas>
         </div>

         <button class="btn1" onclick="generar_qr(this.parentElement)">Generar QR</button>
       </div>
 </div>


{{--   function sortTable(n) {
    var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
    table = document.getElementById("Exportar_a_Excel");
    switching = true;
    //Set the sorting direction to ascending:
    dir = "asc";
    /*Make a loop that will continue until
    no switching has been done:*/
    while (switching) {
      //start by saying: no switching is done:
      switching = false;
      rows = table.rows;
      /*Loop through all table rows (except the
      first, which contains table headers):*/
      for (i = 1; i < (rows.length - 1); i++) {
        //start by saying there should be no switching:
        shouldSwitch = false;
        /*Get the two elements you want to compare,
        one from current row and one from the next:*/
        x = rows[i].getElementsByTagName("TD")[n];
        y = rows[i + 1].getElementsByTagName("TD")[n];
        /*check if the two rows should switch place,
        based on the direction, asc or desc:*/
        if (dir == "asc") {
            if(n==0){
                if (parseFloat(x.innerHTML.toLowerCase()) > parseFloat(y.innerHTML.toLowerCase())) {
                  //if so, mark as a switch and break the loop:
                  shouldSwitch= true;
                  break;
                }
            }else{
                if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                  //if so, mark as a switch and break the loop:
                  shouldSwitch= true;
                  break;
                }
            }

        } else if (dir == "desc") {
            if(n==0){
                if (parseFloat(x.innerHTML.toLowerCase()) < parseFloat(y.innerHTML.toLowerCase())) {
                  //if so, mark as a switch and break the loop:
                  shouldSwitch= true;
                  break;
                }
            }else{
                if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
                  //if so, mark as a switch and break the loop:
                  shouldSwitch = true;
                  break;
                }
            }
        }
      }
      if (shouldSwitch) {
        /*If a switch has been marked, make the switch
        and mark that a switch has been done:*/
        rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
        switching = true;
        //Each time a switch is done, increase this count by 1:
        switchcount ++;
      } else {
        /*If no switching has been done AND the direction is "asc",
        set the direction to "desc" and run the while loop again.*/
        if (switchcount == 0 && dir == "asc") {
          dir = "desc";
          switching = true;
        }
      }
    }
  } --}}
@endsection
<script src="{{ asset('js/qrious.min.js') }}"></script>
@section("javascript")
  <script>
  elementos=[]
  let buscar =()=>{
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    $.post('{{ route("cambio_rutas") }}', {"s":"s"}, function(response) {
       console.log(response)
       temp1 = response.data;
       temp2 = document.getElementById("rut_list")
       temp1.forEach(item=>{
         a = document.createElement("option")
         a.value=item.id
         a.innerText=item.nombreruta
         temp2.appendChild(a)
         elementos.push(item.nombreruta);
       })

      }).fail(
    function(jqXHR, textStatus, errorThrown) {

     });


  }

  buscar();
   let ver_qr =(dniruc,code)=>{

     let modal = document.getElementById("m_ver_qr");
     abrir(modal);
     modal.querySelector("#el_qr").value = dniruc;
     var qr = new QRious({
         element: document.getElementById('qr'),
           size: 200,
         value: code
       });
   }

    let ver_cla =(obj)=>{
      let cla = obj.querySelector("#clav");
      if(cla.hasAttribute("hidden")){
        cla.removeAttribute("hidden");
        obj.querySelector("a").innerText = "Ocultar";
      }else{
        obj.querySelector("a").innerText = "Ver";
        cla.setAttribute("hidden",true)
      }
    }


    function u_usuario(obj){
      let modal = document.getElementById("m_up_user");
      let m_inputs = modal.querySelectorAll("input");
      let a = obj.querySelectorAll("td");
      modal.querySelector("#dni").innerText = a[2].innerText ;
      modal.querySelector("#nombre").innerText = a[1].innerText;
      nom = a[3].innerText;
      b = modal.querySelector("#rut_list");
      index = -1;
      elementos.forEach((item,i)=>{
        if(item===nom){
          index = i;
        }
      })
      b.selectedIndex= index;
      abrir(modal)
    }


    let up_save = (obj)=>{
      sel = obj.querySelector("#rut_list");
        let dataF = {
        id_ruta : sel.item(sel.selectedIndex).value,
        dni : obj.querySelector("#dni").innerText,
      }

        dataF = $.param(dataF) ;
        console.log(dataF)
        $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });
        $.post('{{ route("cambio_user_final") }}', dataF , function(response) {

          console.log(response)
          location.reload();

          }).fail(function() {
    alert( "DNI/RUC ya existe" );
  })
      afuera(obj)
    }





    $(document).ready(function() {


       var table = $('#Exportar_a_Excel').DataTable({
         "language": {
              "lengthMenu": "Mostrar _MENU_ filas por página",
              "zeroRecords": "No se encontró ese dato",
              "info": "Página _PAGE_ de _PAGES_",
              "infoEmpty": "Dato no disponible",
              "infoFiltered": "(filtered from _MAX_ total records)",
          },
          responsive: {
            details: false
        }
       });

  });


    </script>
@stop
