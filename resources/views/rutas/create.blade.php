@extends('lap::layouts.auth')

@section('title', 'Nueva Ruta')
@section('child-content')
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDpRGEocggOOw6n0bds0lVWADln60A9qU8"></script><!---->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script>

var obj = {"ruta":[{"lati":"-13.5226402","longi":"-71.9673386"}]};//
var tamano = obj.ruta.length;

var  map;



    var total =[];
     var plat =[];
  var plon =[];
  var pval =[];
  var markerst = [];

    var symbolOne = {
  path: 'M -2,0 0,-2 2,0 0,2 z',
  strokeColor: '#F00',
  fillColor: '#F00',
  fillOpacity: 1
};

var symbolThree = {
  path: 'M -2,-2 2,2 M 2,-2 -2,2',
  strokeColor: '#292',
  strokeWeight: 4
};

var lineSymbol ;

    var mypath = new Array();

    var rutaname = "Nueva";
      //document.write(5 + 6);

  var seedita=0;
 var seedita2=1;



  var trilat = parseFloat(obj.ruta[tamano-1].lati);
var trilon =parseFloat(obj.ruta[tamano-1].longi);





         var bermudaTriangle ;



var mruta = [];
var mruta2 = [];
var  tourplan2 ;
       function loadMap(){

$("#carea").toggle();

//$("#earea").toggle();
$("#oarea").toggle();
$("#finf").toggle();

       var mapProp = {
             center:new google.maps.LatLng(obj.ruta[0].lati,obj.ruta[0].longi),
             zoom:16,
             mapTypeId:google.maps.MapTypeId.ROADMAP,
            // mapTypeControl: false

          };

map = new google.maps.Map(document.getElementById("googleMap"),mapProp);

conf = [
  {
            featureType: 'poi.business',
            stylers: [{visibility: 'off'}]
          },
          {
            featureType: 'poi',
            stylers: [{visibility: 'off'}]
          }
];
map.set('styles',conf);

bermudaTriangle = new google.maps.Polygon({
//paths: triangleCoords,
strokeColor: '#FF0000',
draggable:true,
strokeOpacity: 0.8,
strokeWeight: 3,
fillColor: '#FF0000',
editable: true,
// visible: false,
fillOpacity: 0.35
});
tourplan2 = new google.maps.Polyline({


         strokeColor:"#F000FF",
         strokeOpacity:1,
          // draggable:true,
         //  editable:true,
         strokeWeight:2
      });

for(var i=0; i < tamano; i++){

              mypath.push(new google.maps.LatLng(obj.ruta[i].lati, obj.ruta[i].longi));
      }

var contentString =
    '<font size="4" color="black">Ruta '+rutaname+'</font>';

var infowindow = new google.maps.InfoWindow({
  content: contentString,
  position: new google.maps.LatLng(obj.ruta[tamano-1].lati,obj.ruta[tamano-1].longi)

});


          // var marker = new google.maps.Marker({
          //    position: new google.maps.LatLng(obj.ruta[0].lati,obj.ruta[0].longi),
          //    map: map,
          //    draggable:false,
          //      title: rutaname,
          //
          //    icon:'{{ asset('qupa.png') }}',
          //
          // });
//             infowindow.open(map);
//            marker.addListener('click', function() {
//   infowindow.open(map, marker);
//   //document.write(5 + 6);
//
// });
//
//           marker.setMap(map);

lineSymbol = {
  path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW
};
          var tourplan = new google.maps.Polyline({
             path:mypath,
             icons: [ {icon: symbolOne, offset: '0%'},{icon: lineSymbol, offset: '25%'},{icon: lineSymbol, offset: '50%'},{icon: lineSymbol, offset: '75%'}, {icon: symbolThree,
      offset: '100%'}],

             strokeColor:"#0000FF",
             strokeOpacity:1,
             strokeWeight:2
          });

          tourplan.setMap(map);

tourplan2.setMap(map);
bermudaTriangle.setMap(map);

tourplan2.addListener('click', function() {

});




google.maps.event.addListener(map, "click", function(event) {

  if(seedita==1){

var pathth= tourplan2.getPath();

pathth.push(event.latLng);

  }


if(seedita2==1){

var pathth= bermudaTriangle.getPath();

pathth.push(event.latLng);

  }




});


       }



       function f1 (elid){


          //document.getElementById("but"+elid).disabled = true;
         var esta = document.getElementById("but"+elid).value;

            if(esta.startsWith("E")){
                 document.getElementById("but"+elid).value = "Remover este punto de Control";
                  if(elid!=0){ pval[elid]="1";
                              }
            }else{
              document.getElementById("but"+elid).value = "Establecer como Punto de Control";
               if(elid!=0){ pval[elid]="0";
                              }
            }





        document.getElementById("demo").innerHTML ="";

           var tam =pval.length;

          for(var i=0; i < tam; i++){
            if(pval[i]!="0"){
               document.getElementById("demo").innerHTML = document.getElementById("demo").innerHTML+ " " + i ;
            }
          }
       }



       $(document).ready(function(){

$("#enviar1").click(function(){
    var cordr =   JSON.stringify(tourplan2.getPath().getArray());
    var corda =   JSON.stringify(bermudaTriangle.getPath().getArray());
    console.log(corda);
    document.getElementById("area").value=corda;

 });


    $("#earea").click(function(){
if($("#earea").val().startsWith("F")){
var co =   bermudaTriangle.getPath().getArray();
if(co.length>=3){
    if (confirm("Deseas Terminar la edición") == true) {
    seedita2=0;
    bermudaTriangle.setEditable(false);
    bermudaTriangle.setDraggable(false);
    $("#earea").hide();
        var corda =   JSON.stringify(bermudaTriangle.getPath().getArray());
        console.log(corda);
        document.getElementById("area").value=corda;
        $("#finali").show()
    }
}else{
    alert("Edita el area")
}

}



 });


      $("#carea").click(function(){

if (confirm("Deseas Borrar los puntos") == true) {
if($("#carea").val().startsWith("L")){

      // $("#eruta").prop('disabled', false);
       $("#earea").val("Editar Area");
      $("#oarea").prop('disabled', true);
      $("#carea").prop('disabled', true);
      bermudaTriangle.getPath().clear();
      seedita2=0;
      }else{
        // $("#eruta").prop('disabled', false);
         bermudaTriangle.setEditable(true);
            seedita2=1;
        $("#carea").val("Limpiar Area");
     $("#earea").val("Fin Edición");
      clearMarker2();

      }
}

 });

$("#oarea").click(function(){

var dato=false;
   if($("#oarea").val().startsWith("O")){
$("#oarea").val("Mostrar Area");
bermudaTriangle.setVisible(false);
$("#carea").prop('disabled', true);
dato=false;
seedita2=0;
}else{
$("#oarea").val("Ocultar Area");
bermudaTriangle.setVisible(true);
$("#carea").prop('disabled', false);
dato=true;
if($("#carea").val().startsWith("L")){
seedita2=1;
}

}

var tam =mruta2.length;

for(var i=0;i<tam;i++){
mruta2[i].setVisible(dato);

}


 });


});

function addMarker(location,i) {

mruta[i] = new google.maps.Marker({
  position: location,
  map: map
});
mruta[i].setMap(map)
}

function clearMarker() {
for(var i =0;i<mruta.length;i++)
mruta[i].setMap(null);
}

function addMarker2(location,i) {

mruta2[i] = new google.maps.Marker({
  position: location,
  map: map
});
mruta2[i].setMap(map)
}

function clearMarker2() {
for(var i =0;i<mruta2.length;i++)
mruta2[i].setMap(null);
}





</script>
    <h2>@yield('title')</h2>

    <form method="POST" action="{{ route('createruta') }}" novalidate data-ajax-form>
        @csrf

        <div class="list-group">
            <div class="list-group-item">
                <div class="form-group row mb-0">
                    <label for="name" class="col-md-2 col-form-label">Ruta</label>
                    <div class="col-md-8">
                        <input type="text" name="nombreruta" id="nombreruta" class="form-control">
                    </div>
                </div>
            </div>


                    <div class="list-group-item" style="display: flex;">
                        <div>  <label for="h1">Lunes</label> <input type="time" name="h1" id="h1"  class="form-control" > </div>
    					<div>  <label for="h2">Martes</label> <input type="time" name="h2" id="h2"  class="form-control" > </div>
    					<div>  <label for="h3">Miercoles</label> <input type="time" name="h3" id="h3"  class="form-control" > </div>
    					<div>  <label for="h4">Jueves</label> <input type="time" name="h4" id="h4"  class="form-control" > </div>
    					<div>  <label for="h5">Viernes</label> <input type="time" name="h5" id="h5"  class="form-control" > </div>
    					<div>  <label for="h6">Sabado</label> <input type="time" name="h6" id="h6"  class="form-control" > </div>
    					<div>  <label for="h7">Domingo</label> <input type="time" name="h7" id="h7"  class="form-control" > </div>
                    </div>
                    <div class="list-group-item" style="display:none">
                        <div class="form-group row mb-0">
                            <label for="name" class="col-md-2 col-form-label">Area</label>
                            <div class="col-md-8">
                                <input type="text" name="area" id="area" class="form-control">
                            </div>
                        </div>
                    </div>
                    <body class="list-group-item" onload = "loadMap()">
                            <div  class="" >
                              <input type="button" style="position: absolute;z-index: 900;margin-left: 30px;margin-top: 10px;" class="btn btn-danger gm-control-active" value="Fin de edición" id="earea" />
                              <div id = "googleMap" style = "width:100%; height:500px;"></div></div>
                    </body>

                    <div class="list-group-item bg-light text-left text-md-right pb-1" style="display:none" id="finali">
                        <button type="submit" name="_submit" class="btn btn-success mb-2" value="redirect">Guardar &amp; y retroceder</button>
                    </div>
        </div>
    </form>



@endsection
