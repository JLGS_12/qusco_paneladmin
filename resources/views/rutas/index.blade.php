@extends('lap::layouts.auth')

@section('title', 'Ruta y horario')
@section('child-content')
    <h2>@yield('title')</h2>

    <div class="row mb-3">
        <div class="col-md-auto mt-2 mt-md-0">
                <a href="{{ route('create_rutaview') }}" class="btn btn-success">Nueva Ruta</a>
        </div>
    </div>

    <div class="card">
      <div class="card-body">
        <table class="table table-bordered">
            <tr>
                <th>N°</th>
                <th>Ruta</th>
                <th width="180px" >Horario</th>
                <th>Inició</th>
                <th>Terminó</th>
                <th width="180px" >Ubicaión</th>
                <th>Estado</th>
                <th>Mapa</th>
                <th></th>
            </tr>
            @php
              $estado="";
            @endphp
            @foreach ($operaciones as $product )
              @php
                if($product->estado==0){
                  $estado="Sin Operacion";
                }else {
                  $estado="En Operacion";
                }
              @endphp
            <tr id="{{$product->id}}">
                <td>{{ ++$i }}</td>
                <td>{{ $product->nombreruta }}</td>
                <td id="h_{{$i}}">
                    <div class="none{{$product->h1}}">Lunes: {{$product->h1}}</div>
                    <div class="none{{$product->h2}}">Martes: {{$product->h2}}</div>
                    <div class="none{{$product->h3}}">Miercoles: {{$product->h3}}</div>
                    <div class="none{{$product->h4}}">Jueves: {{$product->h4}}</div>
                    <div class="none{{$product->h5}}">Viernes: {{$product->h5}}</div>
                    <div class="none{{$product->h6}}">Sabado: {{$product->h6}}</div>
                    <div class="none{{$product->h7}}">Domingo: {{$product->h7}}</div>
                </td>
                <td>{{ $product->inicio }}</td>
                <td>{{ $product->fin }}</td>
                <td>{{$product->ubilat ." , " .$product->ubilon}}</td>
                <td>{{ $estado }}</td>
                <td>   <a onclick="ver_mapa(this.parentElement.parentElement)" style="cursor:pointer;color:blue">Ver mapa</a>  </td>
                <td><a  class="btn btn-link text-secondary p-1" title="Update"><i onclick="u_mapa(this.parentElement.parentElement.parentElement)" class="fal fa-lg fa-edit"></i></a>
                <a  class="btn btn-link text-secondary p-1" title="Eliminar"><i onclick="d_ruta(this.parentElement.parentElement.parentElement)" class="fal fa-lg fa-trash"></i></a>
                </td>
            </tr>
            @endforeach
        </table>
        {{ $operaciones->links() }}
      </div>

    </div>
    <div onclick="afuera(this)" id="m_up_mapa" class="modalx">
         <div onclick="adentro(event)" class="modal-contenido">
           <div><b>Nombre: </b><input type="text" disabled></div>

           <div class="list-group-item" style="display: flex;">
               <div>  <label for="h1">Lunes</label> <input type="time" name="h1" id="h1"  class="form-control" > </div>
                <div>  <label for="h2">Martes</label> <input type="time" name="h2" id="h2"  class="form-control" > </div>
                <div>  <label for="h3">Miercoles</label> <input type="time" name="h3" id="h3"  class="form-control" > </div>
                <div>  <label for="h4">Jueves</label> <input type="time" name="h4" id="h4"  class="form-control" > </div>
                <div>  <label for="h5">Viernes</label> <input type="time" name="h5" id="h5"  class="form-control" > </div>
                <div>  <label for="h6">Sabado</label> <input type="time" name="h6" id="h6"  class="form-control" > </div>
                <div>  <label for="h7">Domingo</label> <input type="time" name="h7" id="h7"  class="form-control" > </div>
           </div>
           <button class="btn1" onclick="up_save(this.parentElement)">Guardar</button>
         </div>
   </div>

    <div onclick="afuera(this)" id="m_ver_mapa" class="modalx">
        <div style="height:80vh" onclick="adentro(event)" class="modal-contenido">
           <div  style="height:76vh" id="mapa"></div>
        </div>
  </div>
  
  <div onclick="afuera(this)" id="m_de_user" class="modalx">
        <div onclick="adentro(event)" class="modal-contenido" style="width: fit-content;">
          <center > <h2 class="t_title"></h2> </center>
          <h3 class="t_mesaje">

          </h3>
          <br>
          <div><button class="btn1" style="" onclick="delete_ruta(this)">Si!</button></div>
          <div><button class="btn1" style="" onclick="afuera(this.parentElement.parentElement)">No!</button></div>
        </div>
  </div>
@endsection

@section('css')
<link rel="stylesheet" href="https://unpkg.com/leaflet@1.5.1/dist/leaflet.css"
  integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ=="
  crossorigin=""/>
  <style>
    .none{
    display:none
  }

  </style>

@stop
@section('javascript')
<script src="https://unpkg.com/leaflet@1.5.1/dist/leaflet.js"
     integrity="sha512-GffPMF3RvMeYyc1LWMHtK8EbPv0iNZ8/oTtHPx9/cc2ILxQ+u905qIwdpULaqDkyBKgOaB57QTMg7ztg8Jm2Og=="
     crossorigin=""></script>
   <script>
   
    function d_ruta(obj){
         let a = obj.querySelectorAll("td");
        m_input=a[1].innerText;
        let modal = document.getElementById("m_de_user");
        modal.querySelector(".t_title").innerText="Desasignar esta ruta";
        modal.querySelector(".t_mesaje").innerText="¿Realmente deseas eliminar la ruta "+ m_input+"?";
        abrir(modal)

        modal.querySelectorAll("button")[0].setAttribute("td",obj.id)
    }
    function delete_ruta(obj){
        dataF = {id : obj.getAttribute("td")};
        $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });
        console.log(dataF);
        $.post('{{ route("del_ruta") }}', dataF , function(response) {
            console.log(response)
            location.reload();

          });
    }
   
      let a = "{{json_encode($operaciones[0])}}";
      //document.getElementById("");
      console.log("hola");
      let u_mapa = (obj) =>{
        console.log(obj);
        let a = obj.querySelectorAll("td");
        let modal = document.getElementById("m_up_mapa");
        let m_inputs = modal.querySelectorAll("input");
        abrir(modal)
        m_inputs[0].value=a[1].innerText;

        modal.querySelector("button").setAttribute("td",a[0].innerText)
      }

      let up_save = (obj) =>{
        let m_inputs = obj.querySelectorAll("input");

        console.log(m_inputs[3].checked)

        let dataF = {nombre : m_inputs[0].value,
                      h1: m_inputs[1].value ,
                    h2: m_inputs[2].value ,
                    h3: m_inputs[3].value ,
                  h4: m_inputs[4].value ,
                  h5: m_inputs[5].value ,
                h6: m_inputs[6].value ,
                h7: m_inputs[7].value };
        dataF = $.param(dataF) ;
        console.log(dataF)
        $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });
        $.post('{{ route("u_mapa") }}', dataF , function(response) {
            console.log(response)
            location.reload();

          });
      }

       var marker ;

     var l1 = -11.900200;
     var l2 = -77.041463;
     var codigo ="";
     var rutcod="";
     var nombreruta="";
     let configurado =false;
     var mypath = new Array();

      var mapa = L.map('mapa', {
         minZoom: 8,
         maxZoom: 16,
         center: [l1, l2],
         zoom: 16,
     })
     //var mapa = L.map('mapa').setView([l1, l2], 12);

   L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
       attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
   }).addTo(mapa);

      let ver_mapa = (obj)=>{

     let dataF = {nombre : obj.querySelectorAll("td")[1].innerText};
     $.ajaxSetup({
       headers: {
         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
       }
     });
     console.log(dataF)
     $.post('{{ route("area_ruta") }}', dataF , function(response) {
         console.log(response)
         graficado(response)

       });

   }

    function graficado(val){
    if(mypath.length>0){
      mapa.removeLayer(polygon)
      mypath = new Array();
    }

    let tam = val.length;


    if (tam != 0) {

     for (var i = 0; i < tam; i++) {

        mypath.push([parseFloat(val[i].lat),parseFloat(val[i].lon)])
     }
    mapa.panTo(new L.LatLng(val[tam-1].lat, val[tam-1].lon));
     polygon = L.polygon(mypath, {color: 'red'});

    polygon.addTo(mapa);
    abrir(document.getElementById("m_ver_mapa"));
    }
    }



   </script>
@stop
