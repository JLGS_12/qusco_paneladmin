<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link href="https://fonts.googleapis.com/css?family=Nunito:regular,bold" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ asset('lap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('lap/css/fontawesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('lap/css/datatables.min.css') }}">
    <link rel="stylesheet" href="{{ asset('lap/css/easymde.min.css') }}">
    <link rel="stylesheet" href="{{ asset('lap/css/lap.css') }}">

    <title>@yield('title') | {{ config('app.name') }}</title>
</head>
<style>
    .modal-contenido{
    background-color:white;
    width:80vw;
    max-height: 80vh;
    padding: 10px 20px;
    margin: 5vh auto;
    position: relative;
    overflow: auto;
    border-radius: 5px;
  }
  .modalx{
    z-index: 9900;
    background-color: rgba(0,0,0,.8);
    position:fixed;
    top:0;
    right:0;
    bottom:0;
    left:0;
    opacity:0;
    pointer-events:none;
    transition: all 0.5s;
  }
  .mystyle{
    opacity:1;
      pointer-events:auto;
      padding: 5vw;
  }
  .btn1 {
      background-color: #4caf50;
      border: none;
      color: white;
      padding: 8px;
      text-align: center;
      text-decoration: none;
      display: inline-block;
      font-size: 18px;
      margin: 4px 2px;
      cursor: pointer;
      border-radius: 12px;
      width: -webkit-fill-available;
    }
  input[type=text], select,input[type=number],input[type=password],input[type=date],input[type=email] {
  width: 100%;
  padding: 10px 10px;
  margin: 8px 0;
  display: inline-block;
  border: 1px solid #ccc;
  border-radius: 4px;
  box-sizing: border-box;
    }

    input[type="checkbox"] {
        zoom: 2.5;
         display: block;

        }


  </style>
@yield('css')
<body class="@yield('body-class')"{!! session('flash') ? ' data-flash-class="'.session('flash.0').'" data-flash-message="'.session('flash.1').'"' : '' !!}>

@yield('parent-content')

<script src="{{ asset('lap/js/jquery.min.js') }}"></script>
<script src="{{ asset('lap/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('lap/js/datatables.min.js') }}"></script>
<script src="{{ asset('lap/js/easymde.min.js') }}"></script>
<script src="{{ asset('lap/js/lap.js') }}"></script>
<link rel="stylesheet" href="{{ asset('css/datatables.min.css') }}">
<link rel="stylesheet" href="{{asset('js/datatables/dataTables.bootstrap4.min.css')}}">

<script src="{{ asset('js/datatables.min.js') }}"></script>
@stack('scripts')

<script>
    function abrir(obj){

            obj.classList.add("mystyle");
        }
        function afuera(obj) {
        obj.classList.remove("mystyle");
        }
        function adentro(event){
            event.stopPropagation();
        }

</script>
 @yield('javascript')

</body>
</html>
