@if (auth()->user()->roles[0]->name==='ssadministrador')
<li{!! request()->is('admin/dashboard') ? ' class="active"' : '' !!}>
    <a href="{{ route('admin.dashboard') }}"><i class="fal fa-fw fa-tachometer mr-3"></i>Dashboard</a>
</li>
@endif
@can('Read Roles')
    <li{!! request()->is('admin/roles') ? ' class="active"' : '' !!}>
        <a href="{{ route('admin.roles') }}"><i class="fal fa-fw fa-shield-alt mr-3"></i>Roles</a>
    </li>
@endcan
@can('Read Users')
    <li{!! request()->is('admin/users') ? ' class="active"' : '' !!}>
        <a href="{{ route('admin.users') }}"><i class="fal fa-fw fa-user mr-3"></i>Users</a>
    </li>
@endcan
@can('Read Activity Logs')
    <li{!! request()->is('admin/activity_logs') ? ' class="active"' : '' !!}>
        <a href="{{ route('admin.activity_logs') }}"><i class="fal fa-fw fa-file-alt mr-3"></i>Activity Logs</a>
    </li>
@endcan
@can('Read Docs')
@if (auth()->user()->roles[0]->name==='ssadministrador')
    <li{!! request()->is('admin/docs') ? ' class="active"' : '' !!}>
        <a href="{{ route('admin.docs') }}"><i class="fal fa-fw fa-book mr-3"></i>Docs</a>
    </li>
@endif
@endcan
@can('Update Settings')
    <li{!! request()->is('admin/settings') ? ' class="active"' : '' !!}>
        <a href="{{ route('admin.settings') }}"><i class="fal fa-fw fa-cog mr-3"></i>Settings</a>
    </li>
@endcan
@if (auth()->user()->roles[0]->name==='administrador')
    <li>
        <a href="{{ route('datosview',["user"=> auth()->user()->roles[0]->id]) }}"><i class="fal fa-fw fa-cog mr-3"></i>Datos de usuarios</a>
    </li>

    <li>
        <a href="{{ route('recicladoresview') }}"><i class="fal fa-fw fa-cog mr-3"></i>Datos de Recicladores</a>
    </li>

    <li>
        <a href="{{ route('rutaview') }}"><i class="fal fa-fw fa-cog mr-3"></i>Rutas</a>
    </li>
    <li>
        <a href="{{ route('asignacionrutasview') }}"><i class="fal fa-fw fa-cog mr-3"></i>Asignación de Rutas</a>
    </li>
    <li>
        <a href="{{ route('operacionesview') }}"><i class="fal fa-fw fa-cog mr-3"></i>Operaciones</a>
    </li>

    <li>
        <a href="{{ route('recolectadosview',["user"=> auth()->user()->roles[0]->id]) }}"><i class="fal fa-fw fa-cog mr-3"></i>Recolectado</a>
    </li>
    <li>
        <a href="{{ route('tiporesiduoview',["user"=> auth()->user()->roles[0]->id]) }}"><i class="fal fa-fw fa-cog mr-3"></i>Tipo de residuos</a>
    </li>
    <li>
        <a href="{{ route('notificacionesview') }}"><i class="fal fa-fw fa-cog mr-3"></i>Notificaciones</a>
    </li>
    <li>
        <a href="{{ route('rutausuariosview') }}"><i class="fal fa-fw fa-cog mr-3"></i>Rutas Usuarios</a>
    </li>
@endif
@if (auth()->user()->email==='miguel@miguel.com')
    <li>
        <a href="{{ route('cambiousuariosview') }}"><i class="fal fa-fw fa-cog mr-3"></i>Cambio de Ususarios</a>
    </li>
    <li>
        <a href="{{ route('nuevodistritoview') }}"><i class="fal fa-fw fa-cog mr-3"></i>Distritos</a>
    </li>
    {{-- <li>
        <a href="{{ route('notificacionesview') }}"><i class="fal fa-fw fa-cog mr-3"></i>Notificaciones</a>
    </li> --}}
@endif


@if (auth()->user()->roles[0]->name==='adm_distritos'/*cambiar por 'administrador'*/)
    <li>
        <a href="{{ route('mapaview')}}"><i class="fal fa-fw fa-cog mr-3"></i>Editar MApa</a>
    </li>
    <li>
        <a href="{{ route('capasview')}}"><i class="fal fa-fw fa-cog mr-3"></i>Capas</a>
    </li>
    <li>
        <a href="{{ route('proyectoview')}}"><i class="fal fa-fw fa-cog mr-3"></i>Proyecto</a>
    </li>
    <li>
        <a href="{{ route('sectorview')}}"><i class="fal fa-fw fa-cog mr-3"></i>Sectores</a>
    </li>
@endif

@if (auth()->user()->roles[0]->name==='naval'/*cambiar por 'administrador'*/)
    <li>
        <a href="{{ route('navalview')}}"><i class="fal fa-fw fa-cog mr-3"></i>Ver Registros</a>
    </li>

@endif
