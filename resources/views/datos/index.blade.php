@extends('lap::layouts.auth')

@section('title', 'Usuarios')
@section('child-content')
    <h2>@yield('title')</h2>
    <div class="card">
        <div class="card-body ">
          <table class="table table-bordered"  width="100%" border="1"  bordercolor="#666666" id="Exportar_a_Excel">
            <thead>
              <tr>
                  <th ><a href="#">N°</a></th>
                  <th ><a href="#">Nombre</a></th>
                  <th ><a href="#">DNI/RUC</a></th>
                  <th ><a href="#">Email</a></th>
                  <th ><a href="#">Telefono</a></th>
                  <th ><a href="#">Clave</a></th>
                  <th ><a href="#">Dirección</a></th>
                  <th ><a href="#">Ruta</a></th>
                  <th ><a href="#">QR</a></th>
                  <th></th>
              </tr>
            </thead>

              @foreach ($operaciones as $i=>$product )

              <tr>
                  <td>{{ ++$i }}</td>
                  <td>{{ $product->nombre }}</td>
                  <td>{{ $product->dniruc }}</td>
                  <td>{{ $product->email }}</td>
                  <td>{{ $product->telefono }}</td>
                  <td> <a style="cursor:pointer;color:blue" onclick="ver_cla(this.parentElement.parentElement)">Ver </a> <div hidden id="clav">{{$product->clave}}</div></td>
                  <td>{{ $product->direcion }}</td>
                  <td>  {{ $product->nombreruta }}     </td>
                  <td>  <a style="cursor:pointer;color:blue" onclick="ver_qr('{{ $product->dniruc }}','{{ $product->qr_code }}')">QR</a>  </td>
                  <td>
                    <a  class="btn btn-link text-secondary p-1" title="Eliminar"><i onclick="d_usuario(this.parentElement.parentElement.parentElement)" class="fal fa-lg fa-trash"></i></a>
                    <a  class="btn btn-link text-secondary p-1" title="Actualizar"><i onclick="u_usuario(this.parentElement.parentElement.parentElement)" class="fal fa-lg fa-edit"></i></a>
                  </td>
              </tr>
              @endforeach
          </table>
          {{-- {{ $operaciones->links() }} --}}
        </div>

    </div>
    <form action="{{route('descarga')}}" method="post" target="_blank" id="FormularioExportacion" style="cursor:pointer">
  <p>Exportar a Excel  <img src="{{url('/export_to_excel.jpg')}}" class="botonExcel" /></p>
  <input type="hidden" id="datos_a_enviar" name="datos_a_enviar" />
  <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
  </form>
    <div onclick="afuera(this)" id="m_de_user" class="modalx">
         <div onclick="adentro(event)" class="modal-contenido" style="width: fit-content;">
           <center > <h2 class="t_title"></h2> </center>
           <h3 class="t_mesaje">

           </h3>
           <br>
           <div><button class="btn1" style="" onclick="delete_u(this)">Si!</button></div>
           <div><button class="btn1" style="" onclick="afuera(this.parentElement.parentElement)">No!</button></div>
         </div>
   </div>


   <div onclick="afuera(this)" id="m_up_user" class="modalx">
        <div onclick="adentro(event)" class="modal-contenido">
          <div><b>Nombre: </b><input id="nombre" type="text"></div>
          <div><b>DNI/RUC: </b><input id="dni" type="text"></div>
          <div><b>Email: </b><input id="email" type="text" ></div>
          <div><b>Telefono: </b><input id="telefono" type="text"></div>
          <div><b>Clave: </b><input id="clave" type="text" ></div>
          <div><b>Dirección: </b><input id="direccion" type="text"></div>
          <button class="btn1" onclick="up_save(this.parentElement)">Guardar</button>
        </div>
  </div>

  <div onclick="afuera(this)" id="m_ver_qr" class="modalx">
       <div onclick="adentro(event)" class="modal-contenido">
         <div><b>Ver QR: </b><input id="el_qr" type="text"></div>
         <div style="text-align:center"class="">
           <canvas id="qr"></canvas>
         </div>

         <button class="btn1" onclick="generar_qr(this.parentElement)">Generar QR</button>
       </div>
 </div>



@endsection
<script src="{{ asset('js/qrious.min.js') }}"></script>
@section("javascript")
  <script>

  let generar_qr =(obj)=>{
    dat = obj.querySelector("#el_qr").value;
    datos = {dni:dat};

    datos = $.param(datos) ;
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    $.post('{{ route("generar_qr") }}', datos , function(response) {

       ver_qr(dat,response.qr_code)
      }).fail(
    function(jqXHR, textStatus, errorThrown) {

     });
  }
   let ver_qr =(dniruc,code)=>{

     let modal = document.getElementById("m_ver_qr");
     abrir(modal);
     modal.querySelector("#el_qr").value = dniruc;
     var qr = new QRious({
         element: document.getElementById('qr'),
           size: 200,
         value: code
       });
   }

    let ver_cla =(obj)=>{
      let cla = obj.querySelector("#clav");
      if(cla.hasAttribute("hidden")){
        cla.removeAttribute("hidden");
        obj.querySelector("a").innerText = "Ocultar";
      }else{
        obj.querySelector("a").innerText = "Ver";
        cla.setAttribute("hidden",true)
      }
    }

    function d_usuario(obj){
      let a = obj.querySelectorAll("td");
      m_input=a[2].innerText;
      let modal = document.getElementById("m_de_user");
      modal.querySelector(".t_title").innerText="Eliminación de usuario";
      modal.querySelector(".t_mesaje").innerText="¿Realmente deseas eliminar al usuario "+ m_input+"?";
      abrir(modal)

      modal.querySelectorAll("button")[0].setAttribute("td",m_input)

    }
    function u_usuario(obj){

      let modal = document.getElementById("m_up_user");
      let m_inputs = modal.querySelectorAll("input");
      let a = obj.querySelectorAll("td");

      m_inputs[0].value=a[1].innerText;
      m_inputs[1].value=a[2].innerText;
      if(m_inputs[1].value.length>0){m_inputs[1].disabled=true}else{m_inputs[1].disabled=false}
      m_inputs[2].value= a[3].innerText;
      if(m_inputs[2].value.length>0){m_inputs[2].disabled=true}else{m_inputs[2].disabled=false}
      m_inputs[3].value=a[4].innerText;
      m_inputs[4].value=obj.querySelector("#clav").innerText;
      m_inputs[5].value=a[6].innerText;

      abrir(modal)
    }

    let delete_u =(obj)=>{
      id= obj.getAttribute("td");
      let dataF = {id : id };
      dataF = $.param(dataF) ;

      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });
      $.post('{{ route("d_usuario") }}', dataF , function(response) {

          location.reload();

        });
    }
let u = 0;
let old="";
    let up_save = (obj)=>{
        let dataF = {
        nombre : obj.querySelector("#nombre").value,
        dni : obj.querySelector("#dni").value,
        email : obj.querySelector("#email").value,
        telefono : obj.querySelector("#telefono").value,
        direccion : obj.querySelector("#direccion").value,
        clave : obj.querySelector("#clave").value,
        up:u,
        old:old}

        dataF = $.param(dataF) ;

        $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });
        $.post('{{ route("up_user") }}', dataF , function(response) {

            location.reload();

          }).fail(function() {
    alert( "DNI/RUC ya existe" );
  })
      afuera(obj)
    }





    $(document).ready(function() {
       $(".botonExcel").click(function(event) {
       $("#datos_a_enviar").val( $("<div>").append( $("#Exportar_a_Excel").eq(0).clone()).html());
       $("#FormularioExportacion").submit();});

       var table = $('#Exportar_a_Excel').DataTable({
         "language": {
              "lengthMenu": "Mostrar _MENU_ filas por página",
              "zeroRecords": "No se encontró ese dato",
              "info": "Página _PAGE_ de _PAGES_",
              "infoEmpty": "Dato no disponible",
              "infoFiltered": "(filtered from _MAX_ total records)",
          },
          responsive:false
       });

  });


    </script>
@stop
