@extends('lap::layouts.auth')

@section('title', 'Tipo de Residuos')
@section('child-content')

    <h2>@yield('title')</h2>
    <div class="row mb-3">
        <div class="col-md-auto mt-2 mt-md-0">
                <a href="{{ route('create_tiporesiduoview') }}" class="btn btn-success">Nuevo tipo</a>
        </div>
    </div>
    <div class="card">
        <div class="card-body">
          <table>

          </table>
          <table class="table table-bordered" id="Exportar_a_Excel">
            <thead>
              <tr>
                  <th>N°</th>
                  <th>Tipo de Residuo</th>
                  <th></th>
              </tr>
            </thead>

              @foreach ($operaciones as $product )

              <tr id="{{$product->id}}">
                  <td>{{ ++$i }}</td>
                  <td>{{ $product->nombre }}</td>
                  <td>
                      <a  class="btn btn-link text-secondary p-1" title="Eliminar"><i onclick="d_ruta(this.parentElement.parentElement.parentElement)" class="fal fa-lg fa-trash"></i></a>
                  </td>
              </tr>
              @endforeach
          </table>
          {{ $operaciones->links() }}
        </div>

    </div>

    <div onclick="afuera(this)" id="m_de_user" class="modalx">

          <div onclick="adentro(event)" class="modal-contenido" style="width: fit-content;">

            <center > <h2 class="t_title"></h2> </center>

            <h3 class="t_mesaje">



            </h3>

            <br>

            <div><button class="btn1" style="" onclick="delete_ruta(this)">Si!</button></div>

            <div><button class="btn1" style="" onclick="afuera(this.parentElement.parentElement)">No!</button></div>

          </div>

    </div>




@endsection

@section('javascript')
<script type="text/javascript">
$(document).ready(function() {


    $('#Exportar_a_Excel').DataTable({
      "language": {
           "lengthMenu": "Mostrar _MENU_ filas por página",
           "zeroRecords": "No se encontró ese dato",
           "info": "Página _PAGE_ de _PAGES_",
           "infoEmpty": "Dato no disponible",
           "infoFiltered": "(filtered from _MAX_ total records)",

       }
    });
});


function d_ruta(obj){

     let a = obj.querySelectorAll("td");

    m_input=a[1].innerText;

    let modal = document.getElementById("m_de_user");

    modal.querySelector(".t_title").innerText="Desasignar esta ruta";

    modal.querySelector(".t_mesaje").innerText="¿Realmente deseas eliminar la ruta "+ m_input+"?";

    abrir(modal)



    modal.querySelectorAll("button")[0].setAttribute("td",obj.id)

}

function delete_ruta(obj){

    dataF = {id : obj.getAttribute("td")};

    $.ajaxSetup({

      headers: {

        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

      }

    });

    console.log(dataF);

    $.post('{{ route("del_tipo") }}', dataF , function(response) {

        console.log(response)

        location.reload();



      });

}

</script>
@endsection
