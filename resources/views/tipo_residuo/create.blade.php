@extends('lap::layouts.auth')

@section('title', 'Crear Tipo')
@section('child-content')
    <h2>@yield('title')</h2>

    <form method="POST" action="{{ route('createtiporesiduo') }}" novalidate data-ajax-form>
        @csrf

        <div class="list-group">
            <div class="list-group-item">
                <div class="form-group row mb-0">
                    <label for="name" class="col-md-2 col-form-label">Tipo de Residuo</label>
                    <div class="col-md-8">
                        <input type="text" name="nombre" id="nombre" class="form-control">
                    </div>
                </div>
            </div>


<!--       <div class="list-group-item">
                <div class="form-group row mb-0">
                    <label for="name" class="col-md-2 col-form-label">Quien recicla</label>
                    <div class="col-md-8">
                        <input type="text" name="id_quien" id="id_quien" class="form-control">
                    </div>
                </div>
            </div>-->


            <div class="list-group-item bg-light text-left text-md-right pb-1">
              
                <button type="submit" name="_submit" class="btn btn-success mb-2" value="redirect">Guardar &amp; y retroceder</button>
            </div>
        </div>
    </form>
@endsection
