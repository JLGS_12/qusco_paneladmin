@extends('lap::layouts.auth')

@section('title', 'Capas')
@section('child-content')
    <h2>@yield('title')</h2>

    <div class="row mb-3">
        <div class="col-md-auto mt-2 mt-md-0">
                <a onclick="f_nueva_capa()" class="btn btn-success">Nueva Capa</a>
        </div>
    </div>

    <div class="card">
      <div class="card-body">
        <table class="table table-bordered">
            <tr>
                <th>N°</th>
                <th>Capa</th>
                <th>Sector</th>
                <th>Descripcion</th>
                 <th></th>
            </tr>
            @php
              $estado="";
            @endphp
            @foreach ($operaciones as $product )

            <tr>
                <td>{{ ++$i }}</td>
                <td class="r_capa">{{ $product->nom_capa }}</td>
                <td>{{ $product->idsector }}</td>
                <td>{{$product->descripcion}}</td>
                <td><a  class="btn btn-link text-secondary p-1" title="Update"><i onclick="d_capa(this.parentElement.parentElement.parentElement)" class="fal fa-lg fa-trash"></i></a></td>
            </tr>
            @endforeach
        </table>
        {{ $operaciones->links() }}
      </div>

    </div>
    <div onclick="afuera(this)" id="m_nueva_capa" class="modalx">
         <div onclick="adentro(event)" class="modal-contenido">
           <div><b>Nombre: </b><input id="nom" type="text" ></div>
           <div><b>Sector: </b><select id="sec" type="text" > <option value="0">Seleccionar</option> </select></div>
           <div><b>Descripción: </b><input id="des" type="text" ></div>
           <button class="btn1" onclick="up_save(this.parentElement)">Guardar</button>
         </div>
   </div>

   <div onclick="afuera(this)" id="m_mensaje_capa" class="modalx">
        <div style="height:80vh" onclick="adentro(event)" class="modal-contenido">
          <div class="m_title"></div>
          <br>
          <div class="m_mesage"></div>
        </div>
  </div>




@endsection

@section('css')

  <style>
    .none{
    display:none
  }

  </style>
@stop
@section('javascript')

   <script>
   let f_nueva_capa=()=>{
     let modal = document.getElementById("m_nueva_capa");
      abrir(modal);

      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });
      $.post('{{ route("sel_sector") }}', {} , function(response) {
        console.log(response);
        response.forEach((item)=>{
          let op = modal.querySelector("option");
          let sel = modal.querySelector("select");
          let cop = op.cloneNode(true);
          sel.appendChild(cop);
          cop.innerText=item.nom_sector;
          cop.value=item.id;
        });
        }).fail(
      function(jqXHR, textStatus, errorThrown) {
        console.log(jqXHR.responseText);
       });
   }

   let d_capa=(obj)=>{
    let a = obj.querySelector(".r_capa").innerText;
    open_mesaje("Eliminar Capa","¿Deseas eliminar la capa "+a+"?");
   }
   let open_mesaje=(title,message)=>{
    let mod = document.getElementById("m_mensaje_capa");
    mod.querySelector(".m_title").innerText=title;
    mod.querySelector(".m_message").innerText=message;
    abrir(mod);
   }
         let up_save=(obj)=>{
    let nom = obj.querySelector("#nom").value;
    let des = obj.querySelector("#des").value;
    let a = obj.querySelector("#sec");
    let sec = a.options[a.selectedIndex].value;

    let dataF = {nom:nom,
                des:des,
                sec:sec};
    dataF = $.param(dataF) ;
    console.log(dataF)
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    $.post('{{ route("c_capas") }}', dataF , function(response) {
        console.log(response)
        location.reload();

      }).fail(
    function(jqXHR, textStatus, errorThrown) {
          console.log(jqXHR.responseText);
     });
   }


   let consulta =(url,json)=>{



   }
   </script>
@stop
