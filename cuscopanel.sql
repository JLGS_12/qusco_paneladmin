-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 12-06-2019 a las 05:13:33
-- Versión del servidor: 10.3.15-MariaDB
-- Versión de PHP: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `cuscopanel`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `disamd_capas`
--

CREATE TABLE `disamd_capas` (
  `id` int(10) UNSIGNED NOT NULL,
  `nom_capa` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `idsector` int(11) NOT NULL,
  `maestro` int(11) DEFAULT 0,
  `esclavo` int(11) DEFAULT 0,
  `descripcion` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `estado` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `disamd_mapas`
--

CREATE TABLE `disamd_mapas` (
  `id` int(10) UNSIGNED NOT NULL,
  `idcapas` int(11) DEFAULT 0,
  `idrut` int(11) NOT NULL,
  `area` double DEFAULT 0,
  `lat` double NOT NULL,
  `lon` double NOT NULL,
  `estado` int(11) DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `disamd_proyectos`
--

CREATE TABLE `disamd_proyectos` (
  `id` int(10) UNSIGNED NOT NULL,
  `nom_proyecto` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `pro_presupuesto` double NOT NULL,
  `id_sector` int(11) NOT NULL,
  `inicio` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `fin` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `descripcion` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `estado` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `disamd_rutas`
--

CREATE TABLE `disamd_rutas` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombreruta` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `estado` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `disamd_sectors`
--

CREATE TABLE `disamd_sectors` (
  `id` int(10) UNSIGNED NOT NULL,
  `nom_sector` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `presupuesto` double NOT NULL,
  `descripcion` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `estado` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `disamd_capas`
--
ALTER TABLE `disamd_capas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `disamd_mapas`
--
ALTER TABLE `disamd_mapas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `disamd_proyectos`
--
ALTER TABLE `disamd_proyectos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `disamd_rutas`
--
ALTER TABLE `disamd_rutas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `disamd_sectors`
--
ALTER TABLE `disamd_sectors`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `disamd_capas`
--
ALTER TABLE `disamd_capas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `disamd_mapas`
--
ALTER TABLE `disamd_mapas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `disamd_proyectos`
--
ALTER TABLE `disamd_proyectos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `disamd_rutas`
--
ALTER TABLE `disamd_rutas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `disamd_sectors`
--
ALTER TABLE `disamd_sectors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
